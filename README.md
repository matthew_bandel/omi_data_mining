## Juggernaut

Juggernaut is an application meant to crunch through single orbit OMI Level 1b Collection 4 .nc files, reducing each multi-dimensional feature therein to one point per orbit.  The output is meant to provide two streams of data:

1] Single orbit .h5 files to mimic the file formats used in OMPS, as well as an associated fields.txt file to declare the fields.

2] Single orbit .nc files to use in Boquette, an xarray/bokeh based trending prototype in the style of OMPS, as well as an associated nest.json file describing the nested structure of the data.

### Setup

The Juggernaut package includes the following files:

- juggernauts.py: the main Juggernaut script
- flags.txt: a list of quality parameters and associated bit descriptions
- requirements.txt: a list of minimum dependencies
- README.md: this document
- nccs_environment.txt: detailed list of dependencies from the NCCS environment where this is routinely run

Juggernaut was written using the NCCS's established Python 3.7 environment, which can be loaded as follows:

```buildoutcfg
$ module load python/GEOSpyD/Ana2019.10_py3.7
```

In case Juggernaut is run outside NCCS, the requirements.txt file lists the minimum dependencies needed to run the code in a python 3.7.1 environment. The requirements may be installed as follows:

```buildoutcfg
$ pip install -r requirements.txt
```

The nccs_environment.txt has a detailed list of all dependencies in the NCCS environment, for reference.

### Note on Collection 3 and pyhdf module
Collection 3 orbital files are in HDF4 (.he4) format.  I was unable to install the required pyhdf module in my local environment, due to a missing "hdf.h" file in the readily available distribution.  Pyhdf is available on the NCCS and so the juggernaut code is able to process Collection 3 files on the NCCS.  It may also be possible to install the module locally in some fashion.  However, all my attempts to do so were unsuccessful.  


### Single Orbit Use

Juggernaut is designed to act on a single orbit file with the following command, where the first argument is the outbound directory into which to deposit the reduced data, and the second argument is the orbital file to act upon:

```buildoutcfg
$ python juggernauts.py juggernaut_data datasets/orbits/OMI-Aura_L1-OML1BRUG_2011m0720t0213-o037292_v994-2020m1118t1903.nc
```

If the outbound directory is not already present, it will be created, as well as several subdirectories in which to deposit the separate output streams.  

Note: As the orbital time and orbit number are encoded in the orbital file names themselves, the structure of the filename is important and is assumed to follow the above format.  Specifically, the file name is split at its underscores to extract these data.

### Single Directory Use

Juggernaut may also be pointed at a single directory with the same syntax:

```buildoutcfg
$ python juggernauts.py juggernaut_data datasets/orbits
```

This will process each orbit in the directory, one at a time.

### Program Outline

The main program steps are as follows:

- Ingest the .nc files into a data tree of individual parameters by recursively scanning the hdf5 file layers for keys until nodes are found.  Load each parameter into a numpy based xarray.

- Split the features based on the parameter names in the flags.txt file.  Those mentioned in the file are designated as quality control flags, and contain a compressed representation of several bit codes in one integer.

- Decompose each of these features into individual binary bits.  For instance, the spectral channel quality flags are a set of 8 binary bits, on or off.  But they are represented by one entry in the source file, a base ten integer.  This integer is decomposed back into its binary representation, and a new entry is made in the data tree for each bit.  At the xarray level, boolean masking is used to convert the data into a series of True/Falses for every unique base ten integer in the data.  All masks with a particular bit in their binary representations are summed together for 8 new xarrays of 0s and 1s with the same shapes as the original.

- Use xarray to reduce all data features to one point in five different ways, namely:

    - take the average over all timepoints, as well as any other dimensions (all spatial pixels, all spectral pixels, etc.).  For a quality flag feature that has been decomposed by bit, this naturally equates to the fraction of pixels flagged by a particular bit.
    
    - take the standard deviation over all timepoints and other dimensions.
    
    - take the maximum over all timepoints and other dimensions.
    
    - take the minimum over all timepoints and other dimensions.
    
    - take the median over all timepoints and other dimensions using the 50th percentile.
    
- Create a .h5 file with one entry for each feature reduction.  Generate an associated fields schema.  The goal of these files is to feed into an OMPS replica for OMI.

- Combine all feature reductions into one xarray for each feature.  Further concatenate all features for a single band into its own xarray.  Construct a nesting diagram of the features.  The goal of these files is to feed Boquette, a prototype OMI trending system.

#### Thank you!

- Matthew Bandel, matthew.bandel@ssaihq.com

