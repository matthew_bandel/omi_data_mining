# prisms.py for Prism class to contain data for a multi-dimensional feature

# import reload
from importlib import reload

# import math
import numpy
from numpy.random import random
from math import sin, cos, pi, log10, exp, floor, ceil, sqrt

# import tools
from pprint import pprint
from collections import Counter
from copy import copy, deepcopy

# import datetime
from datetime import datetime, timedelta
from time import time

# import xarray
import xarray
xarray.set_options(keep_attrs=True)

# import pandas
import pandas

# import plate module
from plates import Plate


# class Prism to contain the particular data view information
class Prism(list):
    """Prism class to contain data orientation and view information.

    Inherits from:
        list
    """

    def __init__(self, tensor):
        """Initialize a prism instance.

        Arguments:
            tensor: xarray

        Returns:
            Prism instance
        """

        # set current time
        self.now = time()

        # set tensor
        self.tensor = tensor

        # define reductions
        self.reductions = {}
        self._define()

        # begin list of manipulations
        self.manipulations = []

        return

    def __repr__(self):
        """Represent with string.

        Arguments:
            None

        Returns:
            str
        """

        # begin representation
        representation = {}

        # populate
        representation['index'] = self.tensor.index
        representation['name'] = self.tensor.name
        representation['route'] = self.tensor.route
        representation['shape'] = self.tensor.shape
        representation['labels'] = self.tensor.coords

        # pretty print
        pprint(representation)

        # construct fake string
        faux = '< Prism: {} >\n'.format(self.tensor.name)

        return faux

    def _address(self, scheme):
        """Create the address of the orientation.

        Arguments:
            scheme: list of int, str

        Returns:
            str
        """

        # link labels with scheme
        links = [(label, plan) for label, plan in zip(self.tensor.dims, scheme)]
        digits = [(label, plan) for label, plan in links if str(plan).isdigit()]
        reductions = [(label, plan) for label, plan in links if str(plan) in self.reductions.keys()]

        # get feature name
        name = self.tensor.name

        # make location
        location = ''
        if len(digits) > 0:

            # join labels
            location += ', '.join(['{} {}'.format(label, plan) for label, plan in digits])

        # construct reduction
        reduction = ' '
        if len(reductions) > 0:

            # find operation
            operation = [self._pick(plan, self.reductions.keys()) for _, plan in reductions][0]
            parameters = ' x '.join([label for label, _ in reductions])
            reduction += '({} of {})'.format(operation, parameters)

        # make address
        address = '{}:\n{}\n{}'.format(name, location, reduction)

        return reduction

    def _advance(self, *options):
        """Advance through distal layer.

        Arguments:
            *options: unpacked tuple of options

        Returns:
            None
        """

        # increase distal index
        layer = self.layer[self.distal] + 1

        # check options
        if len(options) > 0:

            # set layer to option
            layer = int(options[0])

        # check against bounds
        if layer > self.shape[self.distal] - 1:

            # drop to zero
            layer = self.shape[self.distal]

        # set layer
        self.layer[self.distal] = layer

        # refresh
        self.polish()

        return None

    def _average(self):
        """Replace all reductions with mean.

        Arguments:
            None

        Returns:
            None
        """

        # get layer
        scheme = self[-1].scheme

        # replace all reductions with minimum
        reducing = lambda plan: 'mean' if str(plan).split('_')[0] in self.reductions.keys() else plan
        scheme = [reducing(plan) for plan in scheme]

        # extract the layer
        self.view(scheme)

        return None

    def _bite(self, tensor, bit):
        """Recast a tensor as a mask for whether a bit is on or off.

        Arguments:
            tensor: numpy array
            bit: int

        Returns:
            numpy array
        """

        # convert to int
        bit = int(bit)

        # get all unique values
        integers = numpy.array(tensor).astype(int)
        zeros = numpy.zeros(numpy.max(integers) + 1, dtype=int)
        zeros[integers.ravel()] = 1
        uniques = numpy.nonzero(zeros)[0]

        # decompose all uniques
        codes = []
        for unique in uniques:

            # decompose and add to list
            decomposition = self._decompose(unique)
            if bit in decomposition:

                # add to codes
                codes.append(unique)

        # create boolean masks and convert to ints
        masks = [tensor == code for code in codes]
        masks = [mask.astype('int') for mask in masks]
        masks = [xarray.zeros_like(tensor)] + masks

        # concatenate and sum
        mask = xarray.concat(masks, dim='faux')
        mask = xarray.DataArray.sum(mask, dim='faux')

        # reset attributes
        mask.attrs = tensor.attrs

        return mask

    def _calculate(self):
        """Calculate base statistics of the layer.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.mean
            self.deviation
            self.median
            self.minimum
            self.maximuum

        Returns:
            None
        """

        # skip if bypass
        if not self.bypass:

            # ravel data into a list
            data = self.data.ravel()

            # perform statistics
            self.mean = numpy.average(data)
            self.deviation = numpy.std(data)
            self.median = numpy.percentile(data, 50)
            self.minimum = min(data)
            self.maximum = max(data)

        return None

    def _caliper(self, number):
        """Determine the precision of a floating point number.

        Arguments:
            number: float or int

        Returns:
            int
        """

        # convert to string
        string = str(number)

        # check for decimal place
        precision = 0
        if '.' in string:

            # precision is number of digits past the decimal
            precision = len(string.split('.')[1])

        return precision

    def _chunk(self, data, details):
        """Recast as a running average or minimum / maximum.

        Arguments:
            data: numpy array
            details: str

        Returns:
            numpy array
        """

        # parse details
        horizontal, _, number, method = details.split('_')
        number = int(number)

        # determine number of bins
        bins = ceil(data.shape[1] / number)

        # make reducers
        reducers = {'mean': numpy.mean, 'min': numpy.min, 'max': numpy.max}

        # create new vectors
        vectors = []
        for row in data:

            # make vector
            vector = numpy.array([reducers[method](row[index * number: (index + 1) * number]) for index in range(bins)])
            vectors.append(vector)

        # get data from rows
        data = numpy.array(vectors)

        return data

    def _decompose(self, code):
        """Decompose a quality code into bit positions.

        Arguments:
            code: int

        Returns:
            list of ints
        """

        # get all powers of two
        twos = []
        number = 0
        while 2 ** number <= code:

            # add power of two
            two = 2 ** number
            twos.append(two)
            number += 1

        # create bit list
        bits = []
        positions = [(index, two) for index, two in enumerate(twos)]
        positions.reverse()
        for position, two in positions:

            # try to subtract
            if two <= code:

                # add bit to position
                bits.append(position)
                code += -two

        return bits

    def _define(self):
        """Define the reduction functions:

        Arguments:
            None

        Returns:
            None

        Populates:
            self.reductions
        """

        # define numpy reductions
        self.reductions['mean'] = lambda data, axes, _, __: data.mean(dim=axes)
        self.reductions['minimum'] = lambda data, axes, _, __: data.min(dim=axes)
        self.reductions['maximum'] = lambda data, axes, _, __: data.max(dim=axes)
        self.reductions['deviation'] = lambda data, axes, _, __: data.std(dim=axes)
        self.reductions['median'] = lambda data, axes, _, __: data.quantile(0.5, dim=axes)

        # define complex reductions
        self.reductions['fraction'] = lambda data, axes, code, _: self._fractionate(data, axes, code)
        self.reductions['bit'] = lambda data, axes, bit, _: self._bite(data, axes, bit)
        self.reductions['run'] = lambda data, axes, number, method: self._run(data, axes, number, method)

        return None

    def _deviate(self):
        """Replace all reductions with mean.

        Arguments:
            None

        Returns:
            None
        """

        # get scheme
        scheme = self[-1].scheme

        # replace all reductions with minimum
        reducing = lambda place: 'deviation' if str(place).split('_')[0] in self.reductions.keys() else place
        scheme = [reducing(place) for place in scheme]

        # extract the scheme
        self.view(scheme)

        return None

    def _divide(self, divisor):
        """Divide the current view by the divisor matrix.

        Arguments:
            divisor: 2-D xarray

        Returns:
            None
        """

        # fill in zeros with 1's
        divisor = divisor.where(divisor != 0, 1)
        divisor = divisor.data

        # make a mask to fit the current matrix
        mask = xarray.ones_like(self[-1].matrix)

        # trim divisor against the mask
        divisor = divisor[:mask.shape[0], :mask.shape[1]]

        # replace the mask with the divisor
        mask[:divisor.shape[0], :divisor.shape[1]] = divisor

        # view the matrix
        manipulation = lambda matrix: matrix / mask
        self.maniputions.append(manipulation)
        scheme = self[-1].scheme
        self.view(scheme=scheme)

        return None

    def _diffract(self, code):
        """Replace all reductions with the fraction of a particular code.

        Arguments:
            code: int

        Returns:
            None
        """

        # get scheme
        scheme = self[-1].scheme

        # set manipulation function
        manipulation = lambda tensor: self._fractionate(tensor, code)
        self.manipulations.append(manipulation)

        # extract the scheme
        self.view(scheme, suffix='flag {}'.format(code))

        return None

    def _entitle(self, entitlement):
        """Add a custom title to the view.

        Arguments:
            entitlement: str

        Returns:
            None
        """

        # get layer
        scheme = self[-1].scheme

        # extract the layer
        self.view(scheme, spectrum=self[-1].spectrum, entitlement=entitlement, unity=self[-1].unity)

        return None

    def _fly(self, index, scheme):
        """Go to a particular index.

        Arguments:
            index: int

        Returns:
            None
        """

        # get first place after vertical not horizontal
        horizontal = scheme.index('horizontal')

        # try to
        try:

            # reset to next place
            empty = [place for place, member in enumerate(scheme) if place > horizontal and member != 'vertical'][0]

        # unless too far
        except IndexError:

            # use first available place
            empty = [place for place, member in enumerate(scheme) if place != horizontal and member != 'vertical'][0]

        # reset scheme
        scheme[empty] = 'horizontal'
        scheme[horizontal] = index

        # extract
        self.view(scheme)

        return None

    def _forget(self):
        """Remove the last manipulation.

        Arguments:
            None

        Returns:
            None
        """

        # remove last manipulation
        self.manipulations = self.manipulations[-2:]

        # get layer
        scheme = self[-1].scheme

        # extract the layer
        self.view(scheme, spectrum=self[-1].spectrum, entitlement=self[-1].entitlement, unity=self[-1].unity)

        return None

    def _fractionate(self, tensor, code):
        """Recast a dataset into fractions of a particular value.

        Arguments:
            tensor: numpy array
            code: int

        Returns:
            numpy array
        """

        # create a boolean mask
        mask = tensor.astype(int) == int(code)

        # convert to int
        mask = mask.astype(int)

        return mask

    def _glaze(self, spectrum):

        # get layer
        scheme = self[-1].scheme

        # extract the layer
        self.view(scheme, spectrum=spectrum)

    def _go(self, index, scheme):
        """Go to a particular index.

        Arguments:
            index: int

        Returns:
            None
        """

        # get first place after vertical not horizontal
        vertical = scheme.index('vertical')

        # try to
        try:

            # reset to next place
            empty = [place for place, member in enumerate(scheme) if place > vertical and member != 'horizontal'][0]

        # unless too far
        except IndexError:

            # use first available place
            empty = [place for place, member in enumerate(scheme) if place != vertical and member != 'horizontal'][0]

        # reset scheme
        scheme[empty] = 'vertical'
        scheme[vertical] = index

        # extract
        self.view(scheme)

        return None

    def _maximize(self):
        """Replace all reductions with minimum.

        Arguments:
            None

        Returns:
            numpy array
        """

        # get scheme
        scheme = self[-1].scheme

        # replace all reductions with minimum
        reducing = lambda place: 'maximum' if str(place).split('_')[0] in self.reductions.keys() else place
        scheme = [reducing(place) for place in scheme]

        # extract the scheme
        self.view(scheme)

        return None

    def _mediate(self):
        """Replace all reductions with median.

        Arguments:
            None

        Returns:
            None
        """

        # get layer
        scheme = self[-1].scheme

        # replace all reductions with minimum
        reducing = lambda plan: 'median' if str(plan).split('_')[0] in self.reductions.keys() else plan
        scheme = [reducing(plan) for plan in scheme]

        # extract the layer
        self.view(scheme)

        return None

    def _meld(self, number, method):
        """Replace all reductions with a running average over a certain number.

        Arguments:
            code: int

        Returns:
            None
        """

        # get scheme
        scheme = self[-1].scheme

        # replace vertical axis with running average
        melding = lambda place: 'horizontal_bin_{}_{}'.format(number, method) if str(place).startswith('horizontal') else place
        scheme = [melding(place) for place in scheme]

        # extract the scheme
        self.view(scheme)

        return None

    def _melt(self, number, method):
        """Replace horizontal axis with running average.

        Arguments:
            code: int

        Returns:
            None
        """

        # get scheme
        scheme = self[-1].scheme

        # replace vertical axis with running average
        melting = lambda place: 'horizontal_run_{}_{}'.format(number, method) if str(place).startswith('horizontal') else place
        scheme = [melting(place) for place in scheme]

        # extract the scheme
        self.view(scheme)

        return None

    def _minimize(self):
        """Replace all reductions with minimum.

        Arguments:
            None

        Returns:
            numpy array
        """

        # get scheme
        scheme = self[-1].scheme

        # replace all reductions with minimum
        reducing = lambda place: 'minimum' if str(place).split('_')[0] in self.reductions.keys() else place
        scheme = [reducing(place) for place in scheme]

        # extract the scheme
        self.view(scheme)

        return None

    def _pick(self, offer, options):
        """Pick the closest match to an offer from a list of options.

        Arguments:
            pffer: str
            options: list of str (or generator?)

        Returns:
            str
        """

        # check for direct present
        best = offer
        if best not in options:

            # calculate fuzzy matchies
            fuzzies = [(option, fuzz.ratio(offer.lower(), option.lower())) for option in options]
            fuzzies.sort(key=lambda score: score[1], reverse=True)

            # find the best match
            best = fuzzies[0][0]

        return best

    def _polish(self):
        """Reset view to the default layer.

        Arguments:
            None

        Returns:
            None
        """

        # set scheme
        scheme = ['vertical', 'horizontal'] + ['mean'] * (len(self.tensor.shape) - 2)

        # remove manipulations
        self.manipulations = []

        # make view
        self.view(scheme)

        return None

    def _populate(self, feature):
        """Populate self with feature.

        Arguments:
            feature: dict

        Returns:
            None

        Populates:
            self
        """

        # go through each feature
        for field, info in feature.items():

            # populate self
            self[field] = info

        return None

    def _refract(self, bit):
        """Replace all reductions with the fraction of a particular code.

        Arguments:
            code: int

        Returns:
            None
        """

        # get scheme
        scheme = self[-1].scheme

        # set manipulation function
        manipulation = lambda matrix: self._bite(matrix, bit)
        self.manipulations.append(manipulation)

        # extract the scheme
        self.view(scheme, suffix='bit {}'.format(bit))

        return None

    def _retreat(self, *options):
        """Retreat through distal scheme.

        Arguments:
            *options: unpacked tuple of options

        Returns:
            None
        """

        # increase distal index
        scheme = self.scheme[self.distal] - 1

        # check options
        if len(options) > 0:

            # set scheme to option
            scheme = int(options[0])

        # correct scheme
        if scheme < 0:

            # drop to zero
            scheme = 0

        # set scheme
        self.scheme[self.distal] = scheme

        # refresh
        self.polish()

        return None

    def _round(self, number, precision=None, up=False):
        """Round the number to a particular precision.

        Arguments:
            number: float or int
            precision: int
            up=False: round up?

        Returns:
            float or int
        """

        # duck overflow errors
        rounded = number
        try:

            # default precision
            precision = precision or self.precision

            # shift decimal places
            shift = number * 10 ** precision
            rounded = int(shift)

            # round it up
            if up:

                # round it up
                rounded = rounded + (int(rounded < shift))

            # or round down
            if not up:

                # round it down
                rounded = rounded - (int(rounded > shift))

            # lower back down
            rounded = rounded / 10 ** precision

            # convert to int
            if precision <= 0:

                # convert to int
                rounded = int(rounded)

        # skip if infinity
        except OverflowError:

            # pass
            pass

        return rounded

    def _run(self, data, details):
        """Recast as a running average or minimum / maximum.

        Arguments:
            data: numpy array
            details: str

        Returns:
            numpy array
        """

        # parse details
        horizontal, _, number, method = details.split('_')
        number = int(number)

        # get blank value
        blanks = {'mean': lambda data: 0}
        blanks.update({'min': lambda data: ceil(numpy.max(data)) + 1})
        blanks.update({'max': lambda data: floor(numpy.min(data)) - 1})

        # get blank value
        blank = blanks[method](data)

        # add margins to each side
        margin = numpy.full((data.shape[0], int(number / 2)), blank)
        matrix = numpy.concatenate([margin, data, margin], axis=1)

        # assemble windows
        windows = [matrix[:, index: -(number - index)] for index in range(number)]
        tensor = numpy.array(windows)

        # get reducer
        reducers = {'mean': lambda tensor: numpy.average(tensor, axis=0)}
        reducers.update({'min': lambda tensor: numpy.min(tensor, axis=0)})
        reducers.update({'max': lambda tensor: numpy.max(tensor, axis=0)})

        # apply reducer
        data = reducers[method](tensor)

        return data

    def _skim(self, members):
        """Skim off the unique members from a list.

        Arguments:
            members: list

        Returns:
            list
        """

        # trim duplicates and sort
        members = list(set(members))
        members.sort()

        return members

    def _stamp(self, message):
        """Start timing a block of code, and print results with a message.

        Arguments:
            message: str

        Returns:
            None
        """

        # get final time
        final = time()

        # calculate duration and reset time
        duration = round(final - self.now, 5)
        self.now = final

        # print duration
        print('took {} seconds.'.format(duration))

        # begin new block
        print(message)

        return None

    def _subtract(self, subtractor):
        """Divide the current view by the divisor matrix.

        Arguments:
            subtractor: 2-D xarray

        Returns:
            None
        """

        # get subtractor data
        subtractor = subtractor.data

        # make a mask to fit the current matrix
        mask = xarray.zeros_like(self[-1].matrix)

        # trim subtractor against the mask
        subtractor = subtractor[:mask.shape[0], :mask.shape[1]]

        # replace the mask with the subtractor
        mask[:subtractor.shape[0], :subtractor.shape[1]] = subtractor

        # view the matrix
        manipulation = lambda matrix: 200 * (matrix - mask) / (matrix + mask).where((matrix + mask) != 0, 1)
        self.manipulations.append(manipulation)
        scheme = self[-1].scheme
        self.view(scheme=scheme, entitlement=self[-1].entitlement, unity=self[-1].unity)

        return None

    def _tilt(self):
        """Tilt the vertical axis into the next distal one.

        Arguments:
            None

        Returns:
            None
        """

        # get scheme
        scheme = self[-1].scheme

        # swap horizontal and vertical axes
        horizontal = [index for index, plan in enumerate(scheme) if str(plan).startswith('horizontal')][0]
        vertical = [index for index, plan in enumerate(scheme) if str(plan).startswith('vertical')][0]
        indices = [index for index, plan in enumerate(scheme) if not str(plan).startswith('vertical') and not str(plan).startswith('horizontal')]
        scheme[indices[0]] = 'vertical'
        scheme[vertical] = 'mean'

        # reset zoom
        left, right, top, bottom = self[-1].zoom
        zoom = (top, bottom, left, right)

        # extract the scheme
        self.view(scheme, zoom)

    def _truncate(self, field, size=25):
        """Truncate a field name to a certain size.

        Arguments:
            field: str

        Returns:
            str
        """

        # truncate the string
        truncation = field
        if len(field) > size:

            # replace middle with ellipses
            half = int((size - 3) / 2)

            # make new string
            truncation = field[:half] + '...' + field[-half:]

        return truncation

    def _twist(self):
        """Twist the horizontal and vertical axes.

        Arguments:
            None

        Returns:
            None
        """

        # get scheme
        scheme = self[-1].scheme

        # swap horizontal and vertical axes
        horizontal = [index for index, plan in enumerate(scheme) if str(plan).startswith('horizontal')][0]
        vertical = [index for index, plan in enumerate(scheme) if str(plan).startswith('vertical')][0]
        scheme[horizontal] = 'vertical'
        scheme[vertical] = 'horizontal'

        # reset zoom
        left, right, top, bottom = self[-1].zoom
        zoom = (top, bottom, left, right)

        # extract the scheme
        self.view(scheme, zoom)

        return None

    def _typeset(self, ticks):
        """Determine the labels and fonts for the graph ticks.

        Arguments:
            ticks: list of floats, ints

        Returns:
            None
        """

        # convert ticks to floats based on the precision
        precision = max([0, self.precision])
        format = '%.' + str(precision) + 'f'
        decimals = [format % tick for tick in ticks]

        # convert to single place exponentials
        exponentials = ['%.1e' % tick for tick in ticks]

        # sort by total length
        options = [decimals, exponentials]
        options.sort(key=lambda strings: len(''.join(strings)))
        labels = options[0]

        # determine font size
        length = len(''.join(labels))
        font = min([10, int(500 / length)])

        return labels, font

    def _unify(self, unity):
        """Add a custom unit to the view.

        Arguments:
            unity: str

        Returns:
            None
        """

        # get layer
        scheme = self[-1].scheme

        # extract the layer
        self.view(scheme, spectrum=self[-1].spectrum, entitlement=self[-1].entitlement, unity=unity)

        return None

    def _walk(self, node, *fields, show=False):
        """walk through a dict by multiple fields.

        Arguments:
            node: dict or h5
            #fields: unpacked tuple of key names
            show=False: boolean, show the fields picked?

        Returns:
            dict or h5
        """

        # set contents to node
        contents = node

        # go through each step
        for field in fields:

            # try to
            try:

                # get the best match
                best = self._pick(field, contents.keys())

                # print
                if show:

                    # print
                    print(best)

                # select new node
                contents = contents[best]

            # unless already the data
            except AttributeError:

                # get names
                names = list(contents.dtype.names)
                index = names.index(field)

                # get contents
                contents = [content[index] for content in contents[0]]

        return contents

    def _zoom(self, top, bottom):
        """Twist the horizontal and vertical axes.

        Arguments:
            top: int
            bottom: int

        Returns:
            None
        """

        # get layer
        scheme = self[-1].scheme

        # make zoom
        left = self[-1].zoom[0]
        right = self[-1].zoom[1]
        zoom = (left, right, top, bottom)

        # extract the layer
        self.view(scheme, zoom)

    def dice(self, dimensions):
        """Dice the feature into fractions by unique values.

        Arguments:
            dimensions: int, number of dimensions to use

        Returns:
            list of Prism instances
        """

        # get all unique values
        uniques = numpy.unique(self.tensor)

        print(self.name)
        print(uniques)

        # go through each value
        replicas = []
        for unique in uniques:

            # forge a replica
            replica = self.replicate()

            # make boolean mask for unique value
            mask = replica.matrix == unique

            # determine total number of elements
            elements = numpy.prod(mask.shape[-dimensions:])

            # sum along lowest dimensions
            for _ in range(dimensions):

                # sum
                mask = numpy.sum(mask, axis=-1)

            # divide by total number of elements and convert to percents
            mask = numpy.divide(mask, elements)
            mask = numpy.multiply(mask, 100)

            # reset attributes
            replica.matrix = mask
            replica.shape = mask.shape
            replica.dimensions = len(mask.shape)
            replica.signifiers = replica.signifiers[:-dimensions]
            replica.name += '_%{}'.format(unique)

            # append
            replicas.append(replica)

        return replicas

    def polish(self):
        """Recalculate all bins and data based on new orientation.

        Arguments:
            None

        Returns:
            None
        """

        # recalculate and orient
        self._orient()
        self._calculate()
        self._bin()

        return None

    def replicate(self):
        """Forge a copy of the prism instance.

        Arguments:
            None

        Returns:
            Prism instance
        """

        # make a copy of the instance
        replica = copy(self)

        # deepcopy key attributes
        replica.route = deepcopy(self.route)
        replica.signifiers = deepcopy(self.signifiers)
        replica.shape = deepcopy(self.shape)

        return replica

    def squish(self, operation):
        """Reduce the vertical dimension by the chosen operation.

        Arguments:
            reducer: str

        Returns:
            Prism instance
        """

        # print('\nbefore reduction:')
        # print('horizontal: {}'.format(self.horizontal))
        # print('vertical: {}'.format(self.vertical))
        # print('distal: {}'.format(self.distal))
        # print('shape: {}'.format(self.shape))
        # print('layer: {}'.format(self.layer))
        # print('signifiers: {}'.format(self.signifiers))
        # print('name: {}'.format(self.name))
        # print('dimensions: {}'.format(self.dimensions))

        # create functions
        reducers = {}
        reducers['mean'] = lambda matrix, axis: self._average(matrix, axis)
        reducers['minimum'] = lambda matrix, axis: self._minimize(matrix, axis)
        reducers['maximum'] = lambda matrix, axis: self._maximize(matrix, axis)
        reducers['deviation'] = lambda matrix, axis: numpy.std(matrix, axis)

        # get reducer
        reducer = reducers[operation]

        # reduce the matrix
        matrix = self.tensor

        # get axes
        horizontal = self.horizontal
        vertical = self.vertical
        distal = self.distal

        # reduce matrix along horizontal
        matrix = reducer(matrix, horizontal)

        # reorient axes
        self.vertical = distal
        self.horizontal = vertical
        self.distal = distal - 1

        # reset zoom
        left, right, top, bottom = self.zoom
        left = top
        right = bottom
        top = 0
        bottom = self.shape[self.vertical]
        self.zoom = (left, right, top, bottom)

        # adjust signifiers
        signifiers = self.signifiers
        self.signifiers = signifiers[:horizontal] + signifiers[horizontal + 1:]

        # adjust layer
        layer = self.layer
        self.layer = layer[:horizontal] + layer[horizontal + 1:]

        # reduce dimensions
        self.dimensions = self.dimensions - 1

        # adjust name and shape
        #self.name = '{} ({})'.format(self.name.split('(')[0].strip(), operation)
        self.shape = matrix.shape
        self.tensor = matrix

        # print('\nafter reduction:')
        # print('horizontal: {}'.format(self.horizontal))
        # print('vertical: {}'.format(self.vertical))
        # print('distal: {}'.format(self.distal))
        # print('shape: {}'.format(self.shape))
        # print('layer: {}'.format(self.layer))
        # print('signifiers: {}'.format(self.signifiers))
        # print('name: {}'.format(self.name))
        # print('dimensions: {}'.format(self.dimensions))

        # polish
        self.polish()

        return None

    def splinter(self, axis):
        """Splinter the feature into one prism per index along an axis.

        Arguments:
            axis: int

        Returns:
            list of Prism instances
        """

        # get new prisms
        prisms = []
        for index in range(self.shape[axis]):

            # create copy of prism
            xerox = self.forge()

            # set name
            xerox['name'] = '{}_{}_{}'.format(self.name, self.signifiers[axis], index)

            # find appropriate route step
            steps = [number for number, _ in enumerate(xerox['route']) if len(xerox['route'][number]) > 1]
            step = steps[axis]

            # limit route to the one member
            xerox['route'][step] = [xerox['route'][step][index]]

            # correct shape
            shape = xerox['shape']
            shape = shape[:axis] + shape[axis + 1:]
            xerox['shape'] = shape

            # correct signifiers
            xerox['signifiers'] = xerox['signifiers'][:axis] + xerox['signifiers'][axis + 1:]

            # create prism
            prism = Prism(xerox, self.tree, self.bypass)
            prisms.append(prism)

        return prisms

    def view(self, scheme, zoom=None, spectrum='classic', suffix='', entitlement='', unity=''):
        """VIew a 2-D slice from the main matrix.

        Arguments:
            scheme: list of ints, str
            zoom: tuple of ints
            spectrum: str
            entitlement: str

        Returns:
            Plate instance
        """

        # set scene
        labels = self.tensor.dims
        matrix = self.tensor

        # apply manipulations
        for manipulation in self.manipulations:

            # apply
            matrix = manipulation(matrix)

        # link scene members to axis members
        links = [(label, plan) for label, plan in zip(labels, scheme)]

        # sort order with numbers up front and means at the back
        links.sort(key=lambda link: str(link[1]).startswith('horizontal'), reverse=True)
        links.sort(key=lambda link: str(link[1]).startswith('vertical'), reverse=True)
        links.sort(key=lambda link: str(link[1]).isdigit(), reverse=True)

        # get matrix
        order = [label for label, _ in links]
        matrix = matrix.transpose(*order)

        # go through operations
        for operation in self.reductions.keys():

            # get aggregate axes
            aggregates = [(label, plan) for label, plan in links if str(plan).startswith(operation)]
            if len(aggregates) > 0:

                # add fake arguments if necessary and strip them off
                reduction = aggregates[0][1] + '_!_!_!'
                operation, code, method, extra = reduction.split('_', 3)

                # make aggregates
                axes = [aggregate[0] for aggregate in aggregates]
                matrix = self.reductions[operation](matrix, axes, code, method)

        # get indices
        indices = [plan for _, plan in links if str(plan).isdigit()]

        # march down indices
        for index in indices:

            # march down indices
            matrix = matrix[index]

        # replace units with suffix
        if len(suffix) > 0:

            # add to units
            matrix.attrs['units'] = suffix

        # # get vertical and horizontal links
        # vertical = [link for link in links if str(link[1]).startswith('vertical')][0]
        # horizontal = [link for link in links if str(link[1]).startswith('horizontal')][0]

        # # process running averages etc.
        # if 'run' in horizontal[1]:
        #
        #     # perform running average
        #     data = self._run(data, horizontal[1])
        #
        # # process running averages etc.
        # if 'bin' in horizontal[1]:
        #
        #     # perform running average
        #     data = self._chunk(data, horizontal[1])

        # add address:
        address = self._address(scheme)

        # instantiate plate
        plate = Plate(matrix, scheme, zoom, address, spectrum, entitlement, unity)
        self.append(plate)

        return None