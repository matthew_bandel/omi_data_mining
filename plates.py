# plates.py for the Plate class to store one 2-D view of data

# import reload
from importlib import reload

# import math
import numpy
from numpy.random import random
from math import sin, cos, pi, log10, exp, floor, ceil, sqrt

# import pretty print
from pprint import pprint

# import datetime
from datetime import datetime, timedelta
from time import time

# import matplotlib for plots
from matplotlib import pyplot
from matplotlib import gridspec
from matplotlib import colorbar
from matplotlib import colors as Colors
from matplotlib import style as Style
from matplotlib import rcParams
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib import path as Path
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False

# import modules
from graphs import Graph


# class Prism to contain the particular data view information
class Plate(object):
    """Prism class to contain data orientation and view information.

    Inherits from:
        object
    """

    def __init__(self, matrix, scheme, zoom, address, spectrum='classic', entitlement='', unity=''):
        """Initialize a perspectives instance.

        Arguments:
            matrix: numpy array
            scheme: list of str int
            zoom: tuple of ints
            address: str
            spetrum: str

        Returns:
            Plate instance
        """

        # set current time
        self.now = time()

        # set data
        self.matrix = matrix

        # set scheme
        self.scheme = scheme
        self.address = address

        # set other attributes
        self.entitlement = entitlement
        self.unity = unity

        # set up zoom
        self.zoom = zoom
        if zoom is None:

            # set default zoom
            self.zoom = (0, len(self.matrix[0]), 0, len(self.matrix))

        # set default margin, resolution, and precision
        self.resolution = 10
        self.margin = 2
        self.precision = None

        # squeeze out the color palette
        self.palette = {}
        self.spectrum = spectrum
        self._squeeze()

        # calculate statistics
        self.mean = 0
        self.deviation = 0
        self.median = 0
        self.minimum = 0
        self.maximum = 0
        #self._calculate()

        # create graphs
        self.scatter = None
        self.bar = None
        self.head = None
        self.histogram = None
        self.tail = None
        #self._bin()

        return

    def __repr__(self):
        """Represent with string.

        Arguments:
            None

        Returns:
            str
        """

        # begin representation
        representation = {}

        # populate
        representation['shape'] = self.matrix.shape
        representation['labels'] = self.matrix.coords
        representation['zoom'] = self.zoom
        representation['scheme'] = self.scheme

        # pretty print
        pprint(representation)

        # construct fake string
        faux = '< Plate: {} >\n'.format(self.scheme)

        return faux

    def _address(self):
        """Create the address of the orientation.

        Arguments:
            None

        Returns:
            list of strings
        """

        # link labels with scheme
        links = [(label, plan) for label, plan in zip(self.matrix.labels, self.scheme)]
        digits = [(label, plan) for label, plan in links if str(label).isdigit()]
        reductions = [(label, plan) for label, plan in links if str(label) in self.reductions.keys()]

        # get feature name
        name = self.matrix.name

        # make location
        location = ''
        if len(digits) > 0:

            # join labels
            location += ', '.join(['{} {}'.format(label, plan) for label, plan in digits])

        # construct reduction
        reduction = ''
        if len(reductions) > 0:

            # find operation
            operation = [self._pick(plan, self.reductions.keys()) for _, plan in reductions][0]
            parameters = ' x '.join([label for label, _ in reductions])
            reduction += '{} over {}'.format(operation, parameters)

        # make address
        address = '{}:\n{}\n{}'.format(name, location, reduction)

        return reductions

    def _advance(self, *options):
        """Advance through distal layer.

        Arguments:
            *options: unpacked tuple of options

        Returns:
            None
        """

        # increase distal index
        layer = self.scheme[self.distal] + 1

        # check options
        if len(options) > 0:

            # set layer to option
            layer = int(options[0])

        # check against bounds
        if layer > self.shape[self.distal] - 1:

            # drop to zero
            layer = self.shape[self.distal]

        # set layer
        self.scheme[self.distal] = layer

        # refresh
        self.polish()

        return None

    def _average(self, matrix, axis):
        """Average the matrix along the axis.

        Arguments:
            matrix: numpy array
            axis: int, axis number

        Returns:
            numpy array
        """

        # take the average
        matrix = numpy.average(matrix, axis=axis)

        return matrix

    def _bin(self):
        """Sort data into bins for plotting a histogram and congealing colors.

        Arguments:
            None

        Returns:
            None

        Populates:
            self
        """

        # determine unique values
        data = numpy.array(self.matrix).ravel()
        uniques = self._skim(data)

        # if fewer than resolution, make a discrete plot
        if len(uniques) < self.resolution:

            # discretize
            self.discrete = True
            self._discretize(data, uniques)

        # otherwise consider it a continuous distribution
        else:

            # liquidate into a continuum
            self.discrete = False
            self._liquidate(data)

        return None

    def _calculate(self):
        """Calculate base statistics of the layer.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.mean
            self.deviation
            self.median
            self.minimum
            self.maximuum

        Returns:
            None
        """

        # ravel data into a list
        data = numpy.array(self.matrix).ravel()

        # perform statistics
        self.mean = numpy.average(data)
        self.deviation = numpy.std(data)
        self.median = numpy.percentile(data, 50)
        self.minimum = min(data)
        self.maximum = max(data)

        return None

    def _caliper(self, number):
        """Determine the precision of a floating point number.

        Arguments:
            number: float or int

        Returns:
            int
        """

        # convert to string
        string = str(number)

        # check for decimal place
        precision = 0
        if '.' in string:

            # precision is number of digits past the decimal
            precision = len(string.split('.')[1])

        return precision

    def _coordinate(self, labels, coordinates):
        """Swap generic labels for coordiinate labels.

        Arguments:
            labels: list of str, the given labels
            coordinates: list of float/int

        Returns:
            list of str
        """

        # abort if empty coordinates
        if len(coordinates) > 0:

            # create dummy indices
            dummy = list(range(len(coordinates)))
            for index, label in enumerate(labels):

                # try to
                try:

                    # swap with a coordinate entry
                    labels[index] = str(int(coordinates[(int(label))]))

                # otherwise
                except IndexError:

                    # nevermind
                    pass

        return labels

    def _dip(self, data, axis):
        """Dip the quill into the right marker size and style.

        Arguments:
            data: numpy array
            axis: mattplotlib Axes instance

        Returns:
            int, str tuple
        """

        # unpack axis
        density = axis.get_figure().dpi
        inches = axis.get_figure().get_size_inches()
        position = axis.get_position()
        left, right, bottom, top = [position.x0, position.x1, position.y0, position.y1]

        # get height and width of a single point
        width = (right - left) * inches[0] * density / len(data[0])
        height = (top - bottom) * inches[1] * density / len(data)

        # determine size scale by the biggest of the width / height ratio
        scale = max([(width / height), (height / width)])

        # make custom marker
        codes = [Path.Path.MOVETO, Path.Path.LINETO, Path.Path.LINETO, Path.Path.LINETO, Path.Path.CLOSEPOLY]
        corners = [[width * x, height * y] for x, y in [(1, 1), (1, -1), (-1, -1), (-1, 1), (1, 1)]]

        # determine normalization factor
        factor = max([width, height])
        corners = [[coordinate / factor for coordinate in point] for point in corners]

        # normalize to unit cell
        style = Path.Path(corners, codes)

        # calculate marker size, factoring width, height, scale, and relation of ppi to dpi
        inset = 0.98
        size = width * height * scale * inset * (72 / density) ** 2

        # check size
        if size < 1:

            # set size
            size = 1
            style = ','

        return size, style

    def _discretize(self, data, uniques):
        """Bin using a discrete mode.

        Arguments:
            data: numpy array
            uniques: list of floats, ints

        Returns:
            None

        Populates:
            self
        """

        # begin graphs
        scatter = Graph()
        bar = Graph()
        head = Graph()
        histogram = Graph()
        tail = Graph()

        # set size
        size = len(uniques)

        # estimate precision
        if self.precision is None:

            # get the maxium precision amongst uniques
            self.precision = max([self._caliper(unique) for unique in uniques])

        # get the middle between each pair of unique values for the binning boundaries
        bounds = [(left + right) / 2 for left, right in zip(uniques[:-1], uniques[1:])]

        # and cap off each end
        bounds = [uniques[0] - 1] + bounds + [uniques[-1] + 1]

        # populate bins
        bins = []
        for left, right in zip(bounds[:-1], bounds[1:]):

            # populate bins
            bin = [index for index, datum in enumerate(data.data) if left <= datum <= right]
            bins.append(bin)

        # get left and right bin edges
        left = numpy.array(bounds[:-1])
        right = numpy.array(bounds[1:])

        # replicate data for each bin to have a set
        matrix = numpy.array([data[:] for _ in left])
        matrix = numpy.swapaxes(matrix, 0, 1)

        # create a boolean mask and get indices
        mask = (matrix >= left) & (matrix <= right)

        # make bins the mask
        scatter.bins = mask

        # construct faux boundaries and ticks
        boundaries = [index for index in range(size + 1)]
        ticks = [index + 0.5 for index in range(size)]

        # construct tick labels
        labels, font = self._typeset(uniques)

        # construct color mixing ratios, slightly altered if only one value is present
        ratios = [(index + int(size < 2)) / max([1, (size - 1)]) for index in range(size)]

        # populate scatter plot
        #scatter.bins = bins
        scatter.bounds = boundaries
        scatter.ticks = ticks
        scatter.labels = labels
        scatter.ratios = ratios
        scatter.font = font

        # create scatter plot labels
        scatter.title = self._entitle()
        scatter.independent = self.matrix.dims[1]
        scatter.dependent = self.matrix.dims[0]

        # populate color bar
        bar.bins = bins
        bar.bounds = boundaries
        bar.ticks = ticks
        bar.labels = labels
        bar.ratios = ratios
        bar.font = int(1.2 * font)
        bar.independent = '{} {}'.format(self.matrix.units, self.address)

        # if there are only one or two unique values
        if size < 3:

            # add faux outliers
            bins = [[]] + bins + [[]]
            boundaries = [boundaries[0] - 1] + boundaries + [boundaries[-1] + 1]
            ticks = [ticks[0] - 1] + ticks + [ticks[-1] + 1]
            labels = [' '] + labels + [' ']
            ratios = [0.0] + ratios + [1.0]

        # set all bins
        head.bins = bins[:1]
        histogram.bins = bins[1:-1]
        tail.bins = bins[-1:]

        # set all bounds
        head.bounds = boundaries[:2]
        histogram.bounds = boundaries[1:-1]
        tail.bounds = boundaries[-2:]

        # set all tick mark locations
        head.ticks = ticks[:1]
        histogram.ticks = ticks[1:-1]
        tail.ticks = ticks[-1:]

        # set tick mark labels
        head.labels = labels[:1]
        histogram.labels = labels[1:-1]
        tail.labels = labels[-1:]

        # set lower outlier bar dimensions
        head.width = 1.0
        head.middles = ticks[:1]
        head.heights = [len(bin) for bin in bins[:1]]

        # set main bar dimensions
        histogram.width = 1.0
        histogram.middles = ticks[1:-1]
        histogram.heights = [len(bin) for bin in bins[1:-1]]

        # set upper outlier bar dimensions
        tail.width = 1.0
        tail.middles = ticks[-1:]
        tail.heights = [len(bin) for bin in bins[-1:]]

        # add faux curve to main histogram
        histogram.curve = [[0, 0], [0, 0]]

        # set color mixing ratios
        head.ratios = ratios[:1]
        histogram.ratios = ratios[1:-1]
        tail.ratios = ratios[-1:]

        # set lower outlier scale
        head.left = boundaries[0]
        head.right = boundaries[1]
        head.top = max([1, max(head.heights)])

        # set main histogram scale
        histogram.left = boundaries[1]
        histogram.right = boundaries[-2]
        histogram.top = max([1, max(histogram.heights)])

        # set upper outlier scale
        tail.left = boundaries[-2]
        tail.right = boundaries[-1]
        tail.top = max([1, max(tail.heights)])

        # format statistics
        statistics = [self.minimum, self.median, self.mean, self.deviation, self.maximum]
        statistics = [self._round(statistic) for statistic in statistics]
        statistics, _ = self._typeset(statistics)

        # set lower outlier labels
        head.font = font
        head.dependent = 'counts'
        head.independent = 'min: {}'.format(statistics[0])

        # set main histogram labels
        histogram.font = font
        histogram.independent = 'median: {}, mean {}, stdev: {}'.format(*statistics[1:4])

        # set upper outlier labels
        tail.font = font
        tail.independent = 'max: {}'.format(statistics[4])

        # set graph attributes
        self.scatter = scatter
        self.bar = bar
        self.head = head
        self.histogram = histogram
        self.tail = tail

        return None

    def _entitle(self):
        """Create a title from the route.

        Arguments:
            None

        Returns:
            None
        """

        # break route into 4 parts
        route = str(self.matrix.route).replace('[', '').replace(']', '')

        # # split route into 4 parts
        # length = len(route)
        # chunk = ceil(length / 3)
        #
        # # create title
        # pieces = [str(route)[chunk * index: chunk + chunk * index] for index in range(3)]
        # title = '\n'.join(pieces)

        return route

    def _grab(self, route, *indices):
        """Grab the data by following the record's route and indices.

        Arguments:
            route: list of str
            *indices: unpacked list of ints

        Returns:
            numpy array
        """

        # get the data source at the route
        source = self._walk(self.tree, *route)

        # translate into a matrix
        matrix = numpy.array(source)

        # assume skipping first index if it is one
        if len(matrix.shape) > 1 and len(matrix) == 1:

            # step down
            matrix = matrix[0]

        # go through all indices
        for index in indices:

            # unpack
            matrix = matrix[index]

        return matrix

    def _liquidate(self, data):
        """Bin data using a continuous mode.

        Arguments:
            data: numpy array

        Returns:
            None

        Populates:
            self.histogram attributes
        """

        # begin graphs
        scatter = Graph()
        bar = Graph()
        head = Graph()
        histogram = Graph()
        tail = Graph()

        # get initial lower and upper outlier boundaries
        lower = numpy.percentile(data, self.margin)
        upper = numpy.percentile(data, 100 - self.margin)

        # estimate precision
        if self.precision is None:

            # determine preliminary width from data within margins
            width = (upper - lower) / (self.resolution - 2)

            # check for width
            if width > 0:

                # get precision from width
                self.precision = -floor(log10(width))

            # otherwise determine from max and min
            else:

                # use log of average
                width = numpy.max(data) - numpy.min(data)
                self.precision = -floor(log10(width))

        # round boundaries to precision
        lower = self._round(lower, self.precision, up=False)
        upper = self._round(upper, self.precision, up=True)

        # correct upper in case of zero (rounding edge case)
        if upper <= lower:

            # add one unit
            upper += 10 ** -self.precision

        # determine histogram bounds
        width = (upper - lower) / (self.resolution - 2)
        histogram.width = width
        histogram.bounds = [lower + width * index for index in range(self.resolution - 1)]

        # get lower outlier bounds
        lowers = []
        minimum = self._round(min(data), self.precision, up=False)
        width = (lower - minimum) / (self.resolution - 2)
        if width > 0:

            # calculate lower bounds
            lowers = [minimum + index * width for index in range(self.resolution - 1)]

        # check for empty lowers
        if len(lowers) < 1:

            # set lowers
            lowers = [histogram.bounds[0] - histogram.width, histogram.bounds[0]]

        # set attributes
        head.width = width
        head.bounds = lowers

        # construct upper outlier bounds
        uppers = []
        maximum = self._round(max(data), self.precision, up=True)
        width = (maximum - upper) / (self.resolution - 2)
        if width > 0:

            # calculate upper bounds
            uppers = [upper + index * width for index in range(self.resolution - 1)]

        # check for empty lowers
        if len(uppers) < 1:

            # set lowers
            uppers = [histogram.bounds[-1], histogram.bounds[-1] + histogram.width]

        # set attributes
        tail.width = width
        tail.bounds = uppers

        # populate histograms
        for graph in (head, histogram, tail):

            # get bounds
            bounds = graph.bounds

            # create edges from bounds
            left = numpy.array(bounds[:-1])
            right = numpy.array(bounds[1:])

            # replicate data for the data matrix
            matrix = numpy.array([data[:] for _ in left])
            matrix = numpy.swapaxes(matrix, 0, 1)

            # create a boolean mask and get indices
            mask = (matrix >= left) & (matrix <= right)

            # make bins the mask
            graph.bins = mask

            # make bar coordinates
            graph.middles = (left + right) / 2
            graph.heights = numpy.sum(mask, axis=0)

            # set limits
            graph.left = bounds[0]
            graph.right = bounds[1]
            graph.top = max([1, max(graph.heights)])

        # collect ticks
        ticks = [head.bounds[0]] + histogram.bounds + [tail.bounds[-1]]

        # make labels based on shortest format
        labels, font = self._typeset(ticks)

        # set head ticks and labels
        head.font = font
        head.ticks = ticks[:2]
        head.labels = labels[:2]

        # set main histogram ticks and labels
        histogram.font = font
        histogram.ticks = ticks[1:-1]
        histogram.labels = labels[1:-1]

        # set tail ticks and labels
        tail.font = font
        tail.ticks = ticks[-2:]
        tail.labels = labels[-2:]

        # format statistics
        statistics = [self.minimum, self.median, self.mean, self.deviation, self.maximum]
        statistics = [self._round(statistic) for statistic in statistics]
        statistics, _ = self._typeset(statistics)

        # set lower outlier labels
        head.dependent = 'counts'
        head.independent = 'min: {}'.format(statistics[0])

        # set main histogram labels
        histogram.independent = 'median: {}, mean {}, stdev: {}'.format(*statistics[1:4])

        # set upper outlier labels
        tail.independent = 'max: {}'.format(statistics[4])

        # calculate area and height of equivalent normal curve
        area = sum(histogram.heights) * histogram.width / (1 - 0.02 * self.margin)
        normalization = 1 / sqrt(2 * pi * self.deviation ** 2)

        # calculate normal curve
        xs = [histogram.bounds[0] + histogram.width * index / 10 for index in range(10 * len(histogram.bounds) + 1)]
        ys = [area * normalization * exp(-0.5 * ((x - self.mean) / self.deviation) ** 2) for x in xs]
        histogram.curve = [xs, ys]

        # congeal histogram bins for colorbar and scatterplot
        bounds = [head.bounds[0]] + histogram.bounds + [tail.bounds[-1]]

        # get left and right bin edges
        left = numpy.array(bounds[:-1])
        right = numpy.array(bounds[1:])

        # replicate data for each bin to have a set
        matrix = numpy.array([data[:] for _ in left])
        matrix = numpy.swapaxes(matrix, 0, 1)

        # create a boolean mask and get indices
        mask = (matrix >= left) & (matrix <= right)

        # make bins the mask
        scatter.bins = mask

        # set scatterplot attributes
        scatter.bounds = bounds
        scatter.ticks = bounds
        scatter.labels = labels
        scatter.font = font

        # create scatter plot title
        scatter.title = self._entitle()
        scatter.independent = self.matrix.dims[1]
        scatter.dependent = self.matrix.dims[0]

        # set colorbar attributes
        bar.bounds = bounds
        bar.ticks = bounds
        bar.labels = labels
        bar.font = int(1.2 * font)
        bar.independent = '{}'.format(self.matrix.units)

        # calculate colorbar mixing ratios
        ratios = [index / (len(bounds) - 2) for index in range(len(bounds) - 1)]
        bar.ratios = ratios
        scatter.ratios = ratios
        head.ratios = ratios[0:1]
        histogram.ratios = ratios[1:-1]
        tail.ratios = ratios[-1:]

        # set graph attributes
        self.scatter = scatter
        self.bar = bar
        self.head = head
        self.histogram = histogram
        self.tail = tail

        return None

    def _minimize(self, matrix, axis):
        """Reduce the matrix by replacing the axis with the minimum along that axis.

        Arguments:
            matrix: numpy array
            axis: int, axis number

        Returns:
            numpy array
        """

        # minimize the matrix
        matrix = numpy.min(matrix, axis=axis)

        return matrix

    def _maximize(self, matrix, axis):
        """Reduce the matrix by replacing the axis with the maximum along that axis.

        Arguments:
            matrix: numpy array
            axis: int, axis number

        Returns:
            numpy array
        """

        # minimize the matrix
        matrix = numpy.max(matrix, axis=axis)

        return matrix

    def _mix(self, ratio, *points):
        """Mix a color based on a ratio color points.

        Arguments:
            ratio: float
            *points: unpacked tuple of RGB triplets

        Returns:
            list of floats
        """

        # calculate all distances
        measuring = lambda point, pointii: sqrt(sum([(pointii[index] - point[index]) ** 2 for index in range(3)]))
        distances = [measuring(point, pointii) for point, pointii in zip(points[:-1], points[1:])]

        # normalize distances
        total = sum(distances)
        distances = [distance / total for distance in distances]

        # determine ticks and check for inclusion
        ticks = [0.0 + sum(distances[:index]) for index, _ in enumerate(distances)] + [1.0]
        checks = [tick <= ratio <= tickii for tick, tickii in zip(ticks[:-1], ticks[1:])]

        # get index of first
        index = checks.index(True)

        # mix color
        color = []
        for channel in zip(points[index], points[index + 1]):

            # get intensity
            intensity = ((ratio - ticks[index]) / (ticks[index + 1] - ticks[index])) * (channel[1] - channel[0]) + channel[0]
            color.append(intensity)

        return color

    def _orient(self):
        """Orient the layer in accordance with axes.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.scheme
        """

        # start with full matrix
        data = self.matrix

        # go through each placement
        slices = []
        for place in range(self.dimensions):

            # check for horizontal
            if place != self.horizontal and place != self.vertical:

                # add to slice
                slices.append((self.scheme[place], self.scheme[place] + 1))

            # otherwise take full layer
            else:

                # get full layer
                slices.append((0, self.shape[place]))

        # make slices
        for axis, slice in enumerate(slices):

            # make slice
            data = data.take(indices=range(*slice), axis=axis)

        # remove unnecessary ones
        sizes = (entry for entry in data.shape if entry != 1)
        data = data.reshape(*sizes)

        # set horizontal and vertical
        if self.vertical > self.horizontal:

            # swap axes
            data = numpy.swapaxes(data, 0, 1)

        # set data
        self.matrix = data

        return None

    def _pick(self, offer, options):
        """Pick the closest match to an offer from a list of options.

        Arguments:
            pffer: str
            options: list of str (or generator?)

        Returns:
            str
        """

        # check for direct present
        best = offer
        if best not in options:

            # calculate fuzzy matchies
            fuzzies = [(option, fuzz.ratio(offer.lower(), option.lower())) for option in options]
            fuzzies.sort(key=lambda score: score[1], reverse=True)

            # find the best match
            best = fuzzies[0][0]

        return best

    def _populate(self, feature):
        """Populate self with feature.

        Arguments:
            feature: dict

        Returns:
            None

        Populates:
            self
        """

        # go through each feature
        for field, info in feature.items():

            # populate self
            self[field] = info

        return None

    def _retreat(self, *options):
        """Retreat through distal layer.

        Arguments:
            *options: unpacked tuple of options

        Returns:
            None
        """

        # increase distal index
        layer = self.scheme[self.distal] - 1

        # check options
        if len(options) > 0:

            # set layer to option
            layer = int(options[0])

        # correct layer
        if layer < 0:

            # drop to zero
            layer = 0

        # set layer
        self.scheme[self.distal] = layer

        # refresh
        self.polish()

        return None

    def _round(self, number, precision=None, up=False):
        """Round the number to a particular precision.

        Arguments:
            number: float or int
            precision: int
            up=False: round up?

        Returns:
            float or int
        """

        # duck overflow errors
        rounded = number
        try:

            # default precision
            precision = precision or self.precision

            # shift decimal places
            shift = number * 10 ** precision
            rounded = int(shift)

            # round it up
            if up:

                # round it up
                rounded = rounded + (int(rounded < shift))

            # or round down
            if not up:

                # round it down
                rounded = rounded - (int(rounded > shift))

            # lower back down
            rounded = rounded / 10 ** precision

            # convert to int
            if precision <= 0:

                # convert to int
                rounded = int(rounded)

        # skip if infinity
        except OverflowError:

            # pass
            pass

        return rounded

    def _skim(self, members):
        """Skim off the unique members from a list.

        Arguments:
            members: list

        Returns:
            list
        """

        # trim duplicates and sort
        members = list(set(members))
        members.sort()

        return members

    def _siphon(self):
        """Siphon data into the prism from the data tree.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.matrix
        """

        # get the route and current layer
        route = self.route

        # assemble individual data branches from route
        branches = [[]]
        for knot in route:

            # make sure to include all knots
            stack = []
            for step in knot:

                # append to each branch in the reservoir
                for branch in branches:

                    # add each combination
                    stack.append(branch + [step])

            # reset branches
            branches = stack

        # collect matrices
        matrices = [self._grab(branch) for branch in branches]
        collections = [(branch, matrix) for branch, matrix in zip(branches, matrices)]

        # combine the collections
        if len(set([collection[0][-1] for collection in collections])) > 1:

            # combine
            collections = self._combine(collections)

        # go through each orbit
        matrices = []
        branches = []
        for orbit in route[0]:

            # go through each band
            for band in route[1]:

                # collect all modes
                modes = [collection[1] for collection in collections if collection[0][0] == orbit and collection[0][1] == band]
                twigs = [collection[0][:2] for collection in collections if collection[0][0] == orbit and collection[0][1] == band]

                # get shape
                shape = modes[0].shape
                shape = (-1,) + shape

                # reshape to add dimension
                modes = [mode.reshape(*shape) for mode in modes]
                matrix = numpy.concatenate(modes, axis=0)
                matrices.append(matrix)

                # add twig to branches
                branches.append(twigs[0])

        # go through each orbit
        collections = [(branch, matrix) for branch, matrix in zip(branches, matrices)]
        matrices = []
        branches = []
        for orbit in route[0]:

            # collect all modes
            bands = [collection[1] for collection in collections if collection[0][0] == orbit]
            twigs = [collection[0][:1] for collection in collections if collection[0][0] == orbit]

            # get shape
            shape = bands[0].shape
            shape = (-1,) + shape

            # reshape to add dimension
            bands = [band.reshape(*shape) for band in bands]
            matrix = numpy.concatenate(bands, axis=0)
            matrices.append(matrix)

            # add twig to branches
            branches.append(twigs[0])

        # combine orbits
        collections = [(branch, matrix) for branch, matrix in zip(branches, matrices)]
        orbits = [collection[1] for collection in collections]

        # get shape
        shape = orbits[0].shape
        shape = (-1,) + shape

        # remove ones from shape
        shape = tuple([entry for entry in shape if entry != 1])

        # reshape to add dimension
        orbits = [orbit.reshape(*shape) for orbit in orbits]
        matrix = numpy.concatenate(orbits, axis=0)

        # set matrix
        self.matrix = matrix

        return None

    def _squeeze(self):
        """Squeeze out the color palettes.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.palette
        """

        # define classic spectrum
        dark = [0.0, 0.0, 0.3]
        blue = [0.0, 0.0, 0.7]
        green = [0.0, 0.8, 0.0]
        yellow = [1.0, 1.0, 0.0]
        self.palette['classic'] = [dark, blue, green, yellow]

        # define hot spectrum
        black = [0.0, 0.0, 0.0]
        blue = [0.0, 0.0, 1.0]
        magenta = [1.0, 0.0, 1.0]
        white = [1.0, 1.0, 1.0]
        self.palette['hot'] = [black, blue, magenta, white]

        # define cool spectrum
        black = [0.0, 0.0, 0.0]
        blue = [0.0, 0.0, 1.0]
        cyan = [0.0, 1.0, 1.0]
        white = [1.0, 1.0, 1.0]
        self.palette['cool'] = [black, blue, cyan, white]

        # define rainbow spectrum
        red = [1.0, 0.0, 0.0]
        yellow = [1.0, 1.0, 0.0]
        green = [0.0, 1.0, 0.0]
        cyan = [0.0, 1.0, 1.0]
        blue = [0.0, 0.0, 1.0]
        violet = [0.5, 0.0, 0.5]
        self.palette['rainbow'] = [red, yellow, green, cyan, blue, violet]

        # define acid spectrum
        yellow = [1.0, 1.0, 0.0]
        green = [0.0, 1.0, 0.0]
        forest = [0.0, 0.5, 0.5]
        blue = [0.0, 0.2, 1.0]
        violet = [0.7, 0.0, 1.0]
        magenta = [1.0, 0.0, 1.0]
        self.palette['acid'] = [yellow, green, forest, blue, violet, magenta]

        # define acid spectrum
        red = [1.0, 0.0, 0.0]
        orange = [0.8, 0.2, 0.0]
        yellow = [0.8, 0.8, 0.0]
        green = [0.0, 1.0, 0.0]
        forest = [0.0, 0.5, 0.5]
        blue = [0.0, 0.2, 1.0]
        cyan = [0.0, 1.0, 1.0]
        violet = [0.5, 0.2, 1.0]
        navy = [0.0, 0.0, 0.2]
        self.palette['lands'] = [red, yellow, orange, green, blue, cyan, forest, violet, navy]

        return None

    def _stamp(self, message):
        """Start timing a block of code, and print results with a message.

        Arguments:
            message: str

        Returns:
            None
        """

        # get final time
        final = time()

        # calculate duration and reset time
        duration = round(final - self.now, 5)
        self.now = final

        # print duration
        print('took {} seconds.'.format(duration))

        # begin new block
        print(message)

        return None

    def _tilt(self):
        """Tilt the vertical axis into the next distal one.

        Arguments:
            None

        Returns:
            None
        """

        # vertical becomes distal
        distal = self.vertical

        # find next distal
        while distal in (self.vertical, self.horizontal):

            # increase vertical
            distal += -1
            if distal < 0:

                # set to zero
                distal = self.dimensions - 1

        # set vertical axis
        self.vertical = distal

        # get next distal layer
        while distal in (self.vertical, self.horizontal):

            # increase vertical
            distal += -1
            if distal < 0:

                # set to zero
                distal = self.dimensions - 1

        # set distal
        self.distal = distal

        # correct zoom
        left, right, top, bottom = self.zoom
        top = 0
        bottom = self.shape[self.vertical]
        self.zoom = (left, right, top, bottom)

        # refresh prism
        self.polish()

        return None

    def _twist(self):
        """Twist the horizontal and vertical axes.

        Arguments:
            None

        Returns:
            None
        """

        # twist axes
        horizontal = self.horizontal
        self.horizontal = self.vertical
        self.vertical = horizontal

        # unpack zoom
        left, right, top, bottom = self.zoom

        # adjust zoom
        self.zoom = (top, bottom, left, right)

        # reorient
        self.polish()

        return None

    def _typeset(self, ticks):
        """Determine the labels and fonts for the graph ticks.

        Arguments:
            ticks: list of floats, ints

        Returns:
            None
        """

        # convert ticks to floats based on the precision
        precision = max([0, self.precision])
        format = '%.' + str(precision) + 'f'
        decimals = [format % tick for tick in ticks]

        # correct decimals for integers
        if all([int(tick) == tick for tick in ticks]):

            # convert all labels to integers
            decimals = [decimal.split('.')[0] for decimal in decimals]

        # convert to single place exponentials
        exponentials = ['%.1e' % tick for tick in ticks]

        # sort by total length
        options = [decimals, exponentials]
        options.sort(key=lambda strings: len(''.join(strings)))
        labels = options[0]

        # determine font size
        length = len(''.join(labels))
        font = min([10, int(500 / length)])

        return labels, font

    def _walk(self, node, *fields, show=False):
        """walk through a dict by multiple fields.

        Arguments:
            node: dict or h5
            #fields: unpacked tuple of key names
            show=False: boolean, show the fields picked?

        Returns:
            dict or h5
        """

        # set contents to node
        contents = node

        # go through each step
        for field in fields:

            # try to
            try:

                # get the best match
                best = self._pick(field, contents.keys())

                # print
                if show:

                    # print
                    print(best)

                # select new node
                contents = contents[best]

            # unless already the data
            except AttributeError:

                # get names
                names = list(contents.dtype.names)
                index = names.index(field)

                # get contents
                contents = [content[index] for content in contents[0]]

        return contents

    def dice(self, dimensions):
        """Dice the feature into fractions by unique values.

        Arguments:
            dimensions: int, number of dimensions to use

        Returns:
            list of Prism instances
        """

        # get all unique values
        uniques = numpy.unique(self.matrix)

        print(self.name)
        print(uniques)

        # go through each value
        replicas = []
        for unique in uniques:

            # forge a replica
            replica = self.forge()

            # make boolean mask for unique value
            mask = replica.matrix == unique

            # determine total number of elements
            elements = numpy.prod(mask.shape[-dimensions:])

            # sum along lowest dimensions
            for _ in range(dimensions):

                # sum
                mask = numpy.sum(mask, axis=-1)

            # divide by total number of elements and convert to percents
            mask = numpy.divide(mask, elements)
            mask = numpy.multiply(mask, 100)

            # reset attributes
            replica.matrix = mask
            replica.shape = mask.shape
            replica.dimensions = len(mask.shape)
            replica.signifiers = replica.signifiers[:-dimensions]
            replica.layer = replica.layer[:-dimensions]
            replica.name += '_%{}'.format(unique)
            replica.horizontal = len(mask.shape) - 1
            replica.vertical = len(mask.shape) - 2
            replica.distal = len(mask.shape) - 3

            # append
            replicas.append(replica)

        return replicas

    def forge(self):
        """Forge a copy of the prism instance.

        Arguments:
            None

        Returns:
            Prism instance
        """

        # make a copy of the instance
        forgery = copy(self)

        # deepcopy key attributes
        forgery.route = deepcopy(self.route)
        forgery.layer = deepcopy(self.scheme)
        forgery.signifiers = deepcopy(self.signifiers)
        forgery.shape = deepcopy(self.shape)
        forgery.shape = deepcopy(self.shape)
        forgery.zoom = deepcopy(self.zoom)
        forgery.matrix = numpy.copy(self.matrix)

        return forgery

    def glance(self, deposit='../plots/glance.png', slide=None):
        """Project 2-D plot.

        Arguments:
            deposit: str, path for depositing image
            slide=None: str, title for a slide

        Returns:
            None
        """

        # caculate data
        self._calculate()
        self._bin()

        # begin plot
        pyplot.clf()
        figure = pyplot.figure()
        figure.set_size_inches(20, 15)

        # create a grid for combining subplots
        grid = figure.add_gridspec(3, 3, width_ratios=[2, 10, 2], height_ratios=[20, 1, 6])

        # the first axis holds the main 2-D scatter plot, the second holds the colorbar
        axes = [figure.add_subplot(grid[0, :])]
        axes.append(figure.add_subplot(grid[1, :]))

        # the next three house the head, main, and tail histograms
        axes.append(figure.add_subplot(grid[2, 0]))
        axes.append(figure.add_subplot(grid[2, 1]))
        axes.append(figure.add_subplot(grid[2, 2]))

        # adjust margins
        figure.subplots_adjust(hspace=0.7, wspace=0.5)

        # add the title and labels to main scatter plot
        axis = axes[0]
        title = self.entitlement or self.scatter.title
        axis.set_title('{}'.format(title), fontsize=20)
        axis.set_xlabel(self.scatter.independent, fontsize=20)
        axis.set_ylabel(self.scatter.dependent, fontsize=20)

        # get the matrix from the prism and zoom into the spot
        matrix = self.matrix

        # transpose bins
        bins = self.scatter.bins.transpose()

        # set default zoom
        left = 0
        right = len(matrix[0])
        top = 0
        bottom = len(matrix)

        # make row and column index matrices
        rows = numpy.array([numpy.full((right - left,), number) for number in range(top, bottom)])
        columns = numpy.array([numpy.linspace(left, right - 1, right - left) for _ in matrix])

        # get horizontal and vertical coordinates
        verticals = numpy.array([rows.ravel()[:]] * bins.shape[0])
        horizontals = numpy.array([columns.ravel()[:]] * bins.shape[0])

        # zoom into matrix
        left, right, top, bottom = self.zoom
        matrix = matrix[top:bottom, left:right]

        # set scatter plot limits
        axis.set_xlim(left - 0.5, right - 0.5)
        axis.set_ylim(top - 0.5, bottom - 0.5)

        # pick spectrum
        spectrum = self.palette[self.spectrum]

        # mix colors
        colors = [self._mix(ratio, *spectrum) for ratio in self.bar.ratios]

        # determine marker size and styles
        size, style = self._dip(matrix, axis)

        # plot each subset
        for horizontal, vertical, bin, color in zip(horizontals, verticals, bins, colors):

            # plot subset of points
            axis.scatter(horizontal[bin], vertical[bin], color=color, s=size, marker=style)

        # add xticks
        ticks = [tick for tick in axis.get_xticks() if tick == int(tick)]
        labels = [str(tick).split('.')[0] for tick in ticks]

        # replace with coordinates?
        coordinates = matrix.coords[matrix.dims[1]]
        labels = self._coordinate(labels, coordinates)

        # add ticks and labels
        axis.set_xticks(ticks)
        axis.set_xticklabels(labels, fontsize=20)

        # add y ticks
        ticks = [tick for tick in axis.get_yticks() if tick == int(tick)]
        labels = [str(tick).split('.')[0] for tick in ticks]

        # replace with coordinates?
        coordinates = matrix.coords[matrix.dims[0]]
        labels = self._coordinate(labels, coordinates)

        # set y ticks and labels
        axis.set_yticks(ticks)
        axis.set_yticklabels(labels, rotation=90, va='center', fontsize=20)

        # set scatter plot limits
        axis.set_xlim(left - 0.5, right - 0.5)
        axis.set_ylim(top - 0.5, bottom - 0.5)

        # set up color bar
        bar = Colors.ListedColormap(colors)
        bounds = self.bar.bounds
        ruler = Colors.BoundaryNorm(bounds, len(colors), clip=True)

        # add to second axis
        axis = axes[1]
        parameters = {'cmap': bar, 'norm': ruler, 'boundaries': bounds, 'ticks': self.bar.ticks}
        parameters.update({'spacing': 'uniform', 'orientation': 'horizontal'})
        legend = colorbar.ColorbarBase(axis, **parameters)

        # label colorbar with units
        units = self.unity or self.bar.independent
        axis.tick_params(axis='x', which='major', labelsize=self.bar.font)
        axis.set_xlabel(units, fontsize=20)
        legend.ax.set_xticklabels(self.bar.labels, fontsize=20)

        # set up histograms
        graphs = [self.head, self.histogram, self.tail]
        for axis, graph in zip(axes[2:], graphs):

            # set x axis properties
            axis.set_xlim(graph.left, graph.right)
            axis.set_xticks(graph.ticks)
            axis.set_xticklabels(graph.labels, fontsize=20)
            axis.set_xlabel(graph.independent, fontsize=20)
            axis.tick_params(axis='x', which='major', labelsize=graph.font)

            # set y axis properties
            axis.set_ylim(0, graph.top)
            axis.set_ylabel(graph.dependent, fontsize=20)

        # plot lower outliers
        color = self._mix(self.head.ratios[0], *spectrum)
        axes[2].bar(self.head.middles, self.head.heights, self.head.width, color=color, edgecolor='black', linewidth=0.2)

        # plot main histogram
        colors = [self._mix(ratio, *spectrum) for ratio in self.histogram.ratios]
        for middle, height, color in zip(self.histogram.middles, self.histogram.heights, colors):

            # plot bar
            axes[3].bar([middle], [height], self.histogram.width, color=color, edgecolor='black', linewidth=0.2)

        # plot normal curve
        axes[3].plot(*self.histogram.curve, 'k--')

        # plot upper outlier histogram
        color = self._mix(self.tail.ratios[0], *spectrum)
        axes[4].bar(self.tail.middles, self.tail.heights, self.tail.width, color=color, edgecolor='black', linewidth=0.2)

        # check for slide setting
        if slide:

            # retitle axis
            axis = axes[0]
            route = self.matrix.route
            band = route[2][0]
            number = str(int(route[0][0].split('-o')[1]))
            date = str(datetime.strptime(route[0][0].split('-o')[0], '%Ym%m%dt%H%M'))

            # make title
            title = '{}, orbit number {}, ({})'.format(band, number, date)
            axis.set_title(title, fontsize=30)

            # save with cutoutbox
            extent = axis.get_window_extent().transformed(figure.dpi_scale_trans.inverted())
            pyplot.savefig(deposit, bbox_inches=extent.expanded(1.3, 1.5))
            pyplot.clf()

        # otherwise
        else:

            # save full plot to screen
            pyplot.savefig(deposit)
            pyplot.clf()

        return None

    def polish(self):
        """Recalculate all bins and data based on new orientation.

        Arguments:
            None

        Returns:
            None
        """

        # recalculate and orient
        self._orient()
        self._calculate()
        self._bin()

        return None

    def sketch(self, start, finish):
        """Sketch a line plot of the data in the prism.

        Arguments:
            start: vertical position to begin sketch
            finish: vertical position to stop sketch

        Returns:
            None
        """

        # get data
        data = self.matrix

        # set the colors
        black = [0.0, 0.0, 0.0]
        dark = [0.25, 0.0, 0.5]
        light = [0.5, 0.0, 1.0]
        colors = [light, dark, black]

        # get indices
        indices = numpy.array([index for index in range(data.shape[1])])

        # get each lines attributes
        field = self.scatter.dependent
        scratches = [(indices, data[position], colors[0], 1.0, '{} {}'.format(field, position)) for position in range(start, finish)]

        # begin plot
        pyplot.clf()
        figure = pyplot.figure()
        figure.set_size_inches(20, 12)

        # create a grid for combining subplots
        grid = figure.add_gridspec(ncols=1, nrows=3, width_ratios=[1], height_ratios=[2, 10, 2])

        # the next three house the head, main, and tail histograms
        axes = [figure.add_subplot(grid[0, :])]
        axes.append(figure.add_subplot(grid[1, :]))
        axes.append(figure.add_subplot(grid[2, :]))

        # adjust margins
        figure.subplots_adjust(hspace=0.3, wspace=0.5)

        # determine all entries
        data = [float(datum) for scratch in scratches for datum in scratch[1].data]
        quantile = 2

        # get all points
        maximum = max(data)
        head = numpy.percentile(data, 100 - quantile)
        tail = numpy.percentile(data, quantile)
        minimum = min(data)

        # make plot
        scratches.reverse()
        for index, scratch in enumerate(scratches):

            # unpack scratch
            indices, line, color, opacity, label = scratch

            # set color
            color = colors[index]

            # plot line at all axes
            axes[0].plot(indices, line, color=color, alpha=opacity)
            axes[1].plot(indices, line, color=color, alpha=opacity, label=label)
            axes[2].plot(indices, line, color=color, alpha=opacity)

        # set axis limits
        axes[0].set_ylim(head, maximum)
        axes[1].set_ylim(tail, head)
        axes[2].set_ylim(minimum, tail)

        # set ytick labels
        # axes[0].set_yticklabels(axes[0].get_ymajorticklabels(), fontsize=20)
        # axes[1].set_yticklabels(axes[1].get_ymajorticklabels(), fontsize=20)
        # axes[2].set_yticklabels(axes[2].get_ymajorticklabels(), fontsize=20)
        # axes[1].set_ylabel('relative % difference v0499 vs v134', fontsize=20)

        # set ytick labels
        [label.set_fontsize(20) for label in axes[0].get_yticklabels()]
        [label.set_fontsize(20) for label in axes[1].get_yticklabels()]
        [label.set_fontsize(20) for label in axes[2].get_yticklabels()]

        # label with units
        units = self.unity or self.bar.independent
        axes[1].set_ylabel(units, fontsize=20)

        # set xtick labels
        [label.set_fontsize(20) for label in axes[0].get_xticklabels()]
        [label.set_fontsize(20) for label in axes[1].get_xticklabels()]
        [label.set_fontsize(20) for label in axes[2].get_xticklabels()]

        # label x axis as with scatter plot
        axes[2].set_xlabel(self.scatter.independent, fontsize=20)

        # set title
        title = self.entitlement or self.scatter.title
        axes[0].set_title('{}'.format(title), fontsize=20)

        # add legend
        figure.legend(loc='lower right')

        # save the figure
        deposit = 'plots/sketch.png'
        pyplot.savefig(deposit)
        pyplot.clf()

        return None

    def splinter(self, axis):
        """Splinter the feature into one prism per index along an axis.

        Arguments:
            axis: int

        Returns:
            list of Prism instances
        """

        # get new prisms
        prisms = []
        for index in range(self.shape[axis]):

            # create copy of prism
            xerox = self.forge()

            # set name
            xerox['name'] = '{}_{}_{}'.format(self.name, self.signifiers[axis], index)

            # find appropriate route step
            steps = [number for number, _ in enumerate(xerox['route']) if len(xerox['route'][number]) > 1]
            step = steps[axis]

            # limit route to the one member
            xerox['route'][step] = [xerox['route'][step][index]]

            # correct shape
            shape = xerox['shape']
            shape = shape[:axis] + shape[axis + 1:]
            xerox['shape'] = shape

            # correct signifiers
            xerox['signifiers'] = xerox['signifiers'][:axis] + xerox['signifiers'][axis + 1:]

            # create prism
            prism = Prism(xerox, self.tree, self.bypass)
            prisms.append(prism)

        return prisms
