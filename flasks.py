# import json, os
import json
import os

# import flask tools
from flask import Flask
from jinja2 import Template

# import xarray
import xarray

# import bokeh
from bokeh.embed import json_item
from bokeh.layouts import row as Row, column as Column
from bokeh.models import HoverTool, ColumnDataSource, CheckboxGroup, CustomJS
from bokeh.plotting import figure
from bokeh.resources import CDN


# begin flask app
app = Flask(__name__)

# load in datasets
directory = 'netcdfs'
paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]
datasets = [xarray.open_dataarray(path, engine='h5netcdf') for path in paths]

# extract features
data = []
zipper = [(dataset, path) for dataset, path in zip(datasets, paths)]
for dataset, path in zipper[:1]:

    # go through parameters
    parameters = [parameter for parameter in dataset.coords['parameter'].data]
    for parameter in parameters:

        # go through reductions
        feature = {'name': parameter.decode('ascii')}
        reductions = [reduction for reduction in dataset.coords['reduction'].data]
        for reduction in reductions:

            # get data
            values = dataset.sel(parameter=parameter, reduction=reduction)[0].data
            values = [float(value) for value in values]
            feature[reduction.decode('ascii')] = values

        # add feature to data
        data.append(feature)

data.sort(key=lambda feature: feature['name'])


# draw all plots
def draw(data):

    # go through each feature
    reservoir = {}
    graphs = []
    for feature in data:

        # make label
        label = '{}'.format(feature['name'])

        # begin figure
        title = str(feature['name'])
        graph = figure(title=title, sizing_mode="fixed", plot_width=1000, plot_height=600, visible=False)

        # set axis labels
        graph.xaxis.axis_label = 'timepoint'
        #graph.yaxis.axis_label = feature['units'] + feature['reduction']
        graph.yaxis.axis_label = ' '

        # get data
        mean = feature['mean']
        #median = feature['median']
        minimum = feature['minimum']
        maximum = feature['maximum']
        x = [index for index, _ in enumerate(mean)]
        table = {'x': x, 'mean': mean, 'minimum': minimum, 'maximum': maximum}
        source = ColumnDataSource(table)

        # add hover tool
        annotations = [('timepoint', '@x')]
        annotations += [('maximum', '@maximum')]
        annotations += [('mean', '@mean')]
        annotations += [('minimum', '@minimum')]
        hover = HoverTool(tooltips=annotations)
        graph.add_tools(hover)

        # add lines
        reductions = ('minimum', 'maximum', 'mean')
        colors = ('green', 'red', 'purple', 'blue')
        widths = (1, 1, 1, 2)
        for reduction, color, width in zip(reductions, colors, widths):

            # graph line
            graph.line(source=source, x='x', y=reduction, color=color, legend_label=reduction, line_width=width)

        # add legend
        graph.legend.click_policy = 'hide'

        # add graph to reservoir
        reservoir[label] = graph

        # make label
        label = '{}'.format(feature['name'])

        # make checkbox
        checkbox = CheckboxGroup(labels=[label], active=[], width=200)

        # assign callback
        callback = CustomJS(args=dict(re=reservoir, la=label, cb=checkbox), code="""
        re[la].visible = !re[la].visible;
        """)

        # assign callback to active
        checkbox.js_on_click(callback)

        # make figures
        figures = [checkbox]
        for reduction in ('mean', 'minimum', 'maximum'):

            # begin figure and set label
            miniature = figure(sizing_mode="fixed", plot_width=200, plot_height=60)
            miniature.xaxis.axis_label = reduction

            # plot the line
            y = feature[reduction]
            x = [index for index, _ in enumerate(y)]
            miniature.line(x=x, y=y, color='blue')

            # remove ticks
            miniature.xaxis.major_label_text_font_size = '0pt'
            miniature.yaxis.major_label_text_font_size = '0pt'

            # add to figures
            figures.append(miniature)

        # make row of graphs
        row = Row(figures)
        graphs.append(row)
        graphs.append(graph)

    return graphs


# make column plot
graphs = draw(data)
column = Column(graphs)

# begin template
template = """<!DOCTYPE html> <html lang="en"> <h2>OMI Trending System Prototype</h2>"""
template += """<head> {{ resources }} </head> <body>"""

# add div to template to localize plots
div = '<div id="plots"></div>'
template += div

# add script to template for rendering the plots
script = """<script>fetch('/render')"""
script += """.then(function(response) { return response.json(); })"""
script += """.then(function(item) { return Bokeh.embed.embed_item(item,"plots"); })</script>"""
template += script

# close body tag and html tag
template += """</body></html>"""

# make page
page = Template(template)


# define root app route
@app.route('/')
def root():

    return page.render(resources=CDN.render())

# define plot route
@app.route('/render')
def render():

    return json.dumps(json_item(column, "plots"))


# set main to run app from command line
if __name__ == '__main__':

    # run app
    app.run()