# import reload
from importlib import reload

# import json, os
import json
import os

# import math
from math import ceil

# import flask tools
from flask import Flask
from jinja2 import Template

# import datetime
from datetime import datetime, timedelta
from time import time

# import xarray
import xarray

# import numpy
import numpy

# import bokeh
from bokeh.embed import json_item
from bokeh.layouts import row as Row, column as Column
from bokeh.models import HoverTool, ColumnDataSource, CheckboxGroup, CustomJS
from bokeh.models import Paragraph, Button
from bokeh.plotting import figure, curdoc
from bokeh.resources import CDN


# class Flasket to contain the web app
class Flasket(dict):
    """Class Flasket to contain all plots in the flask app.

    Inherits from:
        dict
    """

    def __init__(self):
        """Instantiate the Flasket.

        Arguments:
            None

        Returns:
            None
        """

        # set current time
        self.now = time()

        # ingest the data
        self.reservoir = {}
        self.data = {}
        self._ingest()

        # begin the page
        self.page = None
        self.boxes = {}
        self._begin()

        # begin pageii
        self.pageii = None

        return

    def _begin(self):
        """Begin the page with the choice checkboxes.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.page
        """

        # begin the page
        page = []

        # establish prefixes for each checkbox category
        prefixes = {'bands': '', 'modes': '', 'words': 'parameter_key'}

        # go through categoiess
        rows = {}
        for category in ('bands', 'modes', 'words'):

            # add paragraph
            prefix = prefixes[category]
            text = """Choose_{}{}""".format(prefix, category)
            text += '_' * (100 - len(text))
            paragraph = Paragraph(text=text, width=700)
            page.append(paragraph)

            # add empty row for checkboxes later
            row = Row([])
            rows[category] = row
            page.append(row)

        # add closing paragraph
        text = """"""
        text += '_' * (100 - len(text))
        paragraph = Paragraph(text=text, width=700)
        page.append(paragraph)

        # add choices paragraph
        text = """"""
        paragraph = Paragraph(text=text, width=200)
        page.append(paragraph)

        # make into bokeh column
        heading = Column(page)
        graphs = Column([])
        self.page = Column(heading, graphs)

        # define category specific properties
        numbers = {'bands': 3, 'modes': 3, 'words': 5}
        widths = {'bands': 250, 'modes': 250, 'words': 150}
        addresses = {'bands': 1, 'modes': 2, 'words': 0}

        # define labels override function for words parameter
        overrides = {'bands': lambda labels: labels, 'modes': lambda labels: labels}
        overrides.update({'words': lambda _: self._parse()})

        # go through categoiess to make checkboxesx
        for category in ('bands', 'modes', 'words'):

            # unpack category specifics
            address = addresses[category]
            number = numbers[category]
            width = widths[category]
            override = overrides[category]

            # create labels
            labels = override(self._skim([feature['name'].split(':')[address] for feature in self.values()]))
            pieces = self._chop(labels, number)

            # make checkboxes
            checkboxes = [CheckboxGroup(labels=piece, active=[], visible=True, width=width) for piece in pieces]
            [checkbox.js_on_click(self.offer()) for checkbox in checkboxes]
            self.boxes[category] = checkboxes
            [rows[category].children.append(box) for box in checkboxes]

        return None

    def _choose(self):
        """Generate the Node.js code for choosing features from selections.

        Arguments:
            None

        Returns:
            str
        """

        # begin code for filtering by checked boxes
        code = """"""

        # go through selection layers
        for selection in ('band', 'mode', 'word'):

            # get all identified bands
            code += """var {}s = [];""".format(selection)
            code += """boxes.{}s.forEach(function (box)""".format(selection) + """ {"""
            code += """box.active.forEach(function (index) {"""
            code += """{}s.push(box.labels[index]);""".format(selection)
            code += """})});"""

        # get all feature names
        code += """var names = features.map(function (feature) {"""
        code += """return feature.name;"""
        code += """});"""

        # go through selection layers
        for selection in ('band', 'mode', 'word'):

            # filter out names according to band selections
            code += """names = names.filter(function (name) {"""
            code += """return {}s.some(function ({})""".format(selection, selection) + """ {"""
            code += """return name.includes({})""".format(selection)
            code += """})});"""

        # change active status for features
        code += """features.forEach(function (feature) {"""
        code += """if (names.includes(feature.name)) {"""
        code += """feature.active = true;"""
        code += """};"""
        code += """if (!names.includes(feature.name)) {"""
        code += """feature.active = false;"""
        code += """}});"""

        # add text to names paragraph
        code += """names = names.map(function (name) {"""
        code += """return name.replace(":", "/").replace(":", "/");"""
        code += """});"""
        code += """names = names.sort();"""
        code += """paragraph.text = names.join(' ');"""

        return code

    def _chop(self, words, number):
        """Chop a list of words into roughly equal parts.

        Arguments:
            words: list of str
            number: int, number of pieces

        Returns:
            list of lists of words
        """

        # get the chunk size
        chunk = ceil(len(words) / number)

        # get the pieces
        pieces = [words[index * chunk: (index + 1) * chunk] for index in range(number)]

        return pieces

    def _ingest(self):
        """Ingest the hdf5 files into features.

        Arguments:
            None

        Returns:
            None

        Populates:
            self
        """

        # start timer
        self._stamp('ingesting data into xarrays...')

        # load in datasets
        directory = 'netcdfs'
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]
        arrays = [xarray.open_dataarray(path) for path in paths]

        # extract features
        self._stamp('extracting features...')
        features = []
        reservoir = {}
        for array in arrays:

            # go through parameters
            parameters = [parameter for parameter in array.coords['parameter'].data]
            reductions = [reduction for reduction in array.coords['reduction'].data]

            # convert array to list
            conversion = numpy.nan_to_num(array.data).tolist()

            # create features
            for index, parameter in enumerate(parameters):

                # add feature
                feature = {'name': parameter, 'manifestation': None, 'active': False}
                features.append(feature)

                # create entry in reservoir
                reservoir[parameter] = {}

                # go through reductions
                for indexii, reduction in enumerate(reductions):

                    # add data to data reservoir
                    reservoir[parameter][reduction] = conversion[index][indexii][0]

        # populate
        self._stamp('populating features...')
        [self.update({feature['name']: feature}) for feature in features]

        # populate reservoir
        self.data = reservoir

        # print status
        self._stamp('data ingested.')

        return None

    def _load(self, path):
        """Load a json file.

        Arguments:
            path: str, file path

        Returns:
            dict
        """

        # open json file
        with open(path, 'r') as pointer:

            # get contents
            contents = json.load(pointer)

        return contents

    def _parse(self):
        """Parse feature names into smallest set of words.

        Arguments:
            None

        Returns:
            None
        """

        # get all names
        names = self._skim([feature['name'].split(':')[0].lower() for feature in self.values()])

        # get all words, purging off digits first
        words = self._skim([self._purge(word) for name in names for word in name.split('_')])

        # only retain three-letter words or longer
        words = [word for word in words if len(word) > 0]

        # peel off most important
        peels = []
        while len(names) > 1:

            # count names
            counts = {word: len([name for name in names if word in self._purge(name).lower().split('_')]) for word in words}
            counts = [item for item in counts.items()]
            counts.sort(key=lambda item: item[1], reverse=True)

            # add top to peels
            peels.append(counts[0][0])

            # remove all names
            names = [name for name in names if counts[0][0] not in name.lower()]

        # sort peels
        peels.sort()

        return peels

    def _purge(self, word):
        """Purge digits from a word.

        Arguments:
            word: str

        Return:
            str
        """

        # replace all digits
        for digit in range(10):

            # replace
            word = word.replace(str(digit), '')

        return word

    def _skim(self, members):
        """Skim off the unique members from a list.

        Arguments:
            members: list

        Returns:
            list
        """

        # trim duplicates and sort
        members = list(set(members))
        members.sort()

        return members

    def _stamp(self, message):
        """Start timing a block of code, and print results with a message.

        Arguments:
            message: str

        Returns:
            None
        """

        # get final time
        final = time()

        # calculate duration and reset time
        duration = round(final - self.now, 5)
        self.now = final

        # print duration
        print('took {} seconds.'.format(duration))

        # begin new block
        print(message)

        return None

    def draw(self, feature):
        """Draw the plot based on a feature.

        Arguments:
            feature: dict

        Returns:
            bokeh object.
        """

        # make label
        name = feature['name']
        label = '{}'.format(name)

        # begin figure
        title = str(name.replace(':', '/'))
        graph = figure(title=title, sizing_mode="fixed", plot_width=1000, plot_height=600, visible=False)

        # set axis labels
        graph.xaxis.axis_label = 'timepoint'
        #graph.yaxis.axis_label = feature['units'] + feature['reduction']
        graph.yaxis.axis_label = ' '

        # get data
        mean = self.data[name]['mean']
        #median = feature['median']
        minimum = self.data[name]['minimum']
        maximum = self.data[name]['maximum']
        x = [index for index, _ in enumerate(mean)]
        table = {'x': x, 'mean': mean, 'minimum': minimum, 'maximum': maximum}
        source = ColumnDataSource(table)

        # add hover tool
        annotations = [('timepoint', '@x')]
        annotations += [('maximum', '@maximum')]
        annotations += [('mean', '@mean')]
        annotations += [('minimum', '@minimum')]
        hover = HoverTool(tooltips=annotations)
        graph.add_tools(hover)

        # add lines
        reductions = ('minimum', 'maximum', 'mean')
        colors = ('green', 'red', 'purple', 'blue')
        widths = (1, 1, 1, 2)
        for reduction, color, width in zip(reductions, colors, widths):

            # graph line
            graph.line(source=source, x='x', y=reduction, color=color, legend_label=reduction, line_width=width)

        # add legend
        graph.legend.click_policy = 'hide'

        # add graph to reservoir
        reservoir = self.reservoir
        reservoir[label] = {'graph': graph}
        feature['manifestation'] = {'graph': graph}

        # make label
        label = '{}'.format(name)

        # make checkbox
        checkbox = CheckboxGroup(labels=[label], active=[], width=500, visible=True)

        # assign callback
        code = """re[la]["graph"].visible = !re[la]["graph"].visible"""
        callback = CustomJS(args=dict(re=reservoir, la=label, cb=checkbox), code=code)

        # assign callback to active
        checkbox.js_on_click(callback)

        # make figures
        figures = [checkbox]
        for reduction in ('mean', 'minimum',):

            # begin figure and set label
            miniature = figure(sizing_mode="fixed", plot_width=200, plot_height=60, visible=True)
            miniature.xaxis.axis_label = reduction

            # plot the line
            y = self.data[name][reduction]
            x = [index for index, _ in enumerate(y)]
            miniature.line(x=x, y=y, color='blue')

            # remove ticks
            miniature.xaxis.major_label_text_font_size = '0pt'
            miniature.yaxis.major_label_text_font_size = '0pt'

            # add to figures
            figures.append(miniature)

        # make row of graphs and add to reservoir
        row = Row(figures)
        reservoir[label]['row'] = row
        feature['manifestation']['row'] = row

        return row, graph

    def offer(self):
        """Offer a check box for choices:

        Arguments:
            None

        Returns:
            None
        """

        # set arguments
        boxes = self.boxes
        features = [feature for feature in self.values()]
        paragraph = self.page.children[-1]

        # begin callback code
        code = """"""

        # add the code for extracting parameter names from checkbox selections
        code += self._choose()

        # make callback
        callback = CustomJS(args=dict(boxes=boxes, features=features, paragraph=paragraph), code=code)

        return callback

    def stack(self):
        """Stack all features into a column of graphs.

        Arguments:
            None

        Returns:
            bokeh column object
        """

        # get alll names
        names = self._skim([feature['name'] for feature in self.values()])

        print('length of names, pre filtered: {}'.format(len(names)))

        # get all active features
        for selection in ('bands', 'modes', 'words'):

            # get check boxes
            czechs = [box for box in self.boxes[selection]]

            for box in czechs:
                print(box.labels, box.active)


            members = [box.labels[index] for box in czechs for index in box.active]

            print('members: {}'.format(len(members)))

            # filter out names
            names = [name for name in names if any([member in name for member in members])]

            print('length of names: {}'.format(len(names)))

        print('length of names: {}'.format(len(names)))

        # get all active features
        features = [feature for feature in self.values() if feature['name'] in names]

        # construct column from rows and graphs
        column = []
        for feature in features:

            # make row and column
            row, graph = self.draw(feature)
            column.append(row)
            column.append(graph)

        # create column
        column = Column(column)

        return column

    def test(self):
        self.test = True
        print(self.test)
        return None


# begin flask app
app = Flask(__name__)

# create flasket and create a column of graphs
flasket = Flasket()
#curdoc().add_root(flasket.page)

# begin template
template = """<!DOCTYPE html> <html lang="en"> <h2>OMI Trending System Prototype</h2>"""
template += """<head> {{ resources }} </head> <body>"""

# add button
template += """<p align="center"><a href=view>"""
template += """<button class=grey style="height:30px;width:100px;background-color:lightgreen">"""
template += """view plots</button></a></p>"""

# add div to template to localize plots
div = '<div id="boxes"></div>'
template += div

# add script to template for rendering the plots
script = """<script>fetch('/offer')"""
script += """.then(function(response) { return response.json(); })"""
script += """.then(function(item) { return Bokeh.embed.embed_item(item,"boxes"); })</script>"""
template += script

# close body tag and html tag
template += """</body></html>"""

# make page
page = Template(template)


# begin template
template = """<!DOCTYPE html> <html lang="en"> <h2>OMI Trending System Prototype</h2>"""
template += """<head> {{ resources }} </head> <body>"""

# add button
template += """<p align="center"><a href=refresh>"""
template += """<button class=grey style="height:30px;width:100px;background-color:lightgreen">"""
template += """view menu</button></a></p>"""

# add div to template to localize plots
div = '<div id="plots"></div>'
template += div

# add script to template for rendering the plots
script = """<script>fetch('/render')"""
script += """.then(function(response) { return response.json(); })"""
script += """.then(function(item) { return Bokeh.embed.embed_item(item,"plots"); })</script>"""
template += script


# close body tag and html tag
template += """</body></html>"""

# make page
pageii = Template(template)


# define root app route
@app.route('/')
def root():

    return page.render(resources=CDN.render())

# define plot route
@app.route('/offer')
def offer():

    # get column
    column = flasket.page

    return json.dumps(json_item(column, "boxes"))

# define root app route
@app.route('/view')
def view():

    return pageii.render(resources=CDN.render())

# define plot route
@app.route('/render')
def render():

    # render all plots
    columnii = flasket.stack()

    return json.dumps(json_item(columnii, "plots"))

# define root app route
@app.route('/refresh')
def refresh():

    return page.render(resources=CDN.render())

# set main to run app from command line
if __name__ == '__main__':

    # run app
    app.run()