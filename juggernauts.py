#!/usr/bin/env python3

# juggernauts.py for the Juggernaut class to reduce OMI trending data

# import local classes
from hydras import Hydra
from features import Feature
from formulas import Formula

# import system
import sys

# import regex
import re

# import numpy functions
import numpy


# class Juggernaut to do OMI data reduction
class Juggernaut(Hydra):
    """Juggernaut class to generate reduced OMI files for trending.

    Inherits from:
        Hydra
    """

    def __init__(self, controller='', sink='', source='', start='', finish='', clock=True):
        """Initialize a Juggernaut instance.

        Arguments:
            controller: str, control file name
            sink: str, filepath for data dump
            source: str, filepath of source files
            start: str, date-based subdirectory
            finish: str, date-based subdirectory
            clock: boolean, allow timing output?

        Returns:
            None
        """

        # initialize the base Hydra instance
        Hydra.__init__(self, source, start, finish)

        # set control file name
        self.sink = sink
        self.controller = controller
        self._assume(sink)

        # set timing switch
        self.clock = clock

        # take inventory of quality flags
        self.flags = {}
        self._inventory()

        # set pixel sizes
        self.pixels = {}
        self._pixelate()

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Juggernaut instance at: {} >'.format(self.sink)

        return representation

    def _assume(self, sink):
        """Assume control from a default control file

        Arguments:
            sink: str, sink directory

        Returns:
            None

        Populates:
            self.controller
        """

        # if there is no control file given
        if not self.controller:

            # create control file name from sink
            self.controller = '{}/control-file.txt'.format(sink)

        # print controller
        self._print('assuming controller: {}'.format(self.controller))

        return None

    def _bite(self, tensor, bit, masquerade):
        """Combine boolean masks for a particular bit.

        Arguments:
            tensor: numpy.array
            bit: int
            masquerade: dict of boolean masks

        Returns:
            numpy array
        """

        # convert to int
        bit = int(bit)

        # decompose all uniques
        masks = []
        for unique, mask in masquerade.items():

            # decompose into binary form
            decomposition = bin(unique).replace('0b', '0' * 20)
            decomposition = decomposition[::-1]
            if decomposition[bit] == '1':

                # add to codes
                masks.append(mask)

        # add empty mask
        empty = numpy.zeros(tensor.shape)

        # summ together for new mask
        masque = empty
        for entry in masks:

            # add to the running sum
            masque += entry

        return masque

    def _dictate(self, message, page):
        """Print a message to screen as well as appending to the page.

        Arguments:
            message: str
            page: list of str

        Returns:
            None
        """

        # print the message
        self._print('{}'.format(message))

        # add to page
        page.append('{}'.format(message))

        return None

    def _distinguish(self, features):
        """Rename features using elements from full route to guarantee uniqueness.

        Arguments:
            features: list of feature instances

        Returns:
            None
        """

        # make mapping
        aliases = []

        # group all features based on feature name
        names = self._group(features, lambda feature: feature.name)
        for name, members in names.items():

            # if more than one member
            if len(members) > 1:

                # get all routes, splitting at underscores
                routes = [[word for field in member.route for word in field.split('_')] for member in members]
                steps = zip(*routes)

                # for each route index
                indices = [index for index, step in enumerate(steps) if len(set(step)) > 1]
                for member in members:

                    # make aliase from route
                    words = [word for field in member.route for word in field.split('_')]
                    extension = '_'.join([words[index] for index in indices])
                    alias = '{}_{}'.format(extension, name)

                    # append aliases
                    aliases.append('{}    {}    {}'.format(self._cameo(alias), member.name, member.slash))

                    # change feature name
                    member.dub(alias)

        # jot aliseas
        #self._jot(aliases, 'aliases.txt')

        return None

    def _inventory(self):
        """Take inventory of quality flags.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.flags
        """

        # begin flags
        flags = {}

        # get the contents of the flags file
        text = self._know('flags.txt')

        # join with colons and separate by double colons (blank lines)
        text = ':'.join(text)
        groups = text.split('::')

        # collect groups
        for group in groups:

            # split by colons to get members
            lines = group.split(':')
            name = lines[0]
            members = lines[1:]

            # update flag codes with descriptions, retaining the first member as group name
            codes = {int(bit): description for bit, description in [member.split(' ') for member in members]}
            flags[name] = codes

        # set flags
        self.flags = flags

        return None

    def _jangle(self, name, attributes, specifications=None):
        """Set django app attributes.

        Arguments:
            name: str, parameter name
            attributes: dict

        Returns:
            dict
        """

        # set default attributes
        jangles = {}
        jangles.update({'plotType': 'lines'})
        jangles.update({'doNotPlot': False})
        jangles.update({'FillValue': -999})
        jangles.update({'LongName': name})
        jangles.update({'PlotLegend': name})
        jangles.update({'Units': '_'})
        jangles.update({'Status': ''})
        jangles.update({'AbscissaVariableSelection': 'IndependentVariables/OrbitStartTimeFrYr'})

        # remove problematic fields
        problems = ('DIMENSION_LIST',)
        attributes = {name: value for name, value in attributes.items() if name not in problems}

        # update with attributes
        jangles.update(attributes)

        # add temperature units
        if jangles['Units'] == '_' and 'temp' in name.lower():

            # add temp units
            jangles['Units'] = 'deg K'

        # get pairs of attr names and omps names
        overwrites = [('units', 'Units'), ('description', 'LongName'), ('_FillValue', 'FillValue')]

        # go through each attribute
        for old, new in overwrites:

            # try to get the old value
            try:

                # to get the old value
                jangles[new] = attributes[old]

            # unless not available
            except KeyError:

                # in which case, nevermind
                pass

        # convert FillValue to float
        jangles['FillValue'] = float(jangles['FillValue'])

        # check for specifications
        if specifications:

            # update
            jangles.update(specifications)

        # decode binary string
        for field, quantity in jangles.items():

            # try to
            try:

                # remove binary string
                jangles[field] = quantity.decode('utf8')

            # otherwise
            except AttributeError:

                # nevermind
                pass

        return jangles

    def _masquerade(self, tensor):
        """Create boolean feature masks for each unique feature in the tensor.

        Arguments:
            tensor: numpy array

        Returns:
            dict
        """

        # get all unique values, using a method faster than numpy.unique
        integers = numpy.array(tensor).astype(int)
        zeros = numpy.zeros(numpy.max(integers) + 1, dtype=int)
        zeros[integers.ravel()] = 1
        uniques = numpy.nonzero(zeros)[0]

        # create boolean masks and convert to ints
        masks = {unique: tensor == unique for unique in uniques}

        return masks

    def _pixelate(self):
        """Define pixels / image for each band.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.pixels
        """

        # add each band
        self.pixels['BAND1'] = 30 * 159
        self.pixels['BAND2'] = 60 * 557
        self.pixels['BAND3'] = 60 * 751

        return None

    def _plant(self, path):
        """Build the data tree from file paths.

        Arguments:
            path: str, orbital file path

        Returns:
            dict
        """

        # begin tree
        tree = {}

        # get the data at the path
        data = self._fetch(path)

        # split into orbit, filename
        pieces = path.split('/')[-1].split('.')[0].split('_')
        product = pieces[1]
        orbit = pieces[2]

        # check for orbit in tree
        if orbit not in tree.keys():

            # add orbit
            tree[orbit] = {}

        # insert data into tree
        tree[orbit][product] = data

        return tree

    def _process(self, job):
        """Extract the processing version information from a job description string.

        Arguments:
            job: str, job description

        Returns:
            dict of strings
        """

        # begin processor
        versions = {'processor': '', 'core': ''}

        # search for processor and core
        processor = re.search('OML1BPDS-[0-9].[0-9].[0-9].[0-9]', job.decode('utf8'))
        core = re.search('core_processor_version = [0-9].[0-9].[0-9].[0-9]{5}', job.decode('utf8'))

        # if processor was found
        if processor:

            # add to versions
            versions['processor'] = processor.group()

        # if core was found
        if core:

            # add to versions
            versions['core'] = core.group()

        return versions

    def _profile(self):
        """Parse the profile into sortable form.

        Arguments:
            None

        Returns:
            None
        """

        # open file
        report = self._know('{}/profiles/profile.txt'.format(self.sink))

        # begin profile
        profile = []
        summary = {}
        attentive = False
        headers = None
        for line in report:

            # if attentive
            if attentive:

                # check for headers
                if headers and len(line.split()) > 4:

                    # in which case make entry
                    entry = {key: value for key, value in zip(headers, line.split() + ['_'])}

                    # and convert to float if possibile
                    def floating(string): return string.replace('.', '').isdigit()
                    entry.update({key: float(value) for key, value in entry.items() if floating(value)})
                    profile.append(entry)

                # check for headers
                if not headers and len(line.split()) > 4:

                    # create headers
                    headers = line.split() + ['details']
                    def marking(header, index): return header + str(index) if headers.count(header) > 1 else header
                    headers = [marking(header, index) for index, header in enumerate(headers)]

            # check for primitives
            if 'primitive calls' in line:

                # for each field
                for field in ('primitive calls', 'function calls', 'seconds'):

                    # add to summar
                    summary[field] = float(re.search('[0-9,.]+ ' + field, line).group().strip(' ' + field))

                # switch on attention
                attentive = True

        # print summary
        self._print('\n{} total seconds'.format(summary['seconds']))

        # sort profile by total time
        profile.sort(key=lambda entry: entry['tottime'], reverse=True)

        # print first few
        [self._print(entry) for entry in profile[:20]]

        return None

    def _query(self, record, field, answer):
        """Query a record for data in a field, or send default value.

        Arguments:
            record: dict
            field: str
            answer: variable type, default answer

        Returns:
            variable type
        """

        # search for field
        if field in record.keys():

            # answer is the data
            answer = record[field]

        return answer

    def _state(self, tensor):
        """Perform statistical reductions on the tensor.

        Arguments
            tensor: numpy array

        Returns:
            numpy array
        """

        # calculate stats
        mean = tensor.mean()
        median = numpy.quantile(tensor, 0.5)
        minimum = tensor.min()
        maximum = tensor.max()
        difference = maximum - minimum
        deviation = tensor.std()

        # construct array
        array = numpy.array([mean, median, minimum, maximum, difference, deviation])

        return array

    def annihilate(self):
        """Destroy all derived data folders.

        Arguments:
            None

        Returns:
            None
        """

        # destroy folders
        self._clean('{}/trends'.format(self.sink))
        self._clean('{}/fusions'.format(self.sink))
        self._clean('{}/samples'.format(self.sink))

        return None

    def capture(self, exclusions=None):
        """Capture all numerical features.

        Arguments:
            features: list of dict

        Returns:
            list of xarrays
        """

        # default exclusions
        exclusions = exclusions or []

        # get subset of features that are quality flags
        inventory = self.flags
        subset = self.apply(lambda feature: feature.name not in inventory.keys())

        # remove type str
        subset = self.apply(lambda feature: feature.type != numpy.dtype('O'), features=subset)

        # remove exclusions
        subset = [feature for feature in subset if feature.name not in exclusions]

        # fill all features
        [feature.fill() for feature in subset]

        return subset

    def control(self, trim=False, now=''):
        """Generate a control file.

        Arguments:
            trim: boolean, trim source and sink folders form inputs and outputs?
            now: overrides current now for given now, for keeping production time constant for updates

        Returns:
            None
        """

        # read in from description
        now = now or self._note()
        description = self._acquire('../doc/Description.txt')

        # copy into control
        exclusions = ['Dynamic Input Files', 'Output Files']
        control = {key: value for key, value in description.items() if key not in exclusions}

        # get app name
        name = description['APP Name']

        # begin specifics
        specifics = {'Input Files': {}, 'Output Files': {}}

        # make dual dictionary
        duals = {'OML1BRUG': 'OML1BRVG', 'OML1BRVG': 'OML1BRUG'}

        # grab relevant paths
        paths = [path for path in self.paths if any([product in path for product in duals.keys()])]

        # get version of first path for use in output version
        version = self._stage(paths[0])['version']

        # if trim option selected
        if trim:

            # remove source folder information
            paths = [path.split('/')[-1] for path in paths]

        # for each path
        sources = []
        destinations = []
        for path in paths:

            # stage the path
            stage = self._stage(path)
            orbit, date, product = [stage[field] for field in ('orbit', 'date', 'product')]

            # add path to inputs, as well as other prdouct
            sources.append(path)
            sources.append(path.replace(product, duals[product]))

            # find appropriate output template
            template = description['Output Files'][0]['Filename']

            # create destination name and add to outputs
            destination = template.replace('<OrbitNumber>', orbit)
            destination = destination.replace('<StartTime!%Ym%m%dt%H%M%S>', date)
            destination = destination.replace('<ProductionTime>', now)
            destination = destination.replace('<ECSCollection>', version)

            # if not trimming
            if not trim:

                # add sink
                destination = '{}/{}'.format(self.sink, destination)

            # add destination
            destinations.append(destination)

        # reduce to unique sources and sort
        sources = self._skim(sources)
        for product in duals.keys():

            # add product files
            specifics['Input Files'][product] = [source for source in sources if product in source]

        # shrink outputs to unique entries
        specifics['Output Files'][name] = self._skim(destinations)

        # update with specifics
        control.update(specifics)

        # dump into control
        self._dispense(control, self.controller)

        # improve spacing
        self._disperse(self.controller)

        return None

    def define(self):
        """Define the earth science data type.

        Arguments:
            None

        Returns:
            None
        """

        # begin definition
        definition = {}

        # add esdt name and processing level
        definition['ESDT Name'] = 'OML1BRADHIS'
        definition['Processing Level'] ='L1B'

        # add Long Name
        name = 'OMI/Aura Monitoring Data from Reduced OML1BRUG, OML1BRVG Radiance Files, 1-Orbit'
        definition['Long Name'] = name

        # add description
        text = 'Single orbit one point parameter reductions and quality flag bit decompositions '
        text += 'of OML1BRUG and OML1BRVG files, reduced to orbital mean, orbital minimum, orbital '
        text += 'maximum, orbital median, and orbital standard deviation, for the purpose of '
        text += 'monitoring OMI performance.'
        definition['Description'] = text

        # add conntacts
        definition['Science Team Contacts'] = ['Matthew Bandel (matthew.bandel@ssaihq.com)']
        definition['Science Team Contacts'] += ['David Haffner (david.haffner@ssaihq.com)']
        definition['Support Contact'] = 'Phillip Durbin (pdurbin@sesda.com)'

        # define sizes
        definition['Minimum Size'] = '6 MB'
        definition['Maximum Size'] = '18 MB'

        # define file name pattern
        pattern = '<Instrument>-<Platform>_<Processing Level>-<ESDT Name>_<DataDate>-o<OrbitNumber>'
        pattern += '_v<Collection>-<ProductionTime>.<File Format>'
        definition['Filename Pattern'] = pattern

        # add filename elements
        definition['Platform'] = 'Aura'
        definition['Instrument'] = 'OMI'
        definition['File Format'] = 'h5'

        # add other processing elements
        definition['Period'] = 'Orbits=1'
        definition['Archive Method'] = 'Compress'
        definition['File SubTable Type'] = 'L1L2'
        definition['Extractor Type'] = 'Filename_L1L2'
        definition['Metadata Type'] = 'orbital'
        definition['Parser'] = 'filename'
        definition['keypattern'] = '<OrbitNumber>'

        # dump into yaml
        destination = '../doc/OML1BRADHIS.txt'
        self._dispense(definition, destination)

        # clean up spacing
        self._disperse(destination)

        return None

    def describe(self, version=None):
        """Generate the descriptions.txt file.

        Arguments:
            None

        Returns:
            None
        """

        # load in the esdt
        definition = self._acquire('../doc/OML1BRADHIS.txt')

        # begin description
        description = {}

        # add APP Name
        name = definition['ESDT Name']
        description['APP Name'] = name

        # add APP Type
        description['APP Type'] = 'OMI'

        # add APP Version, from input if not specified
        version = version or input('version? ')
        description['APP Version'] = version

        # add Long Name
        description['Long Name'] = definition['Long Name']

        # add description, apply block form
        description['Description'] = '>\n' + definition['Description']

        # add algorithm lead
        description['Lead Algorithm Scientist'] = definition['Science Team Contacts'][0]

        # add other scientists
        description['Other Algorithm Scientists'] = definition['Science Team Contacts'][1]

        # add software developer
        description['Software Developer'] = definition['Science Team Contacts'][0]

        # add software developer
        description['Support Contact'] = definition['Support Contact']

        # add structure field
        structure = '>\nRun {}.py as a python program, using the control.txt file path as its argument.'.format(name)
        description['Structure'] = structure

        # add operational scenario
        scenario = '>\nRun over entire mission, and for every orbit as new data arrives.'
        description['Operational Scenario'] = scenario

        # add the period
        description['Period'] = definition['Period']

        # add the execution
        description['EXE'] = {'Name': '{}.py'.format(name), 'Program': '{}.py'.format(name)}

        # begin description of dynamic input files
        static = 'Static Input Files'

        # make entry for flags.txt
        contents = 'text file of quality flags and bit designations'
        description[static] = [{'Filename': 'flags.txt', 'Desc': contents}]

        # begin description of dynamic input files
        dynamic = 'Dynamic Input Files'

        # make entry for UV
        product = 'OML1BRUG'
        contents = 'OMI Level 1B UV Radiance Data, Bands 1 and 2'
        description[dynamic] = [{'ESDT': product, 'Rule': 'Required', 'Desc': contents}]

        # make entry for VIS
        product = 'OML1BRVG'
        contents = 'OMI Level 1B VIS Radiance Data, Band 3'
        description[dynamic] += [{'ESDT': product, 'Rule': 'Required', 'Desc': contents}]

        # start outputs
        output = 'Output Files'

        # begin entry for Filename
        pattern = definition['Filename Pattern']

        # for each filename parameterr
        for parameter in ('Instrument', 'Platform', 'ESDT Name', 'Processing Level', 'File Format'):

            # replace with esdt definition
            pattern = pattern.replace('<{}>'.format(parameter), definition[parameter])

        # add start time format and collection for now
        pattern = pattern.replace('<DataDate>', '<StartTime!%Ym%m%dt%H%M%S>')
        pattern = pattern.replace('<Collection>', '<ECSCollection>')

        contents = 'Reduced OMI UV and VIS Monitoring Data'
        description[output] = [{'Filename': pattern, 'ESDT': name, 'Rule': 'Required', 'Desc': contents}]

        # make entry for Runtime Parameters
        runtime = 'Runtime Parameters'
        description[runtime] = [{'Param': 'AppShortName', 'Value': name}]
        description[runtime] += [{'Param': 'Instrument', 'Value': definition['Instrument']}]
        description[runtime] += [{'Param': 'Platform', 'Value': definition['Platform']}]
        description[runtime] += [{'Param': 'OrbitNumber', 'Value': '"<OrbitNumber>"'}]
        description[runtime] += [{'Param': 'StartTime', 'Value': '"<StartTime>"'}]
        description[runtime] += [{'Param': 'ProductionTime', 'Value': '"<ProductionTime>"'}]
        description[runtime] += [{'Param': 'ECSCollection', 'Value': '"<ECSCollection>"'}]
        description[runtime] += [{'Param': 'Maximum Level of Detail', 'Value': '2'}]

        # make entry for Production Rules
        rules = 'Production Rules'
        description[rules] = [{'Rule': 'GetOrbitParams'}]
        description[rules] += [{'Rule': 'OrbitMatch', 'ESDT': 'OML1BRUG', 'Min_Files': 1}]
        description[rules] += [{'Rule': 'OrbitMatch', 'ESDT': 'OML1BRVG', 'Min_Files': 1}]

        # add compiler
        description['Compilers'] = [{'CI': 'python', 'Version': '3.6.8'}]

        # add environment
        description['Environment'] = [{'CI': 'env_setup', 'Version': '0.0.9'}]
        description['Environment'] += [{'CI': 'python', 'Version': '3.6.8'}]

        # add operating system
        description['Operating System'] = [{'CI': 'Linux', 'Version': '2.6.9'}]

        # dump into yaml
        destination = '../doc/Description.txt'.format(self.sink)
        self._dispense(description, destination)

        # improve spacing
        self._disperse(destination)

        return None

    def fracture(self):
        """Extract quality flag bit fractions from integer conglomerates.

        Arguments:
            features: list of dict

        Returns:
            list of feature instances
        """

        # get subset of features that are quality flags
        inventory = self.flags
        subset = self.apply(lambda feature: feature.name in inventory.keys())

        # for each feature
        flags = []
        for feature in subset:

            # get the data
            feature.fill()
            tensor = feature.data.squeeze()

            # get unique feature masks
            masks = self._masquerade(tensor)

            # for each bit code in the flags
            bits = inventory[feature.name]
            for bit, description in bits.items():

                # crack into fractions with each bit
                decomposition = self._bite(tensor, bit, masks)

                # multiply by pixel grid for total counts
                counts = decomposition * numpy.prod(tensor.shape[1:])

                # create new feature from fraction
                replica = feature.copy()
                replica.instil(counts)
                name = '{}_count'.format(description)
                replica.attributes['units'] = 'average counts / image'
                replica.dub(name, step=True)
                flags.append(replica)

        return flags

    def imitate(self):
        """Imitate the /run folder setup of the APP, assuming currently in /tbl"

        Arguments:
            None

        Returns:
            None
        """

        # create run directory
        self._make('../run')

        # copy all /bin files into /run
        paths = self._see('../bin')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        # copy all /tbl files into /run
        paths = self._see('../tbl')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        # copy all /utd files into /run
        paths = self._see('../utd')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        return None

    def interpret(self, report, destination):
        """Rearrange the features in the file according to sample entropy from an entropy file.

        Arguments:
            report: str, entropy file
            destination: str, destination file

        Returns:
            None
        """

        # get the entropies from the file
        entropies = self._know(report)

        # keep track of parameter names so far
        names = []
        for line in entropies:

            # get the parameter address
            address = line.split()[-1]
            places = address.split('/')
            name = '/'.join(places[-3:-1])

            # if not already in the list
            if name not in names:

                # add it
                names.append(name)

        # subset the feature by those without data link
        features = self.dig('Categories')
        independents = self.dig('IndependentVariables')

        # sort names
        names.sort()
        names.sort(key=lambda name: 'GEODATA' in name, reverse=True)
        names.sort(key=lambda name: 'INSTRUMENT' in name, reverse=True)
        names.sort(key=lambda name: 'OBSERVATIONS' in name, reverse=True)
        names.sort(key=lambda name: 'quality' in name, reverse=True)
        names.sort(key=lambda name: 'temp' in name, reverse=True)
        names.sort(key=lambda name: 'housekeeping' in name, reverse=True)
        names.sort(key=lambda name: 'spectral_channel_quality' in name, reverse=True)

        # for each name
        arrangement = []
        for name in names:

            # for each reduction
            for reduction in ('mean', 'std', 'max', 'median', 'min'):

                # for each band
                for band in (1, 2, 3):

                    # construct feature name
                    places = name.split('/')
                    group = places[-2]
                    parameter = places[-1]

                    # construct alias
                    alias = 'orbit_{}_BAND{}_RADIANCE_{}_{}'.format(reduction, band, group, parameter)

                    # try to
                    try:

                        # find the feature and add to features
                        feature = self.dig(alias, features)[0]
                        arrangement.append(feature)

                    # unless not found
                    except IndexError:

                        # print alert, but skip
                        self._print('skipped {}'.format(alias))

        # fill all features and link Categories
        [feature.update({'link': True}) for feature in arrangement]
        arrangement = independents + arrangement
        [feature.fill() for feature in arrangement]

        # stash at destination
        self.stash(arrangement, destination, 'Data', mode='w')

        return None

    def melt(self, outward='fusions'):
        """Fuse all individual trending files into larger aggregates.

        Arguments:
            inward: str, source directory relative to sink
            outward: str, destination directory relative to sink

        Returns:
            None
        """

        # create folder if not yet made
        folder = '/'.join(self.sink.split('/')[:-1])
        self._make('{}/{}'.format(folder, outward))

        # get all trend paths
        trends = self._see(self.sink)

        # get all orbits of trend paths
        orbits = [self._stage(path)['orbit'] for path in trends]
        orbits.sort()

        # get first and last orbit number
        first = orbits[0]
        last = orbits[-1]

        # print status
        self._stamp('\nfusing {} trends...'.format(len(trends)), initial=True, clock=self.clock)

        # search for fusion files
        fusions = [path for path in self._see('{}/{}'.format(folder, outward))]
        if len(fusions) > 1:

            # get first and last orbits from fusion names
            orbits = [self._stage(path)['orbit'] for path in fusions]
            orbits += [self._stage(path.replace(orbit, ''))['orbit'] for path, orbit in zip(fusions,orbits)]
            first = min(first, orbits[0])
            last = max(last, orbits[-1])

        # create destination path
        now = self._note()
        destination = '{}/{}/OMIREDUX_{}_{}_{}.h5'.format(folder, outward, first, last, now)

        # meld
        self.meld(trends, fusions, destination)

        # timestamp
        self._stamp('fused.', clock=self.clock)

        return None

    def mock(self, orbit):
        """Prepare a mock configure file to test in RunApp.

        Arguments:
            date: str, datestring
            destination: file name

        Returns:
            None
        """

        # open up the description file
        description = self._acquire('../doc/Description.txt')
        version = description['APP Version']

        # prepare mock attributes
        mock = {'archiveset': 70004, 'ECSCollection': 4, 'OrbitNumber': orbit}
        mock.update({'PGE': 'OML1BRADHIS', 'PGEVersion': version})
        mock.update({'InstrumentConfigAS': 70004, 'source': 'OMI'})

        # set inside runtimte
        mock = {'Runtime Parameters': mock}

        # dump into file
        destination = '../bin/OML1BRADHIS.input'
        self._dispense(mock, destination)

        return None

    def rampage(self):
        """Feed one orbit at a time through reduction.

        Arguments:
            None

        Returns:
            None
        """

        # open up the control file and retrieve input and output files
        control = self._acquire(self.controller)
        paths = control['Input Files']['OML1BRUG'] + control['Input Files']['OML1BRVG']
        destinations = control['Output Files']['OML1BRADHIS']

        # pair them by orbit number
        pairs = self._group(paths, lambda path: self._stage(path)['orbit'])

        # go through each path
        for orbit, pair in pairs.items():

            # try to
            try:

                # get destination with matching orbit number
                destination = [path for path in destinations if str(int(orbit)) in path][0]

                # reduce all features by mean, std, max, min, median\
                self.stomp(pair, destination)

            # unless there is a runtime snag
            except RuntimeError as error:

                # print the error and post to the log
                self._print('error for path {}: \n{}'.format(pair[0], error))

        return None

    def sample(self):
        """Sample subsets of fusion data to feed to OMPs.

        Arguments:
            None

        Returns:
            None
        """

        # create samples folder
        self._make('{}/samples'.format(self.sink))

        # get all fusion paths
        paths = self._see('{}/fusions'.format(self.sink))
        paths.sort()

        # sift out housekeeping and spectral channel quality
        self.sift(paths[0], 'samples', 'BAND1_RADIANCE/housekeeping_data', 'BAND1_RADIANCE/instrument_settings')
        self.sift(paths[0], 'samples', 'BAND1_RADIANCE/spectral_channel_quality', 'BAND1_RADIANCE/xtrack_quality')
        self.sift(paths[0], 'samples', 'BAND2_RADIANCE/housekeeping_data', 'BAND2_RADIANCE/instrument_settings')
        self.sift(paths[0], 'samples', 'BAND2_RADIANCE/spectral_channel_quality', 'BAND2_RADIANCE/xtrack_quality')
        self.sift(paths[1], 'samples', 'BAND3_RADIANCE/housekeeping_data', 'BAND3_RADIANCE/instrument_settings')
        self.sift(paths[1], 'samples', 'BAND3_RADIANCE/spectral_channel_quality', 'BAND3_RADIANCE/xtrack_quality')

        return None

    def stomp(self, paths, destination):
        """Reduce all orbital features to a single point.

        Arguments:
            paths: list of str, input paths
            destination: str, destination filepath

        Returns:
            None
        """

        # if sink given
        if self.sink:

            # create folder
            self._make(self.sink)

        # reduce each path, using write mode for first
        notices = {'a': 'appending', 'w': 'overwriting!'}
        features = []
        processor = []
        for path in paths:

            # ingest the path
            self._stamp('ingesting {}...'.format(path), initial=True, clock=self.clock)
            self.ingest(path)

            # decompose quality flag features into bit fractions
            self._stamp('decomposing quality flags...', clock=self.clock)
            flags = self.fracture()

            # gather remaining features
            self._stamp('gathering data...', clock=self.clock)
            remainder = self.capture(exclusions=['small_pixel_radiance'])
            features += flags + remainder
            self._stamp('gathered.', clock=self.clock)

            # get all processor features
            self._stamp('formulating...', initial=True, clock=self.clock)
            processor += self.dig('PROCESSOR')

        # create unique names for features
        self._distinguish(features)

        # begin formula
        formula = Formula()

        # get processor information
        def processing(tensor): return numpy.array([self._process(tensor[0])['processor']]).astype('S')
        formula.formulate('job_configuration', processing, 'processor_version')

        # get processor core information
        def coring(tensor): return numpy.array([self._process(tensor[0])['core']]).astype('S')
        formula.formulate('job_configuration', coring, 'core_processor_version')

        # organize features by band
        bands = self._group(features, lambda feature: re.search('BAND[1-3]', feature.slash).group())

        # for each band
        for band, members in bands.items():

            # for every feature
            for feature in members:

                # add operation name, as well as accounting for potential duplicate entries by appending _'s
                stub = feature.name
                def naming(tag): return '_'.join(['orbit', tag, stub])

                # get django attributes
                jangles = self._jangle(stub, feature.attributes)

                # if the feature is at least 2-D:
                if numpy.prod(feature.shape) > 1:

                    # perform statistical reductions
                    def stating(tensor): return self._state(tensor)
                    formula.formulate(feature.slash, stating, feature.name, None, jangles)

                    # # take the mean
                    # def averaging(tensor): return numpy.atleast_1d(tensor.mean())
                    # formula.formulate(feature.slash, averaging, naming('mean'), None, jangles)
                    #
                    # # take the minimum
                    # def minimizing(tensor): return numpy.atleast_1d(tensor.min())
                    # formula.formulate(feature.slash, minimizing, naming('min'), None, jangles)
                    #
                    # # take the maximuum
                    # def maximizing(tensor): return numpy.atleast_1d(tensor.max())
                    # formula.formulate(feature.slash, maximizing, naming('max'), None, jangles)
                    #
                    # # take the standard deviation
                    # def deviating(tensor): return numpy.atleast_1d(tensor.std())
                    # formula.formulate(feature.slash, deviating, naming('std'), None, jangles)
                    #
                    # # find the median
                    # def mediating(tensor): return numpy.atleast_1d(numpy.percentile(tensor, 50))
                    # formula.formulate(feature.slash, mediating, naming('median'), None, jangles)
                    #
                    # # find the range ( max - min )
                    # def ranging(tensor): return numpy.atleast_1d(tensor.max() - tensor.min())
                    # formula.formulate(feature.slash, ranging, naming('range'), None, jangles)

                # otherwise
                else:

                    # take the mean
                    def averaging(tensor): return numpy.atleast_1d(tensor.mean())
                    formula.formulate(feature.slash, averaging, feature.name, None, jangles)

        # calculate reductions
        self._stamp('reducing...', clock=self.clock)
        categories = self.cascade(formula, features + processor, scan=False)

        # # insert into Categories and link to Data, but avoid linking processor
        # [feature.divert('Categories') for feature in categories]
        # [feature.update({'link': True}) for feature in categories if 'PROCESSOR' not in feature.slash]

        # grab the independent variables from file path
        self._stamp('creating independents...', clock=self.clock)
        independents = self._parse(paths[0])

        # create features from all independent variable
        independents = {field: numpy.array([quantity]) for field, quantity in independents.items()}
        independents = [Feature([name], array) for name, array in independents.items()]

        # add django attributes
        [feature.attributes.update(self._jangle(feature.name, {'doNotPlot': True})) for feature in independents]

        # begin transfers of auxiliary independent variables from categories
        transfers = Formula()

        # grab earth sun distance
        jangles = self._jangle('orbit_earth_sun_distance', {'doNotPlot': True})
        transfers.formulate('earth_sun_distance', None, 'orbit_earth_sun_distance', 'IndependentVariables', jangles)

        # get the track length from the longitudes
        def tracking(longitudes): return numpy.array([longitudes.shape[-1]])
        jangles = self._jangle('orbit_track_length', {'doNotPlot': True})
        transfers.formulate('satellite_longitude', tracking, 'orbit_track_length', 'IndependentVariables', jangles)

        # find the index closest to the equator
        def indexing(latitudes): return numpy.array([numpy.argmax(-latitudes ** 2)])
        jangles = self._jangle('orbit_equatorial_index', {'doNotPlot': True})
        transfers.formulate('satellite_latitude', indexing, 'orbit_equatorial_index', 'IndependentVariables', jangles)

        # get the longitude at the equatorial index
        def equating(longitudes, indices): return numpy.array([longitudes.squeeze()[int(indices.squeeze())]])
        jangles = self._jangle('orbit_longitude', {'doNotPlot': True})
        parameters = ['satellite_longitude', 'orbit_equatorial_index']
        transfers.formulate(parameters, equating, 'orbit_longitude', 'IndependentVariables', jangles)

        # add transfers
        independents += self.cascade(transfers, features)

        # change to lower case
        [feature.dub(feature.name.lower()) for feature in independents + categories]

        # stash features
        mode = 'w'
        self._stamp('stashing {}, mode: {}...'.format(destination, notices[mode]), clock=self.clock)
        self.stash(independents + categories, destination, mode=mode)

        # status
        self._stamp('stashed.', clock=self.clock)

        return None

    def tally(self):
        """Generate report of missing orbits.

        Arguments:
            None

        Returns:
            None
        """

        # make report directory
        self._make('{}/reports'.format(self.sink))

        # construct years, months, and products
        years = [str(year) for year in list(range(2004, 2022))]
        months = [str(month).zfill(2) for month in list(range(1, 13))]
        products = ['OML1BRUG', 'OML1BRVG']

        # begin orbit counts dictionary
        orbits = {}
        for year in years:

            # add year engry
            orbits[year] = {month: {product: ['000', '000000', '000000'] for product in products} for month in months}

        # for each year
        for year in years:

            # collect all fusion files and go through each one
            fusions = self._see('{}/fusions_{}'.format(self.sink, year))
            for fusion in fusions:

                # get the month and product
                month = fusion.split('.')[-2].split('m')[-1]
                product = re.search('|'.join(products), fusion).group()

                # open h5 file and get the orbit numbers
                five = self._fetch(fusion)
                numbers = five['IndependentVariables/orbit_number'][:]

                # extract orbit info and add to collection
                info = [str(len(numbers)).zfill(3), str(numbers[0]).zfill(6), str(numbers[-1]).zfill(6)]
                orbits[year][month][product] = info

                # close h5 file
                five.close()

        # construct report
        report = ['Tally of reduced orbit numbers', '']

        # for each year
        for year in years:

            # add year
            report.append('{}:'.format(year))

            # and each month
            for month in months:

                # add month
                report.append('    {}:'.format(month))

                # and each product
                marker = '   '
                infos = [orbits[year][month][product] for product in products]
                if '/'.join(infos[0]) != '/'.join(infos[1]):

                    # set marker to alert
                    marker = '!!!'

                # print each entry
                for info, product in zip(infos, products):

                    # add product and info
                    formats = (product, info[0], info[1], info[2], marker)
                    line = '        {}: counts: {}, first: {}, last: {} {}'.format(*formats)
                    report.append(line)

        # jot report
        self._jot(report, '{}/reports/tally.txt'.format(self.sink))

        return None


# if commandline system arguments are found
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 5
    arguments = arguments[:5]

    # check for clock option to time processes
    clock = any([option in options for option in ['-clock', '--clock', '-c', '--c']])

    # check for renew option to renew the control file
    refresh = any([option in options for option in ['-refresh', '--refresh', '-r', '--r']])

    # check for meld option to meld single orbit files
    meld = any([option in options for option in ['-meld', '--meld', '-m', '--m']])

    # unpack control file name, output directory, input directory, and subdirectory range
    controller, sink, source, start, finish = arguments

    # create juggernaut instance
    juggernaut = Juggernaut(controller, sink, source, start, finish, clock)

    # if control file is to be refreshed
    if refresh:

        # create Description.txt and control.txt file from given directories
        juggernaut.control()

    # reduce each orbit to orbital mean, min, max, std, and median
    juggernaut.rampage()

    # add meld option
    if meld:

        # fuse all orbits together and take samples
        juggernaut.melt()
