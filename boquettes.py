# import reload
from importlib import reload

# import json, os
import json
import os

# import math
from math import ceil

# import datetime
from datetime import datetime, timedelta
from time import time

# import xarray
import xarray

# import numpy
import numpy

# import bokeh
from bokeh.embed import json_item
from bokeh.events import Tap, ButtonClick, MenuItemClick
from bokeh.layouts import row as Row, column as Column
from bokeh.models import HoverTool, ColumnDataSource, CheckboxGroup, Paragraph
from bokeh.models import Paragraph, Button, LinearAxis, Range1d, Legend
from bokeh.models import CustomAction, UndoTool
from bokeh.models.callbacks import CustomJS
from bokeh.plotting import figure, curdoc
from bokeh.resources import CDN

# import sklearn
from sklearn.linear_model import LinearRegression

# import pillow
from PIL import Image
import requests


# class Boquette to contain the web app
class Boquette(dict):
    """Class Boquettte to contain all bokeh plots in the bokeh app.

    Inherits from:
        dict
    """

    def __init__(self, directory='boquette'):
        """Instantiate the Boquette instance.

        Arguments:
            None

        Returns:
            None
        """

        # set current time
        self.now = time()

        # set directory
        self.directory = directory

        # inventory presets
        self.presets = {}
        self._inventory()

        # ingest the data
        self.reservoir = {}
        self.data = {}
        self.arrays = {}
        self.orbits = {}
        self.almanac = {}
        self.units = {}
        self.pixels = {}
        self._ingest()

        # build the nest
        self.nest = {}
        self.diagram = {}
        self.twigs = {}
        self._build()

        # begin the page
        self.page = None
        self.boxes = {}
        self._begin()

        return

    def _alter(self):
        """Alter the scaling of the vertical axis in javascript.

        Arguments:
            None

        Returns:
            str, javascript
        """

        # begin java
        java = """"""

        # go through each line, inserting the alteration
        java += """Object.keys(lines).forEach(function(name) {"""
        java += """  var alteration = [name, integration, normalization].join(":");"""

        # check for alteration in source data
        java += """  if (!Object.keys(source.data).includes(alteration)) {"""

        # set default pixels
        java += """    var spatial = pixels["UV-2"][0];"""
        java += """    var spectral = pixels["UV-2"][1];"""

        # check for appropriate band
        java += """    var bands = ["UV-1", "UV-2", "VIS"];"""
        java += """    bands.forEach(function(band) {"""

        # determine band for integrating over pixels
        java += """      if (graph.title.text.includes(band) || name.includes(band)) {"""
        java += """        spatial = pixels[band][0];"""
        java += """        spectral = pixels[band][1];"""

        java += """console.log("found band " + band + " !");"""

        java += """        };"""
        java += """      });"""

        java += """console.log(pixels);"""
        java += """console.log(graph.title.text);"""
        java += """console.log(name);"""
        java += """console.log(spatial);"""
        java += """console.log(spectral);"""

        # get raw native data
        java += """    var native = [name, "native", "absolute"].join(":");"""
        java += """    var data = source.data[native];"""

        # integrate by number of pixels for image integration
        java += """    if (integration === "row") {"""
        java += """      data = data.map(function(datum) {"""
        java += """        return datum * spectral;"""
        java += """        });"""
        java += """      };"""

        # integrate by number of pixels for image integration
        java += """    if (integration === "image") {"""
        java += """      data = data.map(function(datum) {"""
        java += """        return datum * spatial * spectral;"""
        java += """        });"""
        java += """      };"""

        # integrate by number of pixels and scans for count integration
        java += """    if (integration === "count") {"""
        java += """      data = data.map(function(datum, index) {"""
        java += """        return datum * spatial * spectral * scans[index];"""
        java += """        });"""
        java += """      };"""

        # convert to log scale if normalization is logarithm
        java += """    if (normalization === "logarithm") {"""

        # use the smallest value as an additive constant if it is negative
        java += """      var smallest = Math.abs(Math.min(Math.min(...data), 0));"""

        # add small buffer in case smallest is 0
        java += """      var buffer = 1e-10;"""

        # take the logarithm after adding smallest value and buffer
        java += """      data = data.map(function(datum, index) {"""
        java += """        return Math.log10(datum + smallest + buffer);"""
        java += """        });"""
        java += """      };"""

        # convert to sigma scale if normalization is sigmas
        java += """    if (normalization === "sigma") {"""

        # calculate average
        java += """      var average = data.reduce(function(datum, datumii) {"""
        java += """        return datum + datumii}, 0) / data.length;"""

        # calculate variance as the average of squares - square of average
        java += """      var squares = data.map(function(datum) {return datum ** 2});"""
        java += """      var summation = squares.reduce(function(square, squareii) {return square + squareii}, 0);"""
        java += """      var variance = (summation / squares.length) - (average ** 2);"""

        # calculate standard deviation and correct for zero case
        java += """      var deviation = Math.sqrt(variance);"""
        java += """      if (deviation === 0) {deviation = 1};"""

        # calculate the z-score (stdevs from mean)
        java += """      data = data.map(function(datum, index) {"""
        java += """        return (datum - average) / deviation;"""
        java += """        });"""
        java += """      };"""

        # square first then convert to sigma scale if normalization is square
        java += """    if (normalization === "square") {"""

        # square all data
        java += """      data = data.map(function(datum) {return datum ** 2});"""

        # calculate average
        java += """      var average = data.reduce(function(datum, datumii) {"""
        java += """        return datum + datumii}, 0) / data.length;"""

        # calculate variance as the average of squares - square of average
        java += """      var squares = data.map(function(datum) {return datum ** 2});"""
        java += """      var summation = squares.reduce(function(square, squareii) {return square + squareii}, 0);"""
        java += """      var variance = (summation / squares.length) - (average ** 2);"""

        # calculate standard deviation and correct for zero case
        java += """      var deviation = Math.sqrt(variance);"""
        java += """      if (deviation === 0) {deviation = 1};"""

        # calculate the z-score (stdevs from mean)
        java += """      data = data.map(function(datum, index) {"""
        java += """        return (datum - average) / deviation;"""
        java += """        });"""
        java += """      };"""

        # assign data to slot in source table
        java += """    source.data[alteration] = data;"""
        java += """  };"""

        # change line to read verticals
        java += """  source.data[mirror[name]] = source.data[alteration];"""
        java += """  source.change.emit();"""
        java += """});"""

        # adjust units, converting to pixel fraction for quality flag measures
        java += """var designation = units;"""
        java += """if (units.includes("fraction")) {designation = "pixel fraction"};"""

        # adjust for row integration
        java += """if (integration === "row") {"""
        java += """  designation = units + " over row";"""
        java += """  if (units.includes("fraction")) {designation = "average pixels / row"}};"""

        # adjust for image integration
        java += """if (integration === "image") {"""
        java += """  designation = units + " over image";"""
        java += """  if (units.includes("fraction")) {designation = "average pixels / image"}};"""

        # adjust for count integration
        java += """if (integration === "count") {"""
        java += """  designation = units + ' over orbit';"""
        java += """  if (units.includes("fraction")) {designation = "average pixels / orbit"}};"""

        # adjust for logarithm
        java += """if (normalization === "logarithm") {designation = "~log " + designation};"""

        # adjust for sigma
        java += """if (normalization === "sigma") {designation = "stdevs from mean " + designation};"""

        # adjust for sigma
        java += """if (normalization === "square") {designation = "stdevs from mean squared " + designation};"""

        # add new units name
        java += """axis.axis_label = designation;"""

        return java

    def _begin(self):
        """Begin the page with the data map and choice checkboxes.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.page
        """

        # begin the page
        page = []

        # add title as paragraph
        text = """OMI Trends Monitoring Prototype"""
        paragraph = Paragraph(text=text, width=700, style={'font-size': '15pt'})
        page.append(paragraph)

        # add map heading
        text = """View_data_structure"""
        text += '_' * (130 - len(text))
        paragraph = Paragraph(text=text, width=700)
        page.append(paragraph)

        # add View checkbox
        column = self.diagram['Data']['box']
        page.append(column)

        # add presets heading
        text = """View_presets"""
        text += '_' * (130 - len(text))
        paragraph = Paragraph(text=text, width=700)
        page.append(paragraph)

        # add presets checkbox
        box = CheckboxGroup(labels=['Presets'], active=[], visible=True, width=200)

        # add presets
        presets = CheckboxGroup(labels=[key for key in self.presets.keys()], active=[], visible=False, width=200)

        # make the presets boxes visible on click
        unsealing = lambda _: self.reveal(presets)
        box.on_click(unsealing)

        # transfer preset clicks to choices
        transferring = lambda _: self._transfer(presets)
        presets.on_click(transferring)

        # make into column
        column = Column([box, presets])
        page.append(column)

        # go through categories
        rows = {}
        for category in ('bands', 'modes', 'parameters'):

            # add paragraph
            text = """Choose_{}""".format(category)
            text += '_' * (130 - len(text))
            paragraph = Paragraph(text=text, width=700)
            page.append(paragraph)

            # add empty row for checkboxes later
            row = Row([])
            rows[category] = row
            page.append(row)

        # add dividing line
        text = """"""
        text += '_' * (130 - len(text))
        paragraph = Paragraph(text=text, width=700)
        page.append(paragraph)

        # make into bokeh column
        heading = Column(page)
        graphs = Column([])
        self.page = Column(heading, graphs)

        # define category specific properties
        numbers = {'bands': 3, 'modes': 3, 'parameters': 4}
        widths = {'bands': 250, 'modes': 300, 'parameters': 220}
        addresses = {'bands': 1, 'modes': 2, 'parameters': 0}

        # go through categories to make checkboxes
        for category in ('bands', 'modes'):

            # unpack category specifics
            address = addresses[category]
            number = numbers[category]
            width = widths[category]

            # create labels
            labels = self._skim([feature['name'].split(':')[address] for feature in self.values()])
            pieces = self._chop(labels, number)

            # make checkboxes and add
            checkboxes = [CheckboxGroup(labels=piece, active=[], visible=True, width=width) for piece in pieces]
            [checkbox.on_click(self.select) for checkbox in checkboxes]
            self.boxes[category] = checkboxes
            [rows[category].children.append(box) for box in checkboxes]

        # get all parameter names and initial letters
        names = self._skim([self._purge(feature['name'].split(':')[0]) for feature in self.values()])
        letters = self._skim([name[0] for name in names])

        # break into evenly sized pieces
        pieces = self._chop(letters, numbers['parameters'])

        # make callback function and set up boxes
        unsealing = lambda envelope: lambda _: self.unseal(envelope)
        self.boxes['parameters'] = []

        # create a drawer for each piece
        for piece in pieces:

            # for each letter
            drawer = []
            for letter in piece:

                # add checkbox
                width = widths['parameters']
                box = CheckboxGroup(labels=[letter.upper()], active=[], visible=True, width=width)

                # add checkbox group for names
                members = [member for member in names if member.startswith(letter)]
                group = CheckboxGroup(labels=members, active=[], visible=False, width=width)
                self.boxes['parameters'].append(group)

                # put into envelope and add to drawer
                envelope = Column([box, group])
                drawer.append(envelope)

                # add click instruction for letter box and group box
                box.on_click(unsealing(envelope))
                group.on_click(self.select)

            # make drawer into column and add to row
            drawer = Column(drawer)
            rows['parameters'].children.append(drawer)

        return None

    def _build(self):
        """Build the nest.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.nest
            self.diagram
        """

        # start timing
        self._stamp('building nests...')

        # load nests
        paths = ['{}/{}'.format(self.directory, path) for path in os.listdir(self.directory)]
        paths = [path for path in paths if 'nest.json' in path]

        # begin collections
        nest = {'Data': {}}
        for path in paths:

            # load nest
            collection = self._load(path)

            # get tag
            tag = path.split('/')[-1].split('_')[0]

            # attach to nest
            nest['Data'][tag] = collection['Data']

        # set nest
        self.nest = nest

        # make checkbox diagram
        diagram = self._graft(nest)
        self.diagram = diagram

        # strip off all forks
        twigs = self._pluck(diagram)
        self.twigs = twigs

        # status
        self._stamp('nests built.')

        return None

    def _chop(self, words, number):
        """Chop a list of words into roughly equal parts.

        Arguments:
            words: list of str
            number: int, number of pieces

        Returns:
            list of lists of words
        """

        # get the chunk size
        chunk = ceil(len(words) / number)

        # get the pieces
        pieces = [words[index * chunk: (index + 1) * chunk] for index in range(number)]

        return pieces

    def _cycle(self):
        """Define javascript code for adding orbital cycle to graph.

        Arguments:
            None

        Returns:
            None
        """

        # begin javascript
        java = """"""

        # make longitudinal line visible and set range
        java += """cycle.visible = !cycle.visible;"""
        java += """axis.start = 1400;"""
        java += """axis.end = 2100;"""
        java += """secondary.axis_label = 'orbital cycle (images / orbit)';"""

        # get relevant font size status from graph tag
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("glasses");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # set axis labels
        java += """secondary.axis_label_text_font_size = status;"""
        java += """secondary.major_label_text_font_size = status;"""

        # remove other secondary plots invisible
        java += """seasons.visible = false;"""
        java += """globe.visible = false;"""

        return java

    def _designate(self, parameter, operation):
        """Designate a parameter name as a new parameter name for use in graphs.

        Arguments:
            parameter: str
            operation: str

        Returns:
            str
        """

        # split parameter on colons
        name, product, mode = parameter.split(':')

        # pick collection
        collection = 'Collection-4'
        if 'Earth' in product:

            # set collection
            collection = 'Collection-3'

        # get band name
        band = 'UV-1'
        if '2' in product:

            # set to uv2
            band = 'UV-2'

        # check for band3
        if '3' in product or 'VIS' in product:

            # set to uv2
            band = 'VIS'

        # strip digits off of name
        name = self._purge(name)

        # abbreviate operation name
        abbreviations = {'mean': 'mean', 'median': 'median', 'minimum': 'min', 'maximum': 'max', 'deviation': 'stdev'}
        operation = abbreviations[operation]

        # construct designation
        designation = ':'.join([collection, band, name, operation])

        return designation

    def _dip(self):
        """Dip the quill to change line style.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # associate legend items with their item names
        java += """var diagram = legend[0].items.map(function(item) {"""
        java += """  return [item.label.value, item];"""
        java += """  });"""
        java += """var items = Object.fromEntries(diagram);"""

        # empty all legend item renderers
        java += """Object.keys(items).forEach(function(name) {"""
        java += """  items[name].renderers = [];"""
        java += """  });"""

        # get relevant status from graph tag marked by quill
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("quill");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # get new size from glasses
        java += """var style = quill[status];"""

        # remove old tag and add new to advance dial
        java += """graph.tags = graph.tags.filter(function(tag) {"""
        java += """  return !tag.includes("quill");"""
        java += """  });"""
        java += """graph.tags.push("quill:" + style);"""

        # go through each line and marker
        java += """Object.keys(items).forEach(function(name) {"""
        java += """  var line = lines[name];"""
        java += """  var circle = circles[name];"""
        java += """  var renderers = items[name].renderers;"""

        # repopulate legend with lines if appropriate
        java += """  if (style == "lines" || style == "both") {"""
        java += """    renderers.push(line);"""
        java += """    };"""

        # repopulate legend with markers if appropriate
        java += """  if (style == "dots" || style == "both") {"""
        java += """    renderers.push(circle);"""
        java += """    };"""

        # if line is at least partially visible
        java += """  if (line.visible || circle.visible) {"""

        # set visibilities for lines
        java += """    if (style === 'lines') {"""
        java += """      line.visible = true;"""
        java += """      circle.visible = false;"""
        java += """      };"""
        #
        # set visibilities for circles
        java += """    if (style === "dots") {"""
        java += """      line.visible = false;"""
        java += """      circle.visible = true;"""
        java += """      };"""

        # set visibilities for both
        java += """    if (style === "both") {"""
        java += """      line.visible = true;"""
        java += """      circle.visible = true;"""
        java += """      };"""
        java += """    };"""
        java += """  });"""

        return java

    def _dye(self):
        """Rotate through color palette.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # go through each line and circle
        java += """Object.keys(lines).forEach(function(name) {"""
        java += """  var line = lines[name];"""
        java += """  var circle = circles[name];"""

        # and change color
        java += """  line.glyph.line_color = palette[line.glyph.line_color];"""
        java += """  circle.glyph.line_color.value = palette[circle.glyph.fill_color.value];"""
        java += """  circle.glyph.fill_color.value = palette[circle.glyph.fill_color.value];"""
        java += """  });"""

        return java

    def _export(self):
        """Create javascript code for exporting csv.

        Arguments:
            None

        Returns:
            str
        """

        javascript = """
        function table_to_csv(source) {
            const columns = Object.keys(source.data)
            const nrows = source.get_length()
            const lines = [columns.join(',')]

            for (let i = 0; i < nrows; i++) {
                let row = [];
                for (let j = 0; j < columns.length; j++) {
                    const column = columns[j]
                    row.push(source.data[column][i].toString())
                }
                lines.push(row.join(','))
            }
            return lines.join('\\n').concat('\\n')
        }


        const filename = 'data_result.csv'
        var filetext = table_to_csv(source)
        const blob = new Blob([filetext], { type: 'text/csv;charset=utf-8;' })

        //addresses IE
        if (navigator.msSaveBlob) {
            navigator.msSaveBlob(blob, filename)
        } else {
            const link = document.createElement('a')
            link.href = URL.createObjectURL(blob)
            link.download = filename
            link.target = '_blank'
            link.style.visibility = 'hidden'
            link.dispatchEvent(new MouseEvent('click'))
        }
        """

        return javascript

    def _fork(self, column):
        """Set new fork onto nesting map.

        Arguments:
            column: bokeh column window

        Returns:
            None
        """

        # get all checkboxes by column id
        identifier = column.id
        boxes = self.twigs[identifier]

        # check for active status
        if len(column.children[0].active) > 0:

            # check for terminal node
            if len(boxes) > 0:

                # add in appropriate branches
                [column.children.append(box) for box in boxes]

            # otherwise
            else:

                # propagate selections to choices list
                self._propagate(column)

        # otherwise
        else:

            # remove all checkbox columns
            while len(column.children) > 1:

                # discard
                discard = column.children.pop()

            # prune selections if at a node
            if len(boxes) < 1:

                # prune
                self._prune(column)

        return None

    def _graft(self, nest, route=None, diagram=None, tab=0):
        """Set new branch onto nesting map.

        Arguments:
            nest: paramters nest
            route=None: str, data path
            diagram=None: dict of lists
            tab=0: amount to tab over

        Returns:
            None

        Populates:
            self.diagram
        """

        # set default diagram and route
        diagram = diagram or {}
        route = route or ''

        # try to
        try:

            # go through all members
            clicking = lambda column: lambda _: self._fork(column)
            for name, branches in nest.items():

                # make checkbox
                margin = (2, 50 * tab + 5, 2, 50 * tab + 5)
                box = CheckboxGroup(labels=[name], active=[], visible=True, width=200, margin=margin)

                # make column object around box
                column = Column([box])

                # add to route and set to column name
                structure = '{}:{}'.format(name, route)
                column.name = structure

                # add click action
                box.on_click(clicking(column))

                # add to diagram
                diagram.update({name: {'box': column, 'children': {}}})
                diagram[name]['children'].update(self._graft(branches, structure, diagram[name]['children'], tab + 1))

        # otherwise
        except AttributeError:

            # terminate growth
            pass

        return diagram

    def _ingest(self):
        """Ingest the hdf5 files into features.

        Arguments:
            None

        Returns:
            None

        Populates:
            self
        """

        # start timer
        self._stamp('ingesting data into xarrays...')

        # load in datasets
        directory = self.directory
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory) if '.nc' in path]
        arrays = {path: xarray.open_dataarray(path) for path in paths}
        self.arrays = arrays

        # extract features
        self._stamp('extracting features...')
        features = []
        lexicon = {}
        for path, array in arrays.items():

            # get units from attrs
            attributes = array.attrs
            units = attributes['units'].split(':')

            # go through parameters
            parameters = [parameter for parameter in array.coords['parameter'].data]
            for parameter, unit in zip(parameters, units):

                # add feature
                feature = {'name': parameter, 'units': unit, 'path': path, 'manifestation': None}
                features.append(feature)

                # add entry to units
                name = parameter.split(':')[0]
                lexicon[name] = unit

                # also strip off leading digits and add to units
                lexicon[self._purge(name, criterion=50)] = unit

        # add units attribute
        self.units = lexicon

        # add pixel quantities
        pixels = {'UV-1': [30, 159], 'UV-2': [60, 557], 'VIS': [60, 751]}
        self.pixels = pixels

        # populate
        self._stamp('populating features...')
        [self.update({feature['name']: feature}) for feature in features]

        # synchronize orbit numbers
        self._stamp('sychronizing orbits...')
        self._synchronize()

        # print status
        self._stamp('data ingested.')

        return None

    def _integrate(self):
        """Construct javascript code for adjusting integration scale.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # get relevant status from graph tags marked by layers
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("layers");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # get new integration from layers
        java += """var integration = layers[status];"""

        # remove old tag and add new to advance dial
        java += """graph.tags = graph.tags.filter(function(tag) {"""
        java += """  return !tag.includes("layers");"""
        java += """  });"""
        java += """graph.tags.push("layers:" + integration);"""

        # get relevant status from graph tags marked by glasses
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("scales");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # get new size from glasses
        java += """var normalization = status;"""

        # rescale axis
        java += self._alter()

        # tighten fit
        java += self._tighten()

        return java

    def _inventory(self):
        """Take inventory of quality flags.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.presets
        """

        # begin presets
        presets = {}

        # get the contents of the flags file
        text = self._transcribe('presets.txt')

        # join with colons and separate by blank lines
        text = ':'.join(text)
        groups = text.split('::')

        # collect groups
        for group in groups:

            # split by colons to get members
            lines = group.split(':')
            name = lines[0]
            members = lines[1:]

            # update presets
            presets[name] = members

        # set flags
        self.presets = presets

        return None

    def _load(self, path):
        """Load a json file.

        Arguments:
            path: str, file path

        Returns:
            dict
        """

        # open json file
        with open(path, 'r') as pointer:

            # get contents
            contents = json.load(pointer)

        return contents

    def _magnify(self):
        """Increase text size.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin javascript
        java = """"""

        # get relevant status from graph tags marked by glasses
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("glasses");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # get new size from glasses
        java += """var size = glasses[status];"""

        # remove old tag and add new to advance dial
        java += """graph.tags = graph.tags.filter(function(tag) {"""
        java += """  return !tag.includes("glasses");"""
        java += """  });"""
        java += """graph.tags.push("glasses:" + size);"""

        # adjust title and legend fonts
        java += """graph.title.text_font_size = size;"""
        java += """legend[0].label_text_font_size = size;"""

        # adjust xaxis fonts
        java += """xaxis.axis_label_text_font_size = size;"""
        java += """xaxis.major_label_text_font_size = size;"""

        # adjust yaxis fonts
        java += """yaxis.axis_label_text_font_size = size;"""
        java += """yaxis.major_label_text_font_size = size;"""

        # adjust secondary axis if already visible
        java += """if (yiiaxis.axis_label_text_font_size != '0pt') {"""
        java += """  yiiaxis.axis_label_text_font_size = size;"""
        java += """  yiiaxis.major_label_text_font_size = size;"""
        java += """  };"""

        return java

    def _normalize(self):
        """Construct javascript code for adjusting normalization scale.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # get relevant status from graph tags marked by scales
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("scales");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # get new integration from layers
        java += """var normalization = scales[status];"""

        # remove old tag and add new to advance dial
        java += """graph.tags = graph.tags.filter(function(tag) {"""
        java += """  return !tag.includes("scales");"""
        java += """  });"""
        java += """graph.tags.push("scales:" + normalization);"""

        # get relevant status from graph tags marked by layers
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("layers");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # get new size from glasses
        java += """var integration = status;"""

        # rescale axis
        java += self._alter()

        # tighten fit
        java += self._tighten()

        return java

    def _orient(self):
        """Locate the legend.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # get relevant status from graph tags marked by compass
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("compass");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # get new corner by advancing compass
        java += """var corner = compass[status];"""

        # remove old tag and add new to advance dial
        java += """graph.tags = graph.tags.filter(function(tag) {"""
        java += """  return !tag.includes("compass");"""
        java += """  });"""
        java += """graph.tags.push("compass:" + corner);"""

        # change legend location
        java += """legend[0].location = corner;"""

        return java

    def _overlap(self, name, labels, criterion=2):
        """Determine if there is enough overlap between the name and selected labels to choose the feature.

        Arguments:
            name: str, feature name
            labels: selected labels
            criterion=2: number of correct selections to trigger overlap

        Returns:
            boolean
        """

        # get words from parameter name after removing digits
        name = self._purge(name.split(':')[0])
        words = name.split('_')

        # determine number of words in labels
        number = sum([int(word in labels) for word in words])

        # if enough words overlap
        answer = False
        if number >= min([criterion, len(words)]):

            # set answer to true
            answer = True

        return answer

    def _parse(self, suite):
        """Parse a suite of feature parameters into various labels.

        Arguments:
            suite: dict

        Returns:
            tuple of str and list of str
        """

        # get the keys
        features = [feature for feature in suite.keys()]

        # get all sets of parallel designations
        ingots = [ingot for ingot in zip(*[feature.split(':') for feature in features])]

        # get indices for static designations
        statics = [ingot[0] for ingot in ingots if len(set(ingot)) < 2]
        dynamics = [ingot for ingot in ingots if len(set(ingot)) > 1]

        # construct title from statics
        title = '/'.join(statics)

        # construct labels from dynamics
        labels = ['/'.join(dynamic) for dynamic in zip(*dynamics)]

        return title, labels

    def _pluck(self, diagram, twigs=None):
        """Pluck off all twigs from the checkbox nesting diagram.

        Arguments:
            diagram: dict of dicts
            twigs: dict

        Returns:
            dict
        """

        # set default twigs
        if twigs is None:

            # set to empty
            twigs = {}

        # go through each branch
        for name, branch in diagram.items():

            # find the column id
            identifier = branch['box'].id

            # collect the children
            children = [branch['children'][child]['box'] for child in branch['children'].keys()]

            # add entry to fork
            twigs[identifier] = children

            # check next branch
            twigs = self._pluck(branch['children'], twigs)

        return twigs

    def _propagate(self, column):
        """Propagate selections from data structure to active boxes in choices.

        Arguments:
            column: str, column id

        Returns:
            None
        """

        # extract groups from column name
        groups = column.name.split(':')
        band = groups[-4]
        mode = groups[-5]
        parameter = groups[0]
        words = self._purge(parameter).split('_')

        # activate band checkbox
        box = [member for member in self.boxes['bands'] if band in member.labels][0]
        box.active.append(box.labels.index(band))

        # activate mode checkbox
        box = [member for member in self.boxes['modes'] if mode in member.labels][0]
        box.active.append(box.labels.index(mode))

        # activate all word boxes
        for word in words:

            # check for presence
            boxes = [member for member in self.boxes['parameters'] if word in member.labels]
            if len(boxes) > 0:

                # and activate
                box = boxes[0]
                box.active.append(box.labels.index(word))

        return None

    def _prune(self, column):
        """Prune selections from data structures that turn inactive.

        Arguments:
            column: str, column id

        Returns:
            None
        """

        # extract words from column name
        parameter = column.name.split(':')[0]
        words = self._purge(parameter).split('_')

        # deactive all word boxes
        for word in words:

            # check for presence
            boxes = [member for member in self.boxes['parameters'] if word in member.labels]
            if len(boxes) > 0:

                # and deactivate
                box = boxes[0]
                index = box.labels.index(word)
                box.active = [entry for entry in box.active if entry != index]

        return None

    def _purge(self, word, criterion=30):
        """Purge digits from a word.

        Arguments:
            word: str

        Return:
            str
        """

        # replace all digits
        for digit in range(10):

            # replace each digit or digit underscore combo with a blank
            word = word.replace('_' + str(digit), '').replace(str(digit) + '_', '').replace(str(digit), '')

        # add ellipses if word is longer than criterion
        if len(word) > criterion:

            # add ellipses instead
            front = int(criterion / 2)
            back = criterion - (front + 3)
            word = '{}...{}'.format(word[:front], word[-back:])

        return word

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # make paths
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        return paths

    def _shine(self):
        """Define javascript code for adding seasonal cycle to graph.

        Arguments:
            None

        Returns:
            None
        """

        # begin javascript
        java = """"""

        # make seasonal line visible and set axis range
        java += """seasons.visible = !seasons.visible;"""
        java += """axis.start = 0.97;"""
        java += """axis.end = 1.03;"""
        java += """secondary.axis_label = 'earth sun distance (au)';"""

        # get relevant font size status from graph tag
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("glasses");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # set axis labels
        java += """secondary.axis_label_text_font_size = status;"""
        java += """secondary.major_label_text_font_size = status;"""

        # remove other secondary plots invisible
        java += """globe.visible = false;"""
        java += """cycle.visible = false;"""

        return java

    def _skim(self, members):
        """Skim off the unique members from a list.

        Arguments:
            members: list

        Returns:
            list
        """

        # trim duplicates and sort
        members = list(set(members))
        members.sort()

        return members

    def _stamp(self, message, initial=False):
        """Start timing a block of code, and print results with a message.

        Arguments:
            message: str
            initial: boolean, initial message of block?

        Returns:
            None
        """

        # get final time
        final = time()

        # calculate duration and reset time
        duration = round(final - self.now, 5)
        self.now = final

        # if iniital message
        if initial:

            # add newline
            message = '\n' + message

        # if not an initial message
        if not initial:

            # print duration
            print('took {} seconds.'.format(duration))

        # begin new block
        print(message)

        return None

    def _swivel(self, settings):
        """Turn a list of settings into a dial.

        Arguments:
            settings: list of str

        Returns:
            dict, a settings dial
        """

        # add first setting to end
        initial = settings[0]
        settings.append(initial)

        # create dial
        dial = {first: second for first, second in zip(settings[:-1], settings[1:])}

        return dial

    def _synchronize(self):
        """Synchronize all orbit numbers, keeping only those with full records.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.orbits
            self.almanac
        """

        # load in orbits file
        orbits = self._load('{}/orbits.json'.format(self.directory))
        self.orbits = orbits

        # get lists from arrays
        registry = {}
        for path, array in self.arrays.items():

            # get all orbit numbers
            numbers = [str(int(coordinate.split('-o')[1])) for coordinate in array.coords['orbit'].data]
            registry[path] = numbers

        # get all orbital numbers that are in all arrays
        numbers = [key for key in orbits.keys() if all([key in register for register in registry.values()])]

        # filter each array
        for path, register in registry.items():

            # get subset of indices that are in orbits
            indices = [index for index, number in enumerate(register) if number in numbers]

            # filter out array
            array = self.arrays[path]
            array = array[:, :, indices]
            self.arrays[path] = array

        # construct almanac
        numbers.sort(key=lambda number: int(number))
        longitudes = [orbits[number]['longitude'] for number in numbers]
        suns = [orbits[number]['sun'] for number in numbers]
        scans = [orbits[number]['scans'] for number in numbers]
        dates = [orbits[number]['date'] for number in numbers]
        times = [datetime.strptime(date, '%Ym%m%dt%H%M') for date in dates]
        numbers = [int(number) for number in numbers]
        almanac = {'longitudes': longitudes, 'scans': scans, 'dates': dates, 'times': times, 'numbers': numbers}
        almanac.update({'suns': suns})
        self.almanac = almanac

        return None

    def _tighten(self):
        """Tighten the scaling to fit all visible lines.

        Arguments:
            None

        Returns:
            str, javascript
        """

        # begin java
        java = """"""

        # gather all visible glyphs
        java += """var visibles = Object.values(lines).filter(function(line) {return line.visible});"""
        java += """var visiblesii = Object.values(circles).filter(function(line) {return line.visible});"""
        java += """visibles = visibles.concat(visiblesii);"""

        # find the maximum amongst all glyphs
        java += """var maximum = Math.max(...visibles.map(function(line) {"""
        java += """  return Math.max(...source.data[line.glyph.y.field]);"""
        java += """  }));"""

        # find the minimum amonst all glyphs
        java += """var minimum = Math.min(...visibles.map(function(line) {"""
        java += """  return Math.min(...source.data[line.glyph.y.field]);"""
        java += """  }));"""

        # calculate the margin based on the difference between the two, with a limit
        java += """var margin = Math.max(0.1 * (maximum - minimum), 1e-10);"""

        # make new bounds
        java += """graph.y_range.start = minimum - margin;"""
        java += """graph.y_range.end = maximum + margin;"""

        return java

    def _transcribe(self, path):
        """Transcribe the text file at the path.

        Arguments:
            path: str, file path

        Returns:
            list of str
        """

        # read in file pointer
        with open(path, 'r') as pointer:

            # read in text and eliminate endlines
            lines = pointer.readlines()
            lines = [line.replace('\n', '') for line in lines]

        return lines

    def _triangulate(self):
        """Define javascript code for adding longitude to graph.

        Arguments:
            None

        Returns:
            None
        """

        # begin javascript
        java = """"""

        # make longitudinal line visible and set range
        java += """globe.visible = !globe.visible;"""
        java += """axis.start = -180;"""
        java += """axis.end = 180;"""
        java += """secondary.axis_label = 'equatorial longitude (degrees)';"""

        # get relevant font size status from graph tag
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("glasses");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # set axis labels
        java += """secondary.axis_label_text_font_size = status;"""
        java += """secondary.major_label_text_font_size = status;"""

        # remove other secondary plots invisible
        java += """seasons.visible = false;"""
        java += """cycle.visible = false;"""

        return java

    def _transfer(self, check):
        """Propagate selections from data structure to active boxes in choices.

        Arguments:
            check: checkboxes

        Returns:
            None
        """

        # # extract groups from column name
        # groups = column.name.split(':')
        # band = groups[-4]
        # mode = groups[-5]
        # parameter = groups[0]
        # words = self._purge(parameter).split('_')
        #
        # # activate band checkbox
        # box = [member for member in self.boxes['bands'] if band in member.labels][0]
        # box.active.append(box.labels.index(band))
        #
        # # activate mode checkbox
        # box = [member for member in self.boxes['modes'] if mode in member.labels][0]
        # box.active.append(box.labels.index(mode))

        # get all active labels
        activities = [check.labels[index] for index in check.active]

        # activate all word boxes
        for activity in activities:

            # get preset words
            words = self.presets[activity]
            for word in words:

                # check for presence
                boxes = [member for member in self.boxes['parameters'] if word in member.labels]
                if len(boxes) > 0:

                    # and activate
                    box = boxes[0]
                    box.active.append(box.labels.index(word))

        return None

    def _wash(self, icon, degree, destination):
        """Wash out a solid icon and redestination.

        Arguments:
            icon: str, filepath to icon
            degree: float, lightening factor
            destination: str, destination filepath

        Returns:
            None
        """

        # open up image and convert to array
        image = numpy.array(Image.open(icon))

        # multiply by lightening factor
        image = image * degree
        image = image.astype('uint8')

        # redestination
        Image.fromarray(image).save(destination)

        return None

    def archive(self):
        """Dump copies of all band files into the archive folder.

        Arguments:
            None

        Returns:
            None
        """

        # go through each array
        for path, array in self.arrays.items():

            # get immediate directory name
            directory = path.split('/')[-2]

            # get datestr
            date = str(datetime.now().replace(microsecond=0, second=0)).replace(' ', '_')

            # create archive path name
            destination = path.replace(directory, 'archive').replace('.nc', '_{}.nc'.format(date))
            print('saving {} to {}...'.format(path, destination))

            # save the array
            array.to_netcdf(destination)

        return None

    def compare(self, graph):
        """Draw a graph of all selected graphs for comparison.

        Arguments:
            graph: bokeh graph object
            numbers: list of ints
            dates: list of date strings
            times: list of datetimes
            scans: list of ints
            longitudes: list of floats
            normalize: boolean, normalize each line?

        Returns:
            None
        """

        # find right cell in the page
        for child in self.page.children[1].children:

            # check for length
            if len(child.children) > 1:

                # check for graph id
                if child.children[1].id == graph.id:

                    # set cell
                    cell = child

        # check for missing graph based on the cell length
        if len(cell.children[2].children) < 2:

            # find all selected graphs
            selections = [cell.children[1] for cell in self.page.children[1].children if len(cell.children) > 1 and cell.children[1].visible]

            # collect all data
            data = [plot.renderers[0].data_source for plot in selections]

            # determine first visible line for each plot
            targeting = lambda visibles: 'mean' if len(visibles) < 1 else visibles[0]
            targets = [targeting([line.glyph.y for line in plot.renderers[:5] if line.visible]) for plot in selections]

            # make suite
            suite = {target: datum.data[target] for target, datum in zip(targets, data)}

            # make comparison graph
            comparison = self.paint(suite)

            # add graph to cell
            cell.children[2].children.append(comparison)

        # otherwise
        else:

            # pop out member
            discard = cell.children[2].children.pop()

        return None

    def draw(self, feature):
        """Draw the plot based on a feature.

        Arguments:
            feature: dict

        Returns:
            list of bokeh objects.
        """

        # unpack feature
        name = feature['name']
        path = feature['path']
        array = self.arrays[path]

        # get data
        suite = {}
        for operation in ('mean', 'deviation', 'maximum', 'median', 'minimum'):

            # grab layer
            layer = numpy.array(array.sel(parameter=name, reduction=operation))
            label = self._designate(name, operation)
            suite[label] = layer

        # paint graph
        graph = self.paint(suite)

        # add comparison checkboxes
        box = CheckboxGroup(labels=['Compare selected parameters'], active=[], width=500, visible=True, margin=(10, 50, 10, 50))
        box.on_click(lambda _: self.compare(graph))
        box = Column([box])

        # make children
        children = [graph, box]

        return children

    def manifest(self, feature):
        """Manifest the miniature plots of a feature.

        Arguments:
            feature: dict

        Returns:
            bokeh object.
        """

        # check for current manifestation
        if feature['manifestation']:

            return feature['manifestation']

        # unpack feature
        name = feature['name']
        path = feature['path']
        array = self.arrays[path]

        # get data, discarding nans and infs
        reductions = {}
        for operation in ('mean', 'deviation'):

            # grab layer
            layer = array.sel(parameter=name, reduction=operation).data
            reductions[operation] = layer.data

        # make label
        label = '{}'.format(name)

        # make checkbox
        checkbox = CheckboxGroup(labels=[label.replace(':', '/')], active=[], width=540, visible=True)
        checkbox.on_click(lambda _: self.toggle(feature))

        # make miniature figures
        figures = [checkbox]
        colors = ('blue', 'purple')
        for reduction, abbreviation, color in zip(('mean', 'deviation'), ('mean', 'stdev'), colors):

            # begin figure and set label
            miniature = figure(sizing_mode="fixed", plot_width=200, plot_height=60, visible=True)
            miniature.xaxis.axis_label = abbreviation

            # plot the line
            y = reductions[reduction]
            x = [index for index, _ in enumerate(y)]
            miniature.line(x=x, y=y, color=color)

            # remove ticks
            miniature.xaxis.major_label_text_font_size = '0pt'
            miniature.yaxis.major_label_text_font_size = '0pt'

            # remove log
            miniature.toolbar.logo = None
            miniature.toolbar_location = None

            # add to figures
            figures.append(miniature)

        # make row of graphs and add to reservoir
        row = Row(figures)

        # make cell
        cell = Column([row])
        feature['manifestation'] = cell

        return cell

    def paint(self, suite):
        """Draw the plot based on a feature.

        Arguments:
            suite: dict of data

        Returns:
            bokeh object.
        """

        # grab almanac
        numbers = self.almanac['numbers']
        longitudes = self.almanac['longitudes']
        times = self.almanac['times']
        scans = self.almanac['scans']
        dates = self.almanac['dates']
        suns = self.almanac['suns']

        # get all names
        names = [name for name in suite.keys()]

        # set primary y range according to mean
        first = suite[names[0]]
        difference = max(first) - min(first)
        margin = max([0.05 * difference, 1e-10])
        primary = (min(first) - margin, max(first) + margin)

        # parse suite keys into title words and legend labels
        title, labels = self._parse(suite)

        # determine units
        units = ','.join(set([self.units[parameter.split(':')[-2]] for parameter in suite.keys()]))

        # begin figure
        options = {'title': title, 'sizing_mode': 'fixed', 'plot_width': 1000, 'plot_height': 700}
        options.update({'visible': True, 'y_range': primary, 'margin': (0, 50, 0, 50)})
        options.update({'x_axis_type': 'datetime'})
        graph = figure(**options)

        # create data table from standard data and suite of data
        table = {'timepoint': times, 'number': numbers, 'date': dates}
        table.update(suite)

        # augment table with native, absolute forms
        for label, name in zip(labels, names):

            # add nonintegrated, absolute scale forms
            field = ':'.join([label, 'native', 'absolute'])
            table[field] = table[name]

        # create source table object
        source = ColumnDataSource(table)

        # create mirror
        mirror = {label: name for label, name in zip(labels, names)}

        # create annotations
        annotations = [('number', '@number'), ('date', '@date')]
        for name, label in zip(names, labels):

            # add annotations
            annotations += [(label, '@{' + name + '}')]

        # add hover tool
        hover = HoverTool(tooltips=annotations, names=['one'])
        graph.add_tools(hover)

        # add undo tool
        graph.add_tools(UndoTool())

        # add default first axis lines and colors
        colors = ['blue', 'magenta', 'green', 'orange', 'purple', 'red']
        widths = (2, 2, 2, 2, 2, 2)
        visibilities = (True, False, False, False, False, False)

        # graph all lines
        lines = {}
        circles = {}
        zipper = [quintet for quintet in zip(names, labels, colors, widths, visibilities)]
        for name, label, color, width, visibility in zipper:

            # make line and circle options
            options = {'source': source, 'x': 'timepoint', 'y': name, 'color': color, 'line_width': width}
            options.update({'visible': visibility, 'legend_label': label, 'name': 'one'})

            # make arguments for tighten js call
            arguments = {'lines': lines, 'circles': circles, 'graph': graph, 'source': source}

            # render line, attaching tightening fit on visibility
            line = graph.line(**options)
            line.js_on_change('visible', CustomJS(args=arguments, code=self._tighten()))
            lines[label] = line

            # render associated circle markers in similar fashion
            circle = graph.circle(**options)
            circle.js_on_change('visible', CustomJS(args=arguments, code=self._tighten()))
            circles[label] = circle

        # set legend options
        legend = graph.legend
        legend.click_policy = 'hide'
        legend.label_text_font_size = '9pt'

        # Add secondary y-axis
        graph.extra_y_ranges['cycle'] = Range1d(start=-1, end=1)
        secondary = LinearAxis(y_range_name='cycle', axis_label='_')
        graph.add_layout(secondary, 'right')

        # set axis labels and font sizes
        graph.xaxis.axis_label = 'date'
        graph.yaxis.axis_label = units
        graph.xaxis.axis_label_text_font_size = '9pt'
        graph.yaxis.axis_label_text_font_size = '9pt'
        graph.xaxis.major_label_text_font_size = '9pt'
        graph.yaxis.major_label_text_font_size = '9pt'
        graph.title.text_font_size = '9pt'

        # set axis labels
        graph.yaxis[1].axis_label_text_font_size = '0pt'
        graph.yaxis[1].major_label_text_font_size = '0pt'

        # grab data source
        source = graph.renderers[0].data_source

        # make csv button
        callback = CustomJS(args=dict(source=source), code=self._export())
        custom = CustomAction(action_tooltip='Csv', icon='icons/download.png', callback=callback)
        graph.add_tools(custom)

        # add custom action button for changing text size
        sizes = ['9pt', '10pt', '11pt', '12pt', '13pt', '14pt', '15pt', '16pt', '17pt']
        glasses = self._swivel(sizes)
        graph.tags.append('glasses:' + sizes[0])
        arguments = {'glasses': glasses, 'graph': graph, 'legend': legend}
        arguments.update({'xaxis': graph.xaxis[0], 'yaxis': graph.yaxis[0], 'yiiaxis': graph.yaxis[1]})
        callback = CustomJS(args=arguments, code=self._magnify())
        custom = CustomAction(action_tooltip='Text', icon='icons/glasses.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for changing legend location
        corners = ['top_right', 'bottom_right', 'bottom_left', 'top_left']
        compass = self._swivel(corners)
        graph.tags.append('compass:' + corners[0])
        arguments = {'compass': compass, 'graph': graph, 'legend': legend}
        callback = CustomJS(args=arguments, code=self._orient())
        custom = CustomAction(action_tooltip='Legend', icon='icons/compass.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for changing colors
        palette = self._swivel(colors)
        arguments = {'palette': palette, 'lines': lines, 'circles': circles}
        callback = CustomJS(args=arguments, code=self._dye())
        custom = CustomAction(action_tooltip='Colors', icon='icons/palette.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for changing markers
        styles = ['both', 'lines', 'dots']
        quill = self._swivel(styles)
        graph.tags.append('quill:' + styles[0])
        arguments = {'quill': quill, 'graph': graph, 'lines': lines, 'circles': circles, 'legend': legend}
        callback = CustomJS(args=arguments, code=self._dip())
        custom = CustomAction(action_tooltip='Markers', icon='icons/feather.png', callback=callback)
        graph.add_tools(custom)

        # create secondary axis lines
        seasons = graph.line(x=times, y=suns, color='gray', line_width=1.0, y_range_name='cycle', visible=False)
        globe = graph.line(x=times, y=longitudes, color='gray', line_width=1.0, y_range_name='cycle', visible=False)
        cycle = graph.line(x=times, y=scans, color='gray', line_width=1.0, y_range_name='cycle', visible=False)

        # add seasonal plot to secondary axis
        arguments = {'seasons': seasons, 'graph': graph, 'secondary': secondary, 'axis': graph.extra_y_ranges['cycle']}
        arguments.update({'globe': globe, 'cycle': cycle})
        callback = CustomJS(args=arguments, code=self._shine())
        custom = CustomAction(action_tooltip='Seasons', icon='icons/sun.png', callback=callback)
        graph.add_tools(custom)

        # add longitudinal plot on secondary y axis
        arguments = {'globe': globe, 'graph': graph, 'secondary': secondary, 'axis': graph.extra_y_ranges['cycle']}
        arguments.update({'seasons': seasons, 'cycle': cycle})
        callback = CustomJS(args=arguments, code=self._triangulate())
        custom = CustomAction(action_tooltip='Longitudes', icon='icons/globe.png', callback=callback)
        graph.add_tools(custom)

        # add orbital cycle plot on secondary y axis
        arguments = {'cycle': cycle, 'graph': graph, 'secondary': secondary, 'axis': graph.extra_y_ranges['cycle']}
        arguments.update({'seasons': seasons, 'globe': globe})
        callback = CustomJS(args=arguments, code=self._cycle())
        custom = CustomAction(action_tooltip='Orbits', icon='icons/satellite.png', callback=callback)
        graph.add_tools(custom)

        # create integration attributes
        integrations = ['native', 'row', 'image', 'count']
        layers = self._swivel(integrations)
        graph.tags.append('layers:' + integrations[0])

        # create normalization attributes
        normalizations = ['absolute', 'logarithm', 'sigma', 'square']
        scales = self._swivel(normalizations)
        graph.tags.append('scales:' + normalizations[0])

        # create arguments
        arguments = {'layers': layers, 'scales': scales, 'graph': graph, 'source': source, 'axis': graph.yaxis[0]}
        arguments.update({'lines': lines, 'circles': circles, 'scans': scans, 'pixels': self.pixels, 'mirror': mirror})
        arguments.update({'units': units})

        # add custom action to integrate scale
        callback = CustomJS(args=arguments, code=self._integrate())
        custom = CustomAction(action_tooltip='Integrate', icon='icons/layers.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for normalizing
        callback = CustomJS(args=arguments, code=self._normalize())
        custom = CustomAction(action_tooltip='Scale', icon='icons/scale.png', callback=callback)
        graph.add_tools(custom)

        # remove logo
        graph.toolbar.logo = None

        return graph

    def reveal(self, check):
        """Reveal a hidden checkbox.

        Arguments:
            None

        Returns:
            None
        """

        # change visibility
        check.visible = not check.visible

        return None

    def select(self, event):
        """Select features from checkboxes.

        Arguments:
            event: dict

        Returns:
            str
        """

        # get boxes and features
        boxes = self.boxes
        features = self.values()

        # go through band and mode layers
        for selection in ('bands', 'modes'):

            # get all labels
            labels = [box.labels[index] for box in boxes[selection] for index in box.active]
            features = [feature for feature in features if any([label in feature['name'] for label in labels])]

        # go through parameter labels, choosing those covered by enough selections
        labels = [box.labels[index] for box in boxes['parameters'] for index in box.active]
        features = [feature for feature in features if self._purge(feature['name'].split(':')[0]) in labels]

        # sort features by name
        features.sort(key=lambda feature: feature['name'])
        names = [feature['name'] for feature in features]

        # draw new plots
        news = [self.manifest(self[name]) for name in names]

        # insert new column at bottom
        news = Column(news)
        self.page.children.append(news)

        # pop off old column
        self.page.children.pop(1)

        return None

    def stack(self):
        """Stack all features into a column of graphs.

        Arguments:
            None

        Returns:
            bokeh column object
        """

        # get alll names
        names = self._skim([feature['name'] for feature in self.values()])

        print('length of names, pre filtered: {}'.format(len(names)))

        # get all active features
        for selection in ('bands', 'modes', 'words'):

            # get check boxes
            czechs = [box for box in self.boxes[selection]]
            for box in czechs:

                # print
                print(box.labels, box.active)


            members = [box.labels[index] for box in czechs for index in box.active]

            print('members: {}'.format(len(members)))

            # filter out names
            names = [name for name in names if any([member in name for member in members])]

            print('length of names: {}'.format(len(names)))

        print('length of names: {}'.format(len(names)))

        # get all active features
        features = [feature for feature in self.values() if feature['name'] in names]

        # construct column from rows and graphs
        column = []
        for feature in features:

            # make row and column
            row, graph = self.draw(feature)
            column.append(row)
            column.append(graph)

        # create column
        column = Column(column)

        return column

    def test(self):
        self.test = True
        print(self.test)
        return None

    def toggle(self, feature):
        """Toggle the visible states of a feature, and draw if not yet drawn:

        Arguments:
            feature: dict
            box: bokeh check box
            boxii: bokeh check box

        Returns:
            None
        """

        # grab the feature's manifestation
        manifestation = feature['manifestation']

        # check for less than two children
        if len(manifestation.children) < 2:

            # in which case draw the feature and its checkbox and add
            manifestation.children += self.draw(feature)

        # otherwise
        else:

            # toggle graph
            graph = manifestation.children[1]
            graph.visible = not graph.visible

            # toggle box
            box = manifestation.children[2]
            box.visible = not box.visible

            # toggle off comparison graphs
            if len(box.children) > 1:

                # toggle off
                box.children[1].visible = False
                box.children[0].active = []

        return None

    def unseal(self, envelope):
        """Unseal an envelope of parameter choices:

        Arguments:
            envelope: bokeh column object

        Returns:
            None
        """

        # dial visible state of envelope
        envelope.children[1].visible = not envelope.children[1].visible

        return None

# create flasket and create a column of graphs
boquette = Boquette('boquettes/band_one')
curdoc().add_root(boquette.page)
