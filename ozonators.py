# ozonators.py for the Ozonator class to provide a loose class for exploring OMI data

# import reload
from importlib import reload

# import general tools
import os
import sys
import json
import csv

# import datetime
from datetime import datetime, timedelta
from time import time

# evaluate strings
from ast import literal_eval

# import tools
from pprint import pprint
from collections import Counter
from copy import copy, deepcopy

# import math
import numpy
from numpy.random import random
from math import sin, cos, pi, log10, exp, floor, ceil, sqrt

# import matplotlib for plots
from matplotlib import pyplot
from matplotlib import gridspec
from matplotlib import colorbar
from matplotlib import colors as Colors
from matplotlib import style as Style
from matplotlib import rcParams
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib import path as Path

Style.use('fast')
rcParams['axes.formatter.useoffset'] = False

# import h5py to read h5 files
import h5py

# import fuzzywuzzy for fuzzy matches
from fuzzywuzzy import fuzz

# import counter
from collections import Counter

# import datetime
from datetime import datetime, timedelta

# import random forest and regression
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans, MeanShift
from sklearn.decomposition import PCA

# pdf maker
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter

# cesium, pyreshaper, xarray
import cesium
import pyreshaper
import xarray
xarray.set_options(keep_attrs=True)

# import modules
from prisms import Prism


# class Ozonator to do OMI data mining
class Ozonator(list):
    """Ozonator class to do OMI data mining.

    Inherits from:
        list
    """

    def __init__(self, directory):
        """Initialize an Ozonator instance.

        Arguments:
            None

        Returns:
            None
        """

        # set current time
        self.now = time()

        # set directory
        self.directory = directory

        # set up tree and attributes
        self.tree = {}
        self.numbers = {}
        self.units = {}

        # inventory flags
        self.flags = {}
        self._inventory()

        # ingest the data
        self.shapes = {}
        self.orbits = []
        self._ingest()

        # initalize navigation
        self.navigation = {}

        # make shapes report
        self.subset = []
        self._report()

        # initialize menus options
        self.menu = {}

        # data reservoirs
        self.current = None
        self.kept = None

        return

    def _abbreviate(self, path):
        """Abbreviate the path name.

        Arguments:
            path: str

        Returns:
            str
        """

        # extract stub from OMI file name
        splittings = path.split('-')
        stub = splittings[2]

        return stub

    def _apply(self, filter, features=None):
        """Apply a filter to a list of records.

        Arguments:
            filter: function object
            features=None: list of dicts

        Returns:
            list
        """

        # default features to entire collection
        if not features:

            # set features
            features = [feature for feature in self]

        # apply filter
        survivors = [feature for feature in features if filter(feature)]

        # sort survivors first by product of dimensions, then by number of dimensions
        survivors.sort(key=lambda feature: feature['name'])
        survivors.sort(key=lambda feature: numpy.prod(feature['shape']), reverse=True)
        survivors.sort(key=lambda feature: len(feature['shape']), reverse=True)

        return survivors

    def _arrange(self, collection):
        """Report on the shapes of the features in the dataset.

        Arguments:
            collection: list of dict

        Returns:
            dict

        Populates:
            self.shapes
        """

        # get all shapes
        shapes = self._skim([feature['shape'] for feature in collection])

        # condense shapes together
        condensation = {}
        for shape in shapes:

            # get all entries
            entries = [entry for entry in collection if entry['shape'] == shape]
            entries.sort(key=lambda entry: entry['name'])

            # add to shapes record
            condensation[shape] = entries

        # populate attribute
        self.shapes = condensation

        # sort keys
        shapes = [shape for shape in condensation.keys()]
        shapes.sort(key=lambda shape: max(shape), reverse=True)
        shapes.sort(key=lambda shape: len(shape), reverse=True)

        # make report
        report = ['Shapes:']
        for index, shape in enumerate(shapes):

            # add space
            report.append('')

            # print shape
            report.append('{}) {}:'.format(index, shape))

            # get all names
            entries = condensation[shape]
            names = self._skim([entry['name'] for entry in entries])

            # go through names
            for name in names:

                # print the name
                report.append('{}:'.format(name))

                # go through entries
                for entry in entries:

                    # if the name matches
                    if entry['name'] == name:

                        # print the route
                        route = [self._truncate(step) for step in entry['route']]
                        report.append('      {}'.format(route))

        # jot down report
        self._jot(report, 'reports/shapes.txt')

        return condensation

    def _bin(self, data, instructions):
        """Sort data into bins for plotting a histogram and congealing colors.

        Arguments:
            data: numpy array
            instructions: Instructions instance

        Returns:
            None

        Populates:
            self
        """

        # determine unique values
        data = data.ravel()
        uniques = list(set(data))

        # if fewer than resolution, make a discrete plot
        if len(uniques) < instructions.resolution:

            # each unique value gets it's own bar
            uniques.sort()

            # set width to 1
            width = 1.0

            # get precision from smallest distance between uniques
            distances = [(second - first) for first, second in zip(uniques[:-1], uniques[1:])]

            # calculate precision from smallest distance
            shortest = min(distances or [1])
            precision = -int(log10(shortest))

            # default outlier scale to 0
            scale = (1, 1)

            # middles are simily the half values
            middles = [index + 0.5 for index, _ in enumerate(uniques)]

            # get heights from data
            heights = [len([datum for datum in data if datum == unique]) for unique in uniques]

            # construct bounds, leaving outliers empty
            bounds = [- 1, 0, len(uniques), len(uniques) + 1]

            # labels are the uniques as strings
            labels = [str(round(unique, precision)) for unique in uniques]

        # otherwise consider it a continuous distribution
        else:

            # get initial lower and upper outlier boundaries
            lower = numpy.percentile(data, instructions.margin)
            upper = numpy.percentile(data, 100 - instructions.margin)

            # estimate precision
            if instructions.precision is None:

                # determine preliminary width from data within margins
                width = (upper - lower) / (instructions.resolution - 2)
                instructions.precision = -int(log10(width))

            # round boundaries to precision
            lower = self._round(lower, instructions.precision, up=False)
            upper = self._round(upper, instructions.precision, up=True)

            # determine bin width
            instructions.width = (upper - lower) / (instructions.resolution - 2)

            # construct main bounds
            instructions.bounds = [lower + instructions.width * index for index in range(instructions.resolution - 1)]
            instructions.ticks = instructions.bounds
            instructions.labels = [str(tick) for tick in instructions.ticks]

            # get lower outlier bins
            lowers = []
            minimum = self._round(min(data), instructions.precision, up=False)
            width = (lower - minimum) / (instructions.resolution - 2)
            if width > 0:

                # calculate lower bounds
                lowers = [minimum + index * width for index in range(instructions.resolution - 1)]

            # get upper outlier bins
            uppers = []
            maximum = self._round(max(data), instructions.precision, up=True)
            width = (maximum - upper) / (instructions.resolution - 2)
            if width > 0:

                # calculate upper bounds
                uppers = [upper + index * width for index in range(instructions.resolution - 1)]

            # set attributes
            instructions.lowers = lowers
            instructions.uppers = uppers

            # determine ratios for each color
            bounds = instructions.lowers + instructions.bounds + instructions.uppers
            bounds = list(set(bounds))
            instructions.ratios = [float(index / len(bounds[:-2])) for index, _ in enumerate(bounds[:-1])]



            # # make bins from below lower to above upper
            # bins = []
            # start = round(lower, precision) - width
            # for _ in range(resolution):
            #
            #     # add bins
            #     finish = start + width
            #     bins.append((start, finish))
            #     start = finish
            #
            # # add bounds information
            # bounds = [bins[0][0], bins[-1][1]]
            #
            # # get bins for lower outliers
            # outliers = []
            # lowers = data[data < bounds[0]]
            # for entry in lowers:
            #
            #     # determine bin and add
            #     difference = bounds[0] - entry
            #     if difference > 0 and width > 0:
            #
            #         # but skip if no difference
            #         chunks = floor(difference / width)
            #         outliers.append((bounds[0] - (chunks + 1) * width, bounds[0] - chunks * width))
            #
            # # add to bounds and bins
            # outliers = list(set(outliers))
            # minimum = min([bin[0] for bin in outliers] or [bounds[0] - 1])
            # bounds = [minimum] + bounds
            # bins += outliers
            # first = len(outliers)
            #
            # # get bins for upper outliers
            # outliers = []
            # uppers = data[data > bounds[2]]
            # for entry in uppers:
            #
            #     # determine bin and add
            #     difference = entry - bounds[2]
            #     if difference > 0 and width > 0:
            #
            #         # determine bin and add
            #         chunks = ceil(difference / width)
            #         outliers.append((bounds[2] + (chunks - 1) * width, bounds[2] + chunks * width))
            #
            # # add to bounds and bins
            # outliers = list(set(outliers))
            # maximum = max([bin[1] for bin in outliers] or [bounds[-1] + 1])
            # bounds = bounds + [maximum]
            # bins += outliers
            # last = len(outliers)
            #
            # # sort bins
            # bins.sort(key=lambda bin: bin[0])
            #
            # # create midpoints
            # middles = [(bin[0] + bin[1]) / 2 for bin in bins]
            #
            # # create bar heights from bins
            # heights = [Counter((data >= bin[0]) & (data <= bin[1]))[True] for bin in bins]
            #
            # # determine outlier scale
            # scale = (max(heights[:first] or [1]), max(heights[-last:] or [1]))
            #
            # # make into numpy arraya
            # middles = numpy.array(middles)
            # heights = numpy.array(heights)
            #
            # # labels are empty
            # labels = []

        return instructions

    def _bite(self, tensor, bit):
        """Recast a tensor as a mask for whether a bit is on or off.

        Arguments:
            tensor: numpy array
            bit: int

        Returns:
            numpy array
        """

        # convert to int
        bit = int(bit)

        # get all unique values
        integers = numpy.array(tensor).astype(int)
        zeros = numpy.zeros(numpy.max(integers) + 1, dtype=int)
        zeros[integers.ravel()] = 1
        uniques = numpy.nonzero(zeros)[0]

        # decompose all uniques
        codes = []
        for unique in uniques:

            # decompose into binary form
            decomposition = bin(unique).replace('0b', '0' * 20)
            decomposition = decomposition[::-1]
            if decomposition[bit] == '1':

                # add to codes
                codes.append(unique)

        # create boolean masks and convert to ints
        masks = [tensor == code for code in codes]
        masks = [mask.astype('int') for mask in masks]

        # add empty mask
        empty = xarray.zeros_like(tensor)
        masks.append(empty)

        # concatenate and sum
        mask = xarray.concat(masks, dim='faux')
        mask = xarray.DataArray.sum(mask, dim='faux')

        return mask

    def _build(self, paths):
        """Build the data tree from file paths.

        Arguments:
            paths: list of str, the file paths

        Returns:
            dict
        """

        # begin tree
        tree = {}

        # go through each path
        for path in paths:

            # get the data at the path
            data = self._fetch('{}/{}'.format(self.directory, path))

            # split into orbit, filename, version
            pieces = path.split('_')
            version = pieces[3].split('-')[0]
            orbit = pieces[2]
            name = pieces[1]

            # check for orbit in tree
            designation = '_'.join([orbit, version])
            if designation not in tree.keys():

                # add orbit
                tree[designation] = {}

            # insert data into tree
            tree[designation][name] = data

        return tree

    def _cameo(self, name):
        """Make a name into camel case.

        Arguments:
            name: str

        Returns:
            str
        """

        # split on colons
        camels = []
        sections = name.split(':')
        for section in sections:

            # split by underscores
            words = section.split('_')

            # capitalize each part
            words = [word.capitalize() for word in words]

            # rejoin
            camel = ''.join(words)
            camels.append(camel)

        # connect main name with underscore
        cameo = '_'.join(camels)

        return cameo

    def _combine(self, collection):
        """Combine multiple collections of data.

        Arguments:
            collection: tuple of lists

        Returns:
            None
        """

        # get all routes except for final
        routes = [str(entry[0][:-1]) for entry in collection]
        routes = list(set(routes))

        # check each route
        combinations = []
        for route in routes:

            # collect matrices
            matrices = []
            for branch, matrix in collection:

                # check route
                if str(branch[:-1]) == route:

                    # add matrix
                    matrices.append(matrix.reshape(-1, 1))

            # concatenate all matrices
            concatenation = numpy.concatenate(matrices, axis=1)

            # add to collection
            combinations.append((literal_eval(route), concatenation))

        return combination

    def _coordinate(self, latitude, longitude, altitude=0.0):
        """Determine plotting coordinates for a latitude, longitude, altitude triplet.

        Arguments:
            latitude: float, latitude in degrees
            longitude: float, longitude in degrees
            altitude: float, altitude in km

        Returns:
            tuple of floats
        """

        # determine radius from altitude and earth radius
        earth = 6371
        radius = (earth + altitude) / earth

        # calculate x, y, and z
        x = radius * sin(longitude * pi / 180) * cos(latitude * pi / 180)
        y = radius * cos(longitude * pi / 180) * cos(latitude * pi / 180)
        z = radius * sin(latitude * pi / 180)

        return x, y, z

    def _define(self, collection):
        """Define the units involved.

        Arguments:
            collection: list of dicts

        Return:
            None

        Populates:
            self.units
            self.numbers
        """

        # get all measurement fields
        measurements = self._skim([feature['name'] for feature in collection])

        # define units
        self.units.update({field: 'mol photons/ s nm m^2 st' for field in measurements if 'radiance' in field.lower()})
        self.units.update({field: 'photons/ s nm cm^2 st (?)' for field in measurements if 'straylight' in field.lower()})
        self.units.update({field: 'mol photons/ s nm m^2' for field in measurements if 'irradiance' in field.lower()})
        self.units.update({field: 'flag' for field in measurements if 'quality' in field.lower()})
        self.units.update({field: 'flag' for field in measurements if 'flag' in field.lower()})
        self.units.update({field: 'degrees' for field in measurements if 'latitude' in field.lower()})
        self.units.update({field: 'degrees' for field in measurements if 'longitude' in field.lower()})
        self.units.update({field: 'electrons' for field in measurements if 'signal' in field.lower()})
        self.units.update({field: 'electrons' for field in measurements if 'readout' in field.lower()})
        self.units.update({field: 'volts' for field in measurements if 'offset' in field.lower()})
        self.units.update({field: 'degrees K' for field in measurements if 'temp' in field.lower()})
        self.units.update({field: 'meters' for field in measurements if 'height' in field.lower()})
        self.units.update({field: 'meters' for field in measurements if 'altitude' in field.lower()})
        self.units.update({field: 'pixels' for field in measurements if 'wavelength_shift' in field.lower()})
        self.units.update({field: 'nanometers' for field in measurements if 'wavelength' in field.lower()})
        self.units.update({field: 'seconds' for field in measurements if 'time' in field.lower()})

        # update with bit fraction values
        for name, group in self.flags.items():

            # add descriptions to units
            self.units.update({'_'.join([str(bit), description]): 'bit fraction' for bit, description in group.items()})

        # define numbers
        self.numbers.update({number: 'track' for number in (1494, 800, 400, 294, 1644, 1643, 1493)})
        self.numbers.update({number: 'xtrack' for number in (65, 60, 30)})
        self.numbers.update({number: 'spectral' for number in (792, 751, 557, 159)})
        self.numbers.update({number: 'frame_pixel' for number in (581,)})
        self.numbers.update({number: 'readout_pixel' for number in (814,)})
        self.numbers.update({number: 'data' for number in (1,)})

        return None

    def _determine(self, targets, predictions):
        """Determine the coefficient of determination between targets and predictions.

        Arguments:
            None

        Return:
            None
        """

        # calculate the mean squared error
        error = sum([(target - prediction) ** 2 for target, prediction in zip(targets, predictions)]) / len(targets)

        # calculate the variance
        mean = numpy.average(targets)
        variance = sum([(target - mean) ** 2 for target in targets]) / (len(targets) - 1)

        # calculate determination
        determination = 1 - (error / variance)

        return determination

    def _dig(self, word, negate=False, partial=False):
        """Make a search function object to check for a particular route member.

        Arguments:
            word: str
            negate=False: boolean, negate the search?
            partial=False: boolean, searching for a partial

        Returns:
            function object
        """

        # get list of all route fields
        fields = list(set([member for feature in self for ingot in feature['route'] for member in ingot]))

        # use fuzzy matching to get exact field name
        field = self._pick(word, fields)

        # make function object
        filter = lambda record: field in record['route']

        # or negate function
        if negate:

            # make negative function
            filter = lambda record: field not in record['route']

        # or a partial function
        if partial:

            # make parial function
            filter = lambda record: word in record['name']

        return filter

    def _distinguish(self, features, index):
        """Mark each feature with a distinguishing name based on a starting point in the route.

        Arguments:
            features: list of dicts
            index: int, starting index of routes

        Returns:
            list of dicts
        """

        # get number of features
        number = len(features)

        # group features by name
        lumps = self._lump(features, 'name')

        # add route names to feature names until all are unique
        while len(lumps) < number:

            # get all names with multiple entries
            for name, members in lumps.items():

                # check for mulitple entries
                if len(members) > 1:

                    # add route step
                    for feature in members:

                        # add next route step to feature
                        feature['name'] += ':{}'.format(feature['route'][index])

            # recollect all features
            features = [feature for members in lumps.values() for feature in members]

            # make new lumps
            lumps = self._lump(features, 'name')

            # advance index
            index += 1

        return features

    def _dump(self, contents, destination):
        """Dump a dictionary into a json file.

        Arguments:
            contents: dict
            destination: str, destination file path

        Returns:
            None
        """

        # dump file
        with open(destination, 'w') as pointer:

            # dump contents
            json.dump(contents, pointer)

        return None

    def _fetch(self, path):
        """Fetch the contents from an h5 file.

        Arguments:
            path: str, file path

        Returns:
            H5 file
        """

        # load the file
        contents = h5py.File(path, 'r')

        return contents

    def _format(self, number):
        """Choose formatting for a number.

        Arguments:
            number: float or int

        Returns:
            str
        """

        # set string by default
        string = str(number)

        # try to reformat
        try:

            # check for int
            if number == int(number):

                # check for high number
                if log10(number) > 5:

                    # use exponential
                    string = '%.3e' % number

                # otherwise
                else:

                    # use int
                    string = str(int(number))

            # otherwise assume float
            else:

                # round to 3 digits past its size
                size = int(log10(abs(number)))
                precision = -size + 2
                number = round(number, precision)

                # use float notation for low precision
                if precision <= 5:

                    # make string from rounded number
                    #format = '%.f{}'.format(precision)
                    string = str(number)

                # otherwise
                else:

                    # use exponential
                    string = '%.3e' % number

        # otherwise
        except (OverflowError, ValueError):

            # skip
            pass

        return string

    def _gather(self, data, route=None, collection=None):
        """Gather all routes and shapes from a datafile.

        Arguments:
            data: dict or h5
            route=None: current route
            collection=None: current collection

        Returns:
            list of dicts
        """

        # initialze routes for first round
        route = route or []
        collection = collection or []

        # try to
        try:

            # get all fields
            for field in data.keys():

                # and add each field to the collection
                collection += self._gather(data[field], route + [field])

        # unless it is an endpoint
        except AttributeError:

            # try to
            try:

                # determine shape and type
                shape = data.shape
                type = data.dtype

                # if the type is simple
                if len(type) < 1:

                    # add entry to collection
                    entry = {'shape': shape, 'name': route[-1], 'route': route, 'type': type.name, 'dimensions': len(shape)}
                    collection.append(entry)

                # otherwise the type is complex
                else:

                    # convert to numpy dtype
                    conversion = numpy.dtype(type)

                    # for each member of the type
                    for name in conversion.names:

                        # add an entry to the collection
                        format = str(conversion.fields[name][0])
                        entry = {'shape': shape, 'name': name, 'route': route + [name], 'type': format, 'dimensions': len(shape)}
                        collection.append(entry)

            # otherwise assume a named type
            except AttributeError:

                # and disregard for now
                pass

        return collection

    def _grab(self, route, *indices):
        """Grab the data by following the record's route.

        Arguments:
            route: list of lists of str
            *indices: unpacked list of ints

        Returns:
            numpy array
        """

        # get the data source at the route
        source = self._walk(route + list(indices))

        # create a tensor from the contents
        tensor = xarray.DataArray(source[:])

        # go through all indices
        for index in indices:

            # unpack
            tensor = tensor[index]

        return tensor

    def _graft(self, route, tensor, address, *indices):
        """Graft the data at the route onto the tensor at the given address.

        Arguments:
            route: list of lists of str
            tensor: numpy array, xarray
            address: tuple of ints
            *indices: unpacked list of ints

        Returns:
            None
        """

        # get the data source at the route
        tensor[address] = self._walk(route, self.tree)

        # # create a tensor from the contents
        # #tensor = numpy.array(source[:])
        # tensor[address] = source[:]

        # # assume skipping first index if it is one
        # if len(tensor.shape) > 1 and len(tensor) == 1:
        #
        #     # step down
        #     tensor = tensor[0]
        #
        # # go through all indices
        # for index in indices:
        #
        #     # unpack
        #     tensor = tensor[index]

        return None

    def _group(self, features, function):
        """Group a set of features by the result of a function of the feature.

        Arguments:
            features: list of dicts
            function: str

        Return dict
        """

        # get all fields
        fields = self._skim([function(feature) for feature in features])

        # create groups
        groups = {field: [] for field in fields}
        [groups[function(feature)].append(feature) for feature in features]

        return groups

    def _halve(self, feature):
        """Cut the feature in half along first grouping.

        Arguments:
            feature: dict

        Returns:
            list of dicts
        """

        # make copies of feature
        first = deepcopy(feature)
        second = deepcopy(feature)

        # get first step of route
        route = feature['route']
        half = int(len(route[0]) / 2)

        # reset routes and shapes
        first['route'][0] = route[0][:half]
        first['shape'] = (len(first['route'][0]),) + feature['shape'][1:]
        second['route'][0] = route[0][half:]
        second['shape'] = (len(second['route'][0]),) + feature['shape'][1:]

        # make halves
        halves = [first, second]

        return halves

    def _ingest(self):
        """Ingest all data fields for shape information.

        Arguments:
            None

        Returns:
            dict

        Populates:
            self.tree
            self
            self.measurements
        """

        # build data tree
        self._stamp('building tree...')
        paths = os.listdir(self.directory)
        tree = self._build(paths)
        self.tree = tree

        # gather features and define units
        self._stamp('gathering features...')
        collection = self._gather(tree)
        self._define(collection)

        # arrange features by shape
        self._stamp('arranging shapes...')
        shapes = self._arrange(collection)

        # stack together feaures into larger shapes
        self._stamp('stacking shapes...')
        features = self._stack(shapes)

        # populate with features
        self._stamp('populating...')
        self._populate(features)

        # timestamp
        self._stamp('populated.')

        return None

    def _insert(self, feature, subset, queue):
        """Insert a feauture into the subset at the queue number.

        Arguments:
            feature: dict
            subset: list of dicts
            queue: int

        Returns:
            None
        """

        # insert member
        subset = subset[:queue] + [feature] + subset[queue:]

        return subset

    def _inventory(self):
        """Take inventory of quality flags.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.flags
        """

        # begin flags
        flags = {}

        # get the contents of the flags file
        text = self._transcribe('reports/flags.txt')

        # join with colons and separate by blank lines
        text = ':'.join(text)
        groups = text.split('::')

        # collect groups
        for group in groups:

            # split by colons to get members
            lines = group.split(':')
            name = lines[0]
            members = lines[1:]

            # update flag codes with descriptions, retaining the first member as group name
            codes = {int(bit): description for bit, description in [member.split(' ') for member in members]}
            flags[name] = codes

        # set flags
        self.flags = flags

        return None

    def _jot(self, lines, destination):
        """Jot down the lines into a text file.

        Arguments:
            lines: list of str
            destination: str, file path

        Returns:
            None
        """

        # add final endline
        lines = [line + '\n' for line in lines]

        # save lines to file
        with open(destination, 'w') as pointer:

            # write lines
            pointer.writelines(lines)

        return None

    def _load(self, path):
        """Load a json file.

        Arguments:
            path: str, file path

        Returns:
            dict
        """

        # open json file
        with open(path, 'r') as pointer:

            # get contents
            contents = json.load(pointer)

        return contents

    def _lump(self, features, field):
        """Lump features together based on a field.

        Arguments:
            None

        Returns:
            None
        """

        # group based on field
        lumps = {}
        for feature in features:

            # check for entry and add to lumps
            name = feature[field]
            lumps.setdefault(name, []).append(feature)

        return lumps

    def _melt(self, routes, slag=None):
        """Melt several routes into one coherent structure.

        Arguments:
            routes: list of lists
            tree: dict

        Returns:
            dict
        """

        # get default slag
        if slag is None:

            # set to empty list
            slag = []

        # try to
        try:

            # collect all first level routes
            ingots = list(set([route[0] for route in routes]))

            # sort alphabetically or by mode if appropriate
            fields = [field for route in routes for field in route]
            fields.sort()
            ingots.sort(key=lambda field: fields.index(field))

            # get new routes
            routes = [route[1:] for route in routes]
            slag += self._melt(routes, [ingots])

        # unless end of the line
        except IndexError:

            # end slag
            pass

        return slag

    def _merge(self, feature):
        """Merge several related measurements together.

        Arguments:
            queue: int, queue number

        Returns:
            dict
        """

        # get feature and unpack
        shape = feature['shape']
        route = feature['route']
        type = feature['type']

        # collect all features that meet the criteria
        stack = [feature for feature in self.subset if feature['shape'] == shape]
        stack = [feature for feature in stack if feature['type'] == type]
        stack = [feature for feature in stack if feature['route'][:-1] == route[:-1]]

        # modify index to -1
        feature['index'] = -1

        # create name from all names
        names = [feature['route'][-1][0] for feature in stack]
        feature['name'] = ','.join(names)

        # add step to route
        feature['route'][-1] = names

        # extend shape and signifiers
        feature['shape'] += (len(names),)
        feature['signifiers'] += ['process']

        return feature

    def _normalize(self, matrix):
        """Normalize each row of a matrix using stdev and mean.

        Arguments:
            matrix: numpy array

        Returns:
            numpy array
        """

        # make matrix of means
        means = numpy.array([numpy.average(matrix, axis=1)] * len(matrix[0]))
        means = numpy.swapaxes(means, 0, 1)
        means = numpy.nan_to_num(means)

        # make matrix of standard deviations
        deviations = numpy.array([numpy.std(matrix, axis=1)] * len(matrix[0]))
        deviations = numpy.swapaxes(deviations, 0, 1)
        deviations = numpy.nan_to_num(deviations)

        # subtract mean and divide by deviation
        normalization = numpy.divide(numpy.subtract(matrix, means), deviations)

        return normalization

    def _numerate(self, *dimensions):
        """Numerate all combinations of indices according to given dimensions.

        Arguments:
            *dimensions: unpacked list of int

        Returns:
            list of tuples
        """

        # begin addresses
        addresses = [[]]
        for dimension in dimensions:

            # go through each range
            currents = []
            for number in range(dimension):

                # add number to each
                currents += [address + [number] for address in addresses]

            # reset
            addresses = currents

        return addresses

    def _offer(self, place, prism):
        """Offer up input and parse commands during navigation.

        Arguments:
            place: int
            prism: Prism instance

        Returns:
            dict
        """

        # begin with input
        command = input('>>?')

        # parse into keyword and options
        words = command.split() or ['']
        action = words[0]
        options = words[1:]

        # move to next feature
        trigger = ('', 'next')
        self.menu[trigger] = 'move to next feature'
        if action in trigger:

            # increase the queue number
            place += 1

        # move to previous view
        trigger = ('-', 'back')
        self.menu[trigger] = 'move to previous feature'
        if action in trigger:

            # pop off last view of the prism
            discard = prism.pop()

        # move up through distal schemes
        trigger = ('+',)
        self.menu[trigger] = 'move to next scheme'
        if action in trigger:

            # advance scheme
            prism._advance(*options)

        # get the bit codes
        trigger = ('bite', 'byte', 'bits')
        self.menu[trigger] = 'get the bit decomposition of a bit code'
        if action in trigger:

            # decompose the bits
            code = int(options[0])
            decomposition = prism._decompose(code)
            print('bit positions from {}: {}'.format(code, decomposition))

        # compare the current data with the kept data
        trigger = ('compare',)
        self.menu[trigger] = 'compare current dataset against kept data'
        if action in trigger:

            # create faux feature as subtraction of two datasets
            faux = self.compare(self.current, self.kept)
            prism.destination = 'plots/comparison.png'
            self.glance(faux, prism)

        # attempt to take the stddev of a scheme
        trigger = ('deviate', 'stdev')
        self.menu[trigger] = 'change method of reduction to standard deviation'
        if action in trigger:

            # make a minimized view
            prism._deviate()

        # reanalyze in terms of percent one component
        trigger = ('diffract',)
        self.menu[trigger] = 'reanalyze as percent of a particular quality code'
        if action in trigger:

            # get the code
            code = int(options[0])

            # perform knapping
            prism._diffract(code)

        # divide by the kept matrix
        trigger = ('divide',)
        self.menu[trigger] = 'divide current data by the kept matrix'
        if action in trigger:

            # perform division
            prism._divide(self.kept)

        # keep the plot for comparison
        trigger = ('engrave', )
        self.menu[trigger] = 'save the sketch'
        if action in trigger:

            # set default destination
            destination = 'plots/engrave.png'

            # get destination
            if len(options) > 0:

                # set destination
                destination = options[0]

            # add resave from glance
            image = Image.open('plots/sketch.png')
            image.save(destination)

        # add a custom title
        trigger = ('entitle', )
        self.menu[trigger] = 'add a custom title'
        if action in trigger:

            # set default destination
            entitlement = ' '.join(options)

            # add resave from glance
            prism._entitle(entitlement)

        # exit the console
        trigger = ('exit', 'xxx')
        self.menu[trigger] = 'exit the console'
        if action in trigger:

            # set queue to -1
            place = -1

        # go to a vertical row
        trigger = ('fly',)
        self.menu[trigger] = 'go to a particular vertical entry'
        if action in trigger:

            # get index
            index = int(options[0])

            # go to a scheme
            prism._fly(index, prism[-1].scheme)

        # print out menu choices
        trigger = ('forget',)
        self.menu[trigger] = 'forget the last manipulation applied'
        if action in trigger:

            # forget last manipulation
            prism._forget()

        # reset to full size
        trigger = ('full',)
        self.menu[trigger] = 'return to full size'
        if action in trigger:

            # reset to default scheme
            prism._polish()

        # change color scheme
        trigger = ('glaze',)
        self.menu[trigger] = 'choose a color scheme'
        if action in trigger:

            # get spectrum
            spectrum = options[0]

            # glaze
            prism._glaze(spectrum)

        # go to a vertical row
        trigger = ('go',)
        self.menu[trigger] = 'go to a particular vertical entry'
        if action in trigger:

            # get index
            index = int(options[0])

            # go to a scheme
            prism._go(index, prism[-1].scheme)

        # print out menu choices
        trigger = ('help',)
        self.menu[trigger] = 'this'
        if action in trigger:

            # print out menu
            print('\noptions:')
            triggers = [trigger for trigger in self.menu.keys()]
            triggers.sort()
            for trigger in triggers:

                # print
                print('{}: {}'.format(trigger, self.menu[trigger]))

            # print spacer
            print('')

        # jump to a particular data type
        trigger = ('jump',)
        self.menu[trigger] = 'jump to a particular feature by name or queue number'
        if action in trigger:

            # split command
            jump = options[0]

            # check for digits
            if jump.isdigit():

                # go to that feature index
                place = int(jump)

            # otherwise go by data field
            else:

                # find closest match
                features = [feature['name'] for feature in self.subset]
                pick = self._pick(jump, features)
                place = [index for index, feature in enumerate(self.subset) if feature['name'] == pick][0]

        # keep the plot for comparison
        trigger = ('keep',)
        self.menu[trigger] = 'keep a dataset for later comparisons'
        if action in trigger:

            # set default destination
            destination = 'plots/keep.png'

            # get destination
            if len(options) > 0:

                # set destination
                destination = options[0]

            # add resave from glance
            image = Image.open('plots/glance.png')
            image.save(destination)

            # save matrix
            self.kept = prism[-1].matrix

        # adjust the margine
        trigger = ('margin',)
        self.menu[trigger] = 'adjust the marginalized percentile'
        if action in trigger:

            # split command
            margin = options[0]

            # add margin to prism
            prism.margin = int(margin)

        # attempt to average scheme
        trigger = ('max', 'maximum')
        self.menu[trigger] = 'change method of reduction to maximum'
        if action in trigger:

            # make a minimized view
            prism._maximize()

        # attempt to average scheme
        trigger = ('mean', 'average')
        self.menu[trigger] = 'change method of reduction to average'
        if action in trigger:

            # make a minimized view
            prism._average()

        # attempt to average scheme
        trigger = ('median',)
        self.menu[trigger] = 'change method of reduction to median'
        if action in trigger:

            # make a minimized view
            prism._mediate()

        # meld together into a smaller set
        trigger = ('meld',)
        self.menu[trigger] = 'reduce data by aggregation'
        if action in trigger:

            # get the code
            number, method = options[0], options[1]

            # perform knapping
            prism._meld(number, method)

        # melt together using running averages
        trigger = ('melt',)
        self.menu[trigger] = 'reanalyze by taking the running average/ min/ max'
        if action in trigger:

            # get the code
            number, method = options[0], options[1]

            # perform knapping
            prism._melt(number, method)

        # merge several measurements together
        trigger = ('merge',)
        self.menu[trigger] = 'merge several related measurements together'
        if action in trigger:

            # create new feature
            feature = self._merge(self.subset[queue])

            # advance queue
            queue += 1

            # insert into queue
            self.subset = self._insert(feature, self.subset, queue)

            # create a prism
            prism = self.etch(feature)

        # attempt to average scheme
        trigger = ('min', 'minimum')
        self.menu[trigger] = 'change method of reduction to minimum'
        if action in trigger:

            # make a minimized view
            prism._minimize()

        # adjust precision
        trigger = ('precision',)
        self.menu[trigger] = 'adjust the precision'
        if action in trigger:

            # change resolution
            precision = options[0]
            prism.precision = int(precision)

        # reanalyze in terms of percent one bit component
        trigger = ('refract',)
        self.menu[trigger] = 'reanalyze as percent of a particular quality code'
        if action in trigger:

            # get the code
            bit = int(options[0])

            # perform knapping
            prism._refract(bit)

        # adjust resolution
        trigger = ('resolution',)
        self.menu[trigger] = 'adjust the resolution'
        if action in trigger:

            # change resolution
            resolution = options[0]
            prism.resolution = int(resolution)

        # sketch as a line plot
        trigger = ('sketch',)
        self.menu[trigger] = 'sketch data as a line plot'
        if action in trigger:

            # get start index
            start = int(options[0])

            # get finish index
            finish = start + 1
            if len(options) > 1:

                # get finish
                finish = int(options[1]) + 1

            # sketch as a line plot
            prism[-1].sketch(start, finish)

        # make a slide
        trigger = ('slide',)
        self.menu[trigger] = 'make a slide from the scatter plot'
        if action in trigger:

            # get options
            name = options[0]
            destination = 'slides/raw/{}.png'.format(name)

            # send graph to slide
            prism[-1].glance(slide=True, destination=destination)

        # keep the plot for comparison
        trigger = ('stash', 'save')
        self.menu[trigger] = 'save the graph'
        if action in trigger:

            # set default destination
            destination = 'plots/save.png'

            # get destination
            if len(options) > 0:

                # set destination
                destination = options[0]

            # add resave from glance
            image = Image.open('plots/glance.png')
            image.save(destination)

        # divide by the kept matrix
        trigger = ('subtract',)
        self.menu[trigger] = 'subtract current data by the kept matrix'
        if action in trigger:

            # perform division
            prism._subtract(self.kept)

        # exit the console
        trigger = ('exit', 'xxx')
        self.menu[trigger] = 'exit the console'
        if action in trigger:

            # set queue to -1
            place = -1

        # tilt into other dimension
        trigger = ('tilt',)
        self.menu[trigger] = 'tilt the vertical axis along the schemes'
        if action in trigger:

            # tilt
            prism._tilt()

        # twist axes
        trigger = ('twist',)
        self.menu[trigger] = 'twist axes'
        if action in trigger:

            # twist axes
            prism._twist()

        # add a custom title
        trigger = ('unify', )
        self.menu[trigger] = 'add custom units'
        if action in trigger:

            # set default destination
            unity = ' '.join(options)

            # add resave from glance
            prism._unify(unity)

        # zoom into a subset
        trigger = ('zoom',)
        self.menu[trigger] = 'zoom into the dataset'
        if action in trigger:

            # split command
            top, bottom = options

            # zoom
            prism._zoom(int(top), int(bottom))

        return place

    def _overlap(self, route, routeii):
        """Overlap two routes to get all combinations.

        Arguments:
            route: list
            routeii: list

        Returns:
            list of lists
        """

        # find overlaps
        overlaps = [[]]
        for step, stepii in zip(route, routeii):

            # add steps to each member
            news = [member + [field] for member in overlaps for field in (step, stepii)]
            overlaps = news

        # convert to tuples to remove duplicates
        conversions = self._skim([tuple(overlap) for overlap in overlaps])
        overlaps = [list(member) for member in conversions]

        return overlaps

    def _pad(self, bit, bits):
        """Pad a bit with zeros based on largest bit in the collection.

        Arguments:
            bit: int
            bits: dict

        Returns:
            str
        """

        # get maximum bit length
        maximum = max([number for number in bits.keys()])
        length = len(str(maximum))

        # pad out bit
        pad = '0' * length + str(bit)
        pad = pad[-length:]

        return pad

    def _pick(self, offer, options):
        """Pick the closest match to an offer from a list of options.

        Arguments:
            pffer: str
            options: list of str (or generator?)

        Returns:
            str
        """

        # check for direct present
        best = offer
        if best not in options:

            # calculate fuzzy matchies
            fuzzies = [(option, fuzz.ratio(offer.lower(), option.lower())) for option in options]
            fuzzies.sort(key=lambda score: score[1], reverse=True)

            # find the best match
            best = fuzzies[0][0]

        return best

    def _populate(self, features):
        """Populate the instance with feature records.

        Arguments:
            features: list of dicts

        Returns:
            None

        Populates:
            self
        """

        # depopulate the instances
        while len(self) > 0:

            # pop off
            disposal = self.pop()

        # populate with new instances
        for index, feature in enumerate(features):

            # populate
            feature['index'] = index
            self.append(feature)

        return None

    def _query(self, record, field, answer):
        """Query a record for data in a field, or send default value.

        Arguments:
            record: dict
            field: str
            answer: variable type, default answer

        Returns:
            variable type
        """

        # search for field
        if field in record.keys():

            # answer is the data
            answer = record[field]

        return answer

    def _report(self):
        """Report on the shapes of the features in the dataset.

        Arguments:
            None

        Returns:
            None
        """

        # check for subset
        subset = self.subset
        if len(subset) < 1:

            # subset is entirety
            subset = [feature for feature in self]

        # set current shape
        current = (0,)

        # construct report
        report = ['Features by shape (orbit, band, mode, measurement, spatial pixel, spectral pixel)']
        for feature in subset:

            # get shape
            shape = feature['shape']

            # check for new shape
            if shape != current:

                # print spacer
                report += ['']

                # print shape
                report += ['{}:'.format(str(shape))]

                # print signifiers
                report += ['{}'.format(feature['labels'])]

                # reset current
                current = shape

            # add row for feature
            report += ['{}) {}:'.format(feature['index'], feature['name'])]

            # go through each step of the route
            for level in feature['route'][:-1]:

                # condense each step
                condensing = lambda word: word[:12] + '...' + word[-12:] if len(word) > 25 else word
                condensation = [condensing(entry) for entry in level]

                # add to report
                report += ['     {}'.format(condensation)]

            # print type
            report += ['           type: {}'.format(feature['type'])]

        # add spacer
        report += ['']

        # save report to text file
        self._jot(report, 'reports/survey.txt')

        return None

    def _round(self, number, precision, up=False):
        """Round the number to a particular precision.

        Arguments:
            number: float or int
            precision: int
            up=False: round up?

        Returns:
            float or int
        """

        # round the number
        rounded = round(number, precision)

        # round it up
        if up:

            # round it up
            rounded = rounded + 10 ** -precision * int(rounded < number)

        # or round down
        if not up:

            # round it down
            rounded = rounded - 10 ** -precision * int(rounded > number)

        return rounded

    def _schedule(self):
        """Determine the sequence of observations.

        Arguments:
            None

        Returns:
            tuple of datetime objects
        """

        # begin list of events
        events = []

        # get all time delta features
        features = self._apply(lambda record: record['name'] == 'delta_time')
        for feature in features:

            # find mode
            mode = ([member for member in feature['route'] if 'MODE' in member] + [''])[0]

            # find band
            band = ([member for member in feature['route'] if 'BAND' in member] + [''])[0]

            # find detector
            detector = ([member for member in feature['route'] if 'DETECTOR' in member] + [''])[0]

            # get all time deltas
            deltas = self._grab(feature).ravel().tolist()

            # add events
            [events.append({'delta': delta, 'mode': mode, 'band': band, 'detector': detector}) for delta in deltas]

        # organize events
        events.sort(key=lambda event: event['detector'])
        events.sort(key=lambda event: event['band'])
        events.sort(key=lambda event: event['mode'])
        events.sort(key=lambda event: event['delta'])

        # get start and final times
        start, final = self.synchronize()

        # begin report with heading
        report = ['OMI Observation Schedule from {} to {}'.format(start, final)]
        report.append('')

        # add a line for each event
        modes = []
        for index, event in enumerate(events):

            # calculate delta in minutes
            minutes = (event['delta'] - events[0]['delta']) / 60000
            minutes = round(minutes, 4)

            # unpact event
            mode = event['mode']
            detector = event['detector']
            band = event['band']

            # create line and add to report
            line = '{}) {}: {}, {}, {}'.format(index, minutes, mode, detector, band)
            report.append(line)

            # add mode to modes if new
            if mode not in modes:

                # add to mode
                modes.append(mode)

        # save report
        self.schedule = report
        self.modes = modes

        # save file
        self._jot(report, 'reports/schedule.txt')

        return None

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # make paths
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        return paths

    def _sift(self, number):
        """Create a function object to filter for a particular dimension size.

        Arguments:
            number: int

        Returns:
            function object
        """

        # create function object
        filter = lambda record: number in record['shape']

        return filter

    def _signify(self, route, shape):
        """Determine labels for a route and section shape.

        Arguments:
            route: list of list of str
            shape: tuple of ints

        Returns:
            list of str
        """

        # accumulate labels for each dimension
        labels = ['orbit', 'file']
        for step in route[2:]:

            # check for typical names
            found = False
            for label in ('detector', 'band', 'mode'):

                # in all members
                if all([label in member.lower() for member in step]):

                    # add to label
                    labels.append(label)
                    found = True

            # otherwise
            if not found:

                # check for single entry
                if len(step) == 1:

                    # and lowercase it
                    label = step[0].lower()
                    labels.append(label)
                    found = True

            # otherwise
            if not found:

                # stitch all members together
                label = '_'.join(step).lower()
                labels.append(label)

        # try to get other labels from the number
        for number in shape:

            # query for the label
            label = self._query(self.numbers, number, 'measurement')
            labels.append(label)

        return labels

    def _siphon(self, feature):
        """Siphon data into the prism from the data tree.

        Arguments:
            feature: dict

        Returns:
            numpy array
        """

        # get the route and current layer
        route = feature['route']

        # assemble individual data branches from route
        branches = [[]]
        for knot in route:

            # make sure to include all knots
            stack = []
            for step in knot:

                # append to each branch in the reservoir
                for branch in branches:

                    # add each combination
                    stack.append(branch + [step])

            # reset branches
            branches = stack

        # allocate space for full tensor
        tensor = numpy.zeros(feature['shape'])

        # get index addresses and fill matrix
        dimensions = tensor.shape[:-len(feature['section'])]
        addresses = self._numerate(*dimensions)
        for address, branch in zip(addresses, branches):

            # get data tensor from h5 reference, squeezing out trivial dimensions
            data = self._walk(branch)[:]
            tensor[tuple(address)] = data

        # translate into xarray
        labels = feature['labels']

        # create generic labeled coordinates
        coordinates = {}
        for size, label in zip(tensor.shape, labels):

            # make numbered coordinates
            numbers = [str(number) for number in range(size)]
            coordinates[label] = numbers

        # add route steps
        for size, label, step in zip(tensor.shape, labels, feature['route']):

            # add names to numbers
            #names = ['{}: {}'.format(number, name) for number, name in zip(coordinates[label], step)]
            names = ['{}'.format(name) for name in step]
            coordinates[label] = names

        # create xarray
        tensor = xarray.DataArray(tensor, coords=coordinates, dims=labels, name=feature['name'], attrs=feature)

        # try to
        try:

            # get band information
            band = tensor.band.data[0]
            tensor.name = '{}_{}'.format(tensor.name, band)

        # otherwise
        except AttributeError:

            # nevermind
            pass

        # squeeze out unimportant size 1 dimensions
        singlets = [label for label, size in zip(tensor.dims, tensor.shape) if size == 1]
        singlets = [label for label in singlets if label not in ('orbit',)]
        tensor = tensor.squeeze(dim=tuple(singlets))

        return tensor

    def _skim(self, members):
        """Skim off the unique members from a list.

        Arguments:
            members: list

        Returns:
            list
        """

        # trim duplicates and sort
        members = list(set(members))
        members.sort()

        return members

    def _splay(self, melt):
        """Splay all members of a combined route into individual routes.

        Arguments:
            melt: list of lists

        Returns:
            list of lists
        """

        # begin routes
        routes = [[]]

        # go through each step
        for step in melt:

            # go through each member
            news = []
            for member in step:

                # add to routes
                [news.append(route + [member]) for route in routes]

            # reestablish routes
            routes = news

        return routes

    def _squeeze(self):
        """Squeeze out the color palette.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.palette
        """

        # begin palette
        palette = {}

        # fill in palette for standard mode
        palette['STANDARD_MODE'] = [0.0, 0.5, 0.8]

        # fill in earth radiance modes
        palette['EARTH_RADIANCE_MODE_0000'] = [0.0, 0.4, 0.3]
        palette['EARTH_RADIANCE_MODE_0001'] = [0.2, 0.7, 0.5]
        palette['EARTH_RADIANCE_MODE_0002'] = [0.3, 1.0, 0.7]

        # fill in background modes
        palette['BACKGROUND_RADIANCE_MODE_0003'] = [0.0, 0.0, 0.5]
        palette['BACKGROUND_RADIANCE_MODE_0004'] = [0.1, 0.1, 0.6]
        palette['BACKGROUND_RADIANCE_MODE_0005'] = [0.2, 0.2, 0.7]
        palette['BACKGROUND_RADIANCE_MODE_0006'] = [0.3, 0.3, 0.8]
        palette['BACKGROUND_IRRADIANCE_MODE_0009'] = [0.4, 0.4, 0.9]
        palette['BACKGROUND_MODE_0010'] = [0.5, 0.5, 1.0]

        # fill in dark modes
        palette['DARK_MODE_0012'] = [0.4, 0.0, 0.4]
        palette['DARK_MODE_0013'] = [0.6, 0.1, 0.6]
        palette['DARK_MODE_0141'] = [0.8, 0.2, 0.8]
        palette['DARK_MODE_0142'] = [1.0, 0.3, 1.0]

        # fill in led mode
        palette['LED_MODE_0011'] = [0.9, 0.5, 0.0]

        # fill in solar irradiance
        palette['SOLAR_IRRADIANCE_MODE_0008'] = [1.0, 1.0, 0.0]

        # set attribute
        self.palette = palette

        return None

    def _stack(self, shapes):
        """Stack all features of one shape into a bigger shape.

        Arguments:
            shapes: dicts

        Returns:
            list of dicts
        """

        # condense shapes together
        features = []
        for shape, entries in shapes.items():

            # go through each name separately
            names = self._skim([entry['name'] for entry in entries])
            for name in names:

                # get subset of types
                types = [entry['type'] for entry in entries if entry['name'] == name]

                # get subset of routes with same name
                routes = [entry['route'] for entry in entries if entry['name'] == name]

                # get the crosssectional shape
                cross = tuple(entries[0]['shape'])

                # check route lengths
                lengths = self._skim([len(route) for route in routes])
                for length in lengths:

                    # get the subset of routes at the same length
                    subset = [route for route in routes if len(route) == length]

                    # break routes into saturated sections
                    sections = self._untangle(subset)

                    # go through each section
                    for section in sections:

                        # meld all routes together
                        slag = self._melt(section)

                        # extend shape based on routes
                        extension = [len(ingot) for ingot in slag] + list(cross)
                        extension = tuple(extension)

                        # create feature
                        feature = {'index': 0, 'name': name, 'route': slag, 'shape': extension, 'type': types[0]}
                        feature.update({'section': cross})
                        feature.update({'labels': self._signify(slag, cross)})
                        feature.update({'units': self._query(self.units, name, '(?)')})
                        feature.update({'dimensions': len(extension)})
                        features.append(feature)

        # sort features by name and shape
        features.sort(key=lambda feature: feature['name'])
        features.sort(key=lambda feature: feature['shape'])
        features.sort(key=lambda feature: max(feature['shape']), reverse=True)
        features.sort(key=lambda feature: len(feature['shape']), reverse=True)

        return features

    def _stamp(self, message):
        """Start timing a block of code, and print results with a message.

        Arguments:
            message: str

        Returns:
            None
        """

        # get final time
        final = time()

        # calculate duration and reset time
        duration = round(final - self.now, 5)
        self.now = final

        # print duration
        print('took {} seconds.'.format(duration))

        # begin new block
        print(message)

        return None

    def _tabulate(self, rows, destination):
        """Create a csv file from a list of records.

        Arguments:
            rows: list of list of strings
            destination: str, file path

        Returns:
            None
        """

        # write rows
        with open(destination, 'w') as pointer:

            # write csv
            csv.writer(pointer).writerows(rows)

        return None

    def _transcribe(self, path):
        """Transcribe the text file at the path.

        Arguments:
            path: str, file path

        Returns:
            list of str
        """

        # read in file pointer
        with open(path, 'r') as pointer:

            # read in text and eliminate endlines
            lines = pointer.readlines()
            lines = [line.replace('\n', '') for line in lines]

        return lines

    def _truncate(self, field, size=25):
        """Truncate a field name to a certain size.

        Arguments:
            field: str

        Returns:
            str
        """

        # truncate the string
        truncation = field
        if len(field) > size:

            # replace middle with ellipses
            half = int((size - 3) / 2)

            # make new string
            truncation = field[:half] + '...' + field[-half:]

        return truncation

    def _unpack(self, contents, level=100, nest=0):
        """Unpack an h5 file into a dict.

        Arguments:
            contents: h5 file
            level=100: int, highest nesting level kept
            nest=0: current nesting level

        Returns:
            dict
        """

        # check for nesting level
        if nest > level:

            # data is truncated to an ellipsis
            data = '...'

        # otherwise
        else:

            # try
            try:

                # to go through keys
                data = {}
                for field, info in contents.items():

                    # add key to data
                    data[field] = self._unpack(info, level, nest + 1)

            # unless there are no keys
            except AttributeError:

                # just return the contents
                data = contents

        return data

    def _untangle(self, routes):
        """Untangle routes into sections.

        Arguments:
            routes: list of list of strings

        Returns:
            list of lists
        """

        # begin sections with first route
        sections = [[] for route in routes]
        for route in routes:

            # go through current sections
            found = False
            occupied = [section for section in sections if len(section) > 0]
            for index, section in enumerate(occupied):

                # check overlaps with each member
                overlaps = self._skim([tuple(overlap) for entry in section for overlap in self._overlap(route, entry)])
                if all([list(overlap) in routes for overlap in overlaps]):

                    # add to section and break
                    section.append(route)
                    found = True
                    break

            # add to new section if not found
            if not found:

                # add to next section
                sections[len(occupied)].append(route)

        # only keep nonzero sections
        sections = [section for section in sections if len(section) > 0]

        return sections

    def _walk(self, fields, node=None):
        """walk through a dict by multiple fields.

        Arguments:
            fields: list of field names
            node: dict or h5

        Returns:
            dict or h5
        """

        # default node to full tree
        if node is None:

            # set to tree
            node = self.tree

        # set contents to node
        contents = node

        # go through each step
        for field in fields:

            # try to
            try:

                # get the best match
                best = self._pick(field, contents.keys())

                # select new node
                contents = contents[best]

            # unless already the data
            except AttributeError:

                # get names
                names = list(contents.dtype.names)
                best = self._pick(field, names)

                # select new node
                contents = contents[best]

        return contents

    def _vectorize(self, shape, target, *operations):
        """Vectorize several features into a normalized matrix for sampling.

        Arguments:
            shape: tuple of int
            target: str
            *operations: unpacked tuple of reduction operations

        Returns:
            tuple of numpy array, numpy array, list of str
        """

        # collect all features beginning with the given shape
        features = [feature for feature in self.subset if feature['shape'][:2] == shape]

        # assemble all matrices
        matrices = []
        names = []
        for feature in features:

            # get band
            band = self._truncate(feature['route'][1][0], 15)

            # get mode
            mode = ''
            if len(feature['route']) > 2:

                # add mode
                mode = self._truncate(feature['route'][2][0], 15)

            # make name
            name = '{}_{}_{}'.format(feature['name'], band, mode)

            # add to matrices if shape is right
            if len(feature['shape']) == len(shape):

                # make prism and append matrix
                prism = self.etch(feature)
                matrix = prism.tensor.reshape(*shape, 1)
                matrices.append(matrix)
                names.append(name)

            # otherwise
            else:

                # go through each operation
                for operation in operations:

                    # try to
                    try:

                        # make prism
                        prism = self.etch(feature)
                        while len(prism.shape) > len(shape):

                            # apply reduction
                            prism.squish(operation)

                        # add to matrices
                        matrix = prism.tensor.reshape(*shape, 1)
                        matrices.append(matrix)
                        names.append('{}_{}'.format(operation, name))

                    # but getting axis errors
                    except (numpy.AxisError, ValueError):

                        # skip
                        pass

        # get full tensor
        tensor = numpy.concatenate(matrices, axis=2)

        # concatenate orbits
        tensor = numpy.concatenate([tensor[0], tensor[1]], axis=0)

        # normalize matrix
        tensor = self._normalize(tensor)
        tensor = numpy.nan_to_num(tensor)

        # get closest match to target name
        pick = self._pick(target, names)
        index = names.index(pick)

        # extract targets
        targets = tensor[:, index]
        tensor = numpy.concatenate([tensor[:, :index], tensor[:, (index + 1):]], axis=1)
        names = names[:index] + names[(index + 1):]

        return tensor, targets, names

    def align(self):
        """Align several relevant distances and plot.

        Arguments:
            None

        Returns:
            None
        """

        # define distances in km
        distances = {'statosphere': 30, 'omi': 705, 'earth': 6371, 'moon': 1737}

        # plot distances
        distances = [item for item in distances.items()]
        distances.sort(key=lambda pair: pair[1])

        # set up plot colors
        colors = ['c-', 'm-', 'g-', 'b-', 'r-', 'k-']

        # set up plot
        pyplot.cla()

        # plot distances
        for index, pair in enumerate(distances):

            # plot
            pyplot.plot([index, index], [0, pair[1]], colors[index], label=pair[0])

        # show plot
        pyplot.title('distance comparisons')
        pyplot.ylabel('distance(km)')
        pyplot.legend()
        pyplot.savefig('plots/distances.png')
        pyplot.close()
        pyplot.cla()

        return None

    def audit(self, parameter):
        """Audit differences between v134 and v0499 versions of orbits.

        Arguments:
            parameter: str, parameter name

        Return:
            None
        """

        # load in audit
        audit = self._load('flaws/audit.json')

        # get entries
        entries = audit['entries']

        # set up plot with orbits and band differences
        orbits = {}
        for entry in entries:

            # get orbit number
            orbit = int(entry['paths'][0].split('/')[-1].split('_')[0].split('-o')[-1])
            if orbit not in orbits.keys():

                # add key
                orbits[orbit] = {'band1': 0.0, 'band2': 0.0}

            # get parameter percent differences
            percents = [member for member in entry['differences'] if member[0].split(':')[0] == parameter]
            if len(percents) > 0:

                # check for band1
                if 'BAND1' in percents[0][0]:

                    # add value
                    orbits[orbit]['band1'] = percents[0][1]

                # check for band2
                if 'BAND2' in percents[0][0]:

                    # add value
                    orbits[orbit]['band2'] = percents[0][1]

        # get entries
        orbits = [item for item in orbits.items()]
        orbits.sort(key=lambda pair: pair[0])
        numbers = [item[0] for item in orbits]
        ones = [item[1]['band1'] for item in orbits]
        twos = [item[1]['band2'] for item in orbits]

        print(numbers[:10])
        print(ones[:10])
        print(twos[:10])

        # make plot
        pyplot.clf()
        pyplot.title('v134 vs v0499 for {}'.format(parameter))
        pyplot.plot(numbers, ones, 'm--', label='band 1')
        pyplot.plot(numbers, twos, 'b--', label='band 2')
        pyplot.legend(loc='upper_right')
        pyplot.ylabel('relative % difference')
        pyplot.xlabel('orbit number')

        # save plot
        pyplot.savefig('plots/audit.png')

        return None

    def cluster(self, number=15):
        """Cluster a set of time slices to check for anomolies.

        Arguments:
            number: int, number of clusters

        Returns:
            None
        """

        # collect features
        self._stamp('collecting features...')
        features = [feature for feature in self if tuple(feature['shape'][:2]) == (2, 1494)]

        # create all prisms
        self._stamp('creating prisms...')
        prisms = []
        for feature in features:

            # try to get prism
            try:

                # etch the prism
                prism = self.etch(feature)
                prisms.append(prism)

            # unless error
            except Exception as error:

                # skip
                print('skipping {}! ({})'.format(feature['name'], error))
                pass

        # get all standard mean based features
        self._stamp('creating plates...')
        [prism._polish() for prism in prisms]
        plates = [prism[-1] for prism in prisms]

        # flatten all arrays
        self._stamp('normalizing vectors...')
        matrix = numpy.array([plate.data.ravel() for plate in plates])
        matrix = self._normalize(matrix)

        # get feature namnes
        names = numpy.array([plate.name for plate in plates])

        # prune out nans
        self._stamp('removing nans...')
        nans = numpy.argwhere(numpy.isnan(matrix))
        nans = self._skim([index[0] for index in nans])

        # get indices of non nans and filter
        indices = [index for index in range(matrix.shape[0]) if index not in nans]

        # remove from matrix and names
        names = names.take(indices)
        matrix = matrix.take(indices, axis=0)
        matrix = matrix.transpose(1, 0)

        # feed into clusterer
        self._stamp('clustering...')
        print('matrix: {}'.format(matrix.shape))
        clusterer = KMeans(n_clusters=number).fit(matrix)

        # gather up clusters
        clusters = []
        for index, center in enumerate(clusterer.cluster_centers_):

            # get all timepoints
            timepoints = [timepoint for timepoint, label in enumerate(clusterer.labels_) if label == index]
            timepoints.sort()

            # sort features by biggest z-score
            scores = [(name, score) for name, score in zip(names, center)]
            scores.sort(key=lambda pair: abs(pair[1]), reverse=True)

            # append cluster
            clusters.append((timepoints, scores))

        # sort clusters by size
        clusters.sort(key=lambda pair: len(pair[0]))
        clusters = [(index, timepoints, scores) for index, (timepoints, scores) in enumerate(clusters)]

        # make report
        report = ['Cluster report', '']
        for index, timepoints, scores in clusters:

            # print cluster
            report.append('cluster {} of {}'.format(index, number))

            # add member length
            report.append('members: {}'.format(len(timepoints)))

            # print timepoints in chunks of ten
            first = [timepoint for timepoint in timepoints if timepoint < 1494]
            second = [timepoint - 1494 for timepoint in timepoints if timepoint > 1493]

            # add first orbit
            report.append('orbit 0:')
            while len(first) > 0:

                # add set of ten
                report.append(str(first[:10]))
                first = first[10:]

            # add second orbit
            report.append('orbit 1:')
            while len(second) > 0:

                # add set of ten
                report.append(str(second[:10]))
                second = second[10:]

            # print first few zippers
            report.append('biggest outliers:')
            for name, score in scores[:20]:

                # print score
                report.append('{}: {}'.format(name, score))

            # add spacer
            report.append('')

        # write report
        self._jot(report, 'reports/clusters.txt')

        # create pca decomposition
        compressor = PCA(n_components=2).fit(matrix)
        compression = compressor.transform(matrix)

        # get explained variances
        variances = compressor.explained_variance_ratio_
        one = round(100 * variances[0], 2)
        two = round(100 * variances[1], 2)

        # start cluster plot
        pyplot.clf()
        pyplot.title('cluster report\n {}% / {}%'.format(one, two))

        # get strongest members of each component
        component = compressor.components_[0]
        zipper = [(name, score) for name, score in zip(names, component)]
        zipper.sort(key=lambda pair: abs(pair[1]), reverse=True)
        caption = ''
        for name, score in zipper[:3]:

            # add to caption
            caption += ' {} ({})'.format(name, round(score, 2))

        # add label
        pyplot.xlabel(caption)

        # get strongest members of each component
        component = compressor.components_[1]
        zipper = [(name, score) for name, score in zip(names, component)]
        zipper.sort(key=lambda pair: abs(pair[1]), reverse=True)
        caption = ''
        for name, score in zipper[:3]:

            # add to caption
            caption += ' {} ({})'.format(name, round(score, 2))

        # add label
        pyplot.ylabel(caption)

        # make colors
        magenta = [1.0, 0.0, 1.0]
        cyan = [0.0, 1.0, 1.0]
        ratios = [index / (number - 1) for index in range(number)]
        colors = [[channel * ratio for channel in cyan] for ratio in ratios]

        # plot each color
        clusters.reverse()
        for cluster, color in zip(clusters, colors):

            # get points
            points = compression.take(cluster[1], axis=0)
            points = points.transpose(1, 0)

            # plot
            pyplot.scatter(points[0], points[1], color=color, marker='o')

        # annotate a few
        for index, timepoints, scores in clusters:

            # sort by orbit
            zero = [point for point in timepoints if point < 1494]
            one = [point for point in timepoints if point > 1493]

            # go through each orbit
            for orbit in (zero, one):

                # get top few
                for member in orbit[:1]:

                    # unpack timepoint
                    time = member
                    label = 0
                    if time > 1493:

                        # subtract for second orbit
                        time = time - 1494
                        label = 1

                    # annotate point
                    point = compression[member]
                    text = '{}) {}-{}'.format(index, label, time)
                    pyplot.annotate(text, xy=point)

        # save plot
        pyplot.savefig('plots/clusters.png')

        return None

    def compare(self, data, dataii):
        """Compare two matrices by subtracting one matrix from another.

        Arguments:
            data: numpy array
            dataii: numpy array

        Returns:
            dict
        """

        # get maximum number of columns
        columns = max([len(data[0]), len(dataii[0])])

        # pad data
        padding = numpy.zeros((len(data), columns))
        data = numpy.concatenate((data, padding), axis=1)
        data = data[:, :columns]

        # pad dataii
        padding = numpy.zeros((len(dataii), columns))
        dataii = numpy.concatenate((dataii, padding), axis=1)
        dataii = dataii[:, :columns]

        # get maximum number of columns
        rows = max([len(data), len(dataii)])

        # pad data
        padding = numpy.zeros((rows, len(data[0])))
        data = numpy.concatenate((data, padding), axis=0)
        data = data[:rows]

        # pad dataii
        padding = numpy.zeros((rows, len(dataii[0])))
        dataii = numpy.concatenate((dataii, padding), axis=0)
        dataii = dataii[:rows]

        # make comparison
        comparison = numpy.subtract(dataii, data)
        comparison = comparison.reshape(1, *comparison.shape)

        # create faux feature
        faux = {'data': comparison, 'route': ['Comparison']}

        return faux

    def compile(self):
        """Compile list of features for trendings, based on two orbit comparison.

        Arguments:
            None

        Returns:
            None
        """

        # get all features
        self._stamp('compiling features...')
        features = [feature for feature in self if feature['shape'][0] == 2]

        # get all doublet features
        doublets = [feature for feature in self if feature['shape'][0] == 4]
        halves = [half for feature in doublets for half in self._halve(feature)]
        features += halves

        # get all red herring feautures
        herrings = self._transcribe('reports/herrings.txt')
        herrings = [herring.split(' X')[0].strip() for herring in herrings if ' X' in herring]

        # weed them out
        features = [feature for feature in features if feature['name'] not in herrings]

        # generate all prisms
        self._stamp('generating all prisms...')
        prisms = []
        for feature in features:

            # try to generate prism
            try:

                # generate prism
                prism = oz.etch(feature)
                prisms.append(prism)

            # otherwise
            except Exception as error:

                # skip
                print('skipping {}! ({})'.format(feature['name'], error))
                pass

        # expand categorical features
        self._stamp('expanding categoricals...')
        expansions = []
        for prism in prisms:

            # make copy
            replica = prism.replicate()

            # check type for integers
            if replica.type == 'uint8':

                # check for number of uniques
                uniques = numpy.unique(replica.matrix)
                if len(uniques) <= 10:

                    # dice along each categorical for bottom two dimensions
                    expansions += replica.dice(2)

        # add expansions to prisms
        expansions = [prism for prism in expansions if len(prism.shape) > 0]
        prisms += expansions

        # perform operations
        self._stamp('performing reductions...')
        operations = {'mean': lambda data, axes: data.mean(axes)}
        operations.update({'minimum': lambda data, axes: data.min(axes)})
        operations.update({'maximum': lambda data, axes: data.max(axes)})
        scores = []
        for prism in prisms:

            # print status
            print('{}, {}'.format(prism.name, prism.shape))

            # avoid zero dimensions
            if len(prism.shape) > 1:

                # go through each operation
                for operation in operations.keys():

                    # get matrix
                    data = prism.tensor

                    # set axes
                    axes = tuple([index for index in range(prism.dimensions)][1:])

                    # perform operation
                    data = operations[operation](data, axes)

                    # determine difference between first and second orbits
                    first = float(data[0])
                    second = float(data[1])
                    difference = second - first

                    # determine average using absolute values to avoid zero spanning issues
                    average = (abs(first) + abs(second)) / 2

                    # calculate change, avoiding nans
                    if average != 0 and not numpy.isinf(average):

                        # compute relative change
                        change = difference / average

                    # otherwise
                    else:

                        # assume 0
                        change = 0

                    # create score object
                    score = {'parameter': prism.name, 'drift': change, 'operation': operation}
                    score.update({'from': first, 'to': second, 'address': prism.route})
                    scores.append(score)

        # collect all scores by parameter and reduction
        self._stamp('collecting scores...')
        collection = {}
        for score in scores:

            # make key
            name = score['parameter']
            operation = score['operation']
            if name not in collection.keys():

                # add to collection
                collection[name] = {operation: [] for operation in operations}

            # append entry
            collection[name][operation].append(score)

        # sort entries by highest drift
        self._stamp('sorting scores...')
        for name, operations in collection.items():

            # sort entries in each member
            for operation, entries in operations.items():

                # sort entries
                entries.sort(key=lambda entry: abs(entry['drift']), reverse=True)

            # sort members
            members = [(operation, entries) for operation, entries in operations.items()]
            members.sort(key=lambda pair: abs(pair[1][0]['drift']), reverse=True)

            # reset members
            collection[name] = members

        # sort entire collection by highest drift
        collection = [item for item in collection.items()]
        collection.sort(key=lambda item: abs(item[1][0][1][0]['drift']), reverse=True)

        # get orbit dates
        eleven = 'July 20, 2011'
        nineteen = 'August 6, 2019'

        # create report and csv file
        title = 'OMI parameters sorted by relative difference between orbits on {} and {}'.format(eleven, nineteen)
        report = [title, '']
        rows = [[title], [''], ['parameter', 'reduction', 'drift (rsd)', 'from (abs)', 'to (abs)', 'addresses']]

        # create abbreviated report and csv file summary
        summary = ['Summary of {}'.format(title), '']
        synopsis = [['Summary of {}'.format(title)], [''], ['parameter', 'reduction', 'drift (rsd)', 'from (abs)', 'to (abs)', 'addresses']]

        # go through each key in the collection
        for key, members in collection:

            # go through each operation
            for index, item in enumerate(members):

                # unpack
                operation, entries = item

                # use name for first row only
                name = '{}'.format(key)
                reduction = '({})'.format(operation)

                # go through each entry at the key
                for indexii, entry in enumerate(entries):

                    # check for index
                    if indexii > 0:

                        # only use spacer
                        name = ' ' * len(name)
                        reduction = ' ' * len(reduction)

                    # unpack entry
                    drift = self._format(entry['drift'])
                    first = self._format(entry['from'])
                    second = self._format(entry['to'])
                    address = str(entry['address']).replace('[', '').replace(']', '').replace("'", '')

                    # make csv row
                    row = [name, reduction, drift, first, second, address]
                    rows.append(row)

                    # make report line
                    line = '{} {}:     {} (rsd),     from {} to {} (abs),     at: {}'.format(*row)
                    report.append(line)

                    # add lines to summaries
                    if index == 0 and indexii == 0:

                        # add line to summaries
                        synopsis.append(row)
                        summary.append(line)

            # add spacer
            report.append('')
            rows.append([''])

        # make reports
        self._jot(report, 'reports/parameters.txt')
        self._tabulate(rows, 'tables/parameters.csv')
        self._jot(summary, 'reports/summary.txt')
        self._tabulate(synopsis, 'tables/summary.csv')

        return None

    def cycle(self, orbit):
        """Find the monthly cycle around a particular orbit.

        Arguments:
            orbit: int, orbit number

        Returns:
            None
        """

        # construct OMI month of orbital modes
        day = ['N1'] * 14 + ['D']
        week = ['N1'] * 14 + ['W1', 'D', 'W2', 'W3']
        month = ['N2'] * 14 + ['M1', 'W3', 'W1', 'D', 'W3', 'W2', 'M3', 'M2']
        sequence = day * 6 + week + day * 6 + week + day * 6 + week + day * 6 + month + day * 2

        # open orbits almanac
        orbits = self._load('orbits.json')

        # get all possible starting orbits, going back one full cycle
        starts = [number for number in range(orbit - len(sequence) - 1, orbit + 1)]

        # get reservoir of data for all orbits between earliest possible start and latest possible finish
        reservoir = {}
        for number in range(starts[0], starts[-1] + len(sequence) + 2):

            # set default data
            date = '?'
            scan = 0
            longitude = 0

            # if there is a record of the orbit
            if str(number) in orbits.keys():

                # update information
                date = orbits[str(number)]['date']
                scan = orbits[str(number)]['scans']
                longitude = orbits[str(number)]['longitude']

            # add to reservoir
            reservoir[number] = (number, date, longitude, scan)

        # make a candidate cycle for every starting orbit
        candidates = []
        for start in starts:

            # get all orbit numbers
            numbers = [number for number in range(start, start + len(sequence))]

            # construct candidate cycle
            candidate = []
            dailies = []
            for number, mode in zip(numbers, sequence):

                # unpack reservoir
                _, date, longitude, scan = reservoir[number]

                # add line to cycle
                candidate.append((number, date, longitude, scan, mode))

                # if the mode is a daily
                if mode == 'D' and scan > 0:

                    # add to dailies count
                    dailies.append(scan)

            # score the candidates based on closeness of average daily scan count to 1494
            score = numpy.average([(daily - 1494) ** 2 for daily in dailies])

            # add to candidates
            candidates.append((candidate, score))

        # rank candidates by their scores
        candidates.sort(key=lambda pair: pair[1])
        candidate = candidates[0][0]

        # construct report
        report = ['orbital cycle for {} orbits around {}:'.format(len(sequence), orbit), '']
        for index, data in enumerate(candidate):

            # unpack data
            number, date, longitude, scan, mode = data

            # make line and add to report
            line = '{}) {}, {}, {} degrees: {} scans {}'.format(index + 1, number, date, round(longitude, 1), scan, mode)
            report.append(line)

            # add spacer
            if mode == 'D':

                # add spacer
                report.append(' ')

        # save
        self._jot(report, 'reports/cycle.txt')

        return None

    def diagram(self, features, level=4, destination=None):
        """Diagram the nesting structure of a set of features.

        Arguments:
            features: list of dict
            level=3: int, nesting level
            destination: str, file path

        Returns:
            None
        """

        # get all addresses
        addresses = [tuple(feature['route'][:level]) for feature in features]

        # make counter to count occurences:
        counter = Counter(addresses)

        # build tree
        tree = {}
        for address, feature in zip(addresses, features):

            # place address in tree
            location = tree
            for place in address:

                # check for key
                location = location.setdefault(place, {})

            # if location is empty, add entry in step
            if len(location) < 1:

                # update with feature info
                size = counter[address]
                location.update({'members': size})

        # print
        pprint(tree)

        # print to file
        if destination:

            # print to destination
            with open(destination, 'w') as pointer:

                # pprint
                pprint(tree, pointer)

        return None

    def etch(self, feature):
        """Etch a prism from a feature.

        Arguments:
            feature: dict

        Returns:
            prism instance
        """

        # get the matrix
        matrix = self._siphon(feature)

        # create prism
        prism = Prism(matrix)

        return prism

    def exhibit(self):
        """Exhibit the parameter structure of the files in a csv.

        Arguments:
            None

        Returns:
            None
        """

        # grab all individual feature routes
        routes = [route for feature in self for route in self._splay(feature['route'])]

        # convert routes into designations
        designations = []
        for route in routes:

            # get orbit, product, and band as first three members
            orbit, product, band = route[:3]
            group ='(-)'
            mode = '(-)'
            remainder = route[3:]

            # determine the index of the first noncapitalized entry, and use it to start the parameter name
            lowercase = [address == address.upper() for address in remainder].index(False)
            name = '/'.join(remainder[lowercase:])

            # check for mode designation
            if 'MODE' in remainder[0]:

                # set mode
                mode = remainder[0]

            # check for group designation
            precursor = route[route.index(remainder[lowercase]) - 1]
            if 'MODE' not in precursor and precursor != band:

                # set group
                group = precursor

            # form designation
            designation = (product, mode, band, group, name)
            designations.append(designation)

        # get all product, mode, band combinations
        headers = self._skim([designation[:3] for designation in designations])

        # sort by each level
        for index in range(2, -1, -1):

            # sort headers
            headers.sort(key=lambda header: header[index])

        # sort headers by number of members
        counter = Counter([header[0] for header in headers])
        headers.sort(key=lambda header: counter[header[0]])

        # get all parameters
        parameters = self._skim([designation[3:] for designation in designations])

        # sort by each level
        for index in range(1, -1, -1):

            # sort headers
            parameters.sort(key=lambda parameter: parameter[index])

        # sort parameters by number of members
        counter = Counter([parameter[0] for parameter in parameters])
        parameters.sort(key=lambda parameter: counter[parameter[0]])

        # default all combinations to '_'
        combinations = {parameter: {header: '_' for header in headers} for parameter in parameters}

        # add all designations to combinations
        for designation in designations:

            # get parameter name and header
            header = designation[:3]
            parameter = designation[3:]

            # set to X
            combinations[parameter][header] = 'X'

        # begin csv rows
        rows = []

        # create csv heading rows
        products = [' ', ' ']
        modes = [' ', ' ']
        bands = [' ', ' ']
        stash = [[], [], []]
        for product, mode, band in headers:

            # add product
            if product not in stash[0]:

                # rejuvenate stash
                stash[1] = []
                stash[2] = []

                # add product to stash and row
                products.append(product)
                stash[0].append(product)

            # otherwise
            else:

                # add blank
                products.append(' ')

            # add mode
            if mode not in stash[1]:

                # rejuvenate stash
                stash[2] = []

                # add product to stash and row
                modes.append(mode)
                stash[1].append(mode)

            # otherwise
            else:

                # add blank
                modes.append(' ')

            # add band
            if band not in stash[2]:

                # add product to stash and row
                bands.append(band)
                stash[2].append(band)

            # otherwise
            else:

                # add blank
                bands.append(' ')

        # add rows
        rows.append(products)
        rows.append(modes)
        rows.append(bands)

        # go through each parameter
        stash = []
        for parameter in parameters:

            # deconstruct
            group, name = parameter

            # begin row
            row = []

            # add group
            if group not in stash:

                # add to stash
                stash.append(group)

                # add to row
                row.append(group)

            # otherwise
            else:

                # append blank
                row.append(' ')

            # append name
            row.append(name)

            # append each combination
            for header in headers:

                # get combination mark
                mark = combinations[parameter][header]
                row.append(mark)

            # append row
            rows.append(row)

        # make into csv
        self._tabulate(rows, 'tables/exhibit.csv')

        return None

    def fake(self, width, height):
        """Fake a feature as a random grid of specified dimensions.

        Arguments:
            width: int
            height: int

        Returns:
            Prism instance.
        """

        # create random matrix and put in tree
        matrix = numpy.random.rand(height, width)
        matrix = matrix.reshape(1, height, width)
        self.tree['fake'] = {'fakeii': {'fakeiii': matrix}}

        # create faux feature
        faux = {'name': 'fake', 'shape': matrix.shape, 'route': [['fake'], ['fakeii'], ['fakeiii']]}
        faux.update({'index': 9999, 'type': 'f32', 'units': '?', 'labels': ['?', '?', '?']})

        # create a prism
        prism = self.etch(faux)

        return prism

    def featurize(self, data):
        """Use cesium to featurize 1-D data.

        Arguments:
            data: numpy array

        Returns:
            numpy array
        """

        # get features
        times = numpy.array([[index for index in range(len(data[0]))]] * len(data))
        values = data

        print(times.shape)
        print(values.shape)

        errors = numpy.random.rand(*values.shape)
        print(errors.shape)

        # make feature set
        names = ["amplitude", "percent_beyond_1_std", "maximum", "max_slope"]
        names += ["median", "median_absolute_deviation", "percent_close_to_median", "minimum"]
        names += ["skew", "std", "weighted_average"]
        series = cesium.time_series.TimeSeries(t=times[0], m=values[0], e=errors[0])
        features = cesium.featurize.featurize_time_series(times=times, values=values, errors=None, features_to_use=names)
        lomb = cesium.features.graphs.lomb_scargle_model(time=times[0], signal=values[0], error=errors[0])

        # plot
        pyplot.clf()
        colors = ['b--', 'm--', 'r--', 'k--'] * len(data)
        for time, value, color in zip(times, values, colors):

            # plot line
            pyplot.plot(time, value, color)

        # save fig
        pyplot.savefig('plots/cesium.png')

        return features, lomb, series

    def gather(self):
        """Gather up the first level features in the tree.

        Arguments:
            None

        Returns:
            None
        """

        # gather features
        features = self._gather(self.tree)

        return features

    def generate(self):
        """Generate all 1-D time series data and save to h5 files.

        Arguments:
            None

        Returns:
            None
        """

        # get flag inventory
        flags = self.flags

        # get features
        features = self._gather(self.tree)

        # define operations:
        operations = {}
        operations['mean'] = lambda tensor: tensor.mean(dim=tuple(tensor.dims[2:]))
        operations['minimum'] = lambda tensor: tensor.min(dim=tuple(tensor.dims[2:]))
        operations['maximum'] = lambda tensor: tensor.max(dim=tuple(tensor.dims[2:]))
        operations['deviation'] = lambda tensor: tensor.std(dim=tuple(tensor.dims[2:]))
        operations['median'] = lambda tensor: xarray.DataArray(tensor).quantile(0.5, dim=tuple(tensor.dims[2:]))

        # define shapes
        shapes = [(1, 1494), (1, 800), (1, 400), (1, 294), (1, 84), (1, 5), (1, 3), (1, 213)]

        # get subset of shapes
        features = [feature for feature in features if tuple(feature['shape'][:2]) in shapes]

        # generate prisms for all features in the subset
        self._stamp('generating data...')
        panels = {shape: [] for shape in shapes}
        names = []
        twigs = []
        for feature in features:

            # grab tensor
            tensor = self._grab(feature['route'])

            # for a flagged feature
            name = feature['name']
            if name in flags.keys():

                # get bits
                bits = flags[name]

                # perform bit decomposition
                decompositions = []
                for bit, description in bits.items():

                    # decompose into fractions with each bit
                    decomposition = self._bite(tensor, bit)

                    # make name
                    alias = '{}_{}'.format(self._pad(bit, bits), description)
                    decomposition.name = '{}:{}:{}'.format(alias, feature['route'][2], feature['route'][3])
                    decompositions.append(decomposition)

                    # make faux feature with extended route and add to twigs
                    faux = {'route': feature['route'] + [alias]}
                    twigs.append(faux)

            # otherwise
            else:

                # make name from route
                tensor.name = '{}:{}:{}'.format(feature['name'], feature['route'][2], feature['route'][3])

                # treat as single feature and add to twigs
                decompositions = [tensor]
                twigs.append(feature)

            # go through each expansion
            for tensor in decompositions:

                # make sure name is not already taken
                if tensor.name not in names:

                    # add to names
                    shape = tensor.shape[:2]
                    names.append(tensor.name)

                    # go through all reductions and make panel
                    panel = []
                    for operation, function in operations.items():

                        # apply reduction
                        reduction = function(tensor)
                        expansion = reduction.expand_dims(dim={'reduction': [operation]})
                        panel.append(expansion)

                    # try to
                    try:

                        # concatenate
                        panel = xarray.concat(panel, dim='reduction')
                        panel = panel.expand_dims(dim={'parameter': [panel.name]})
                        panels[shape].append(panel)

                    # otherwise
                    except Exception as error:

                        # print error
                        print(error)

        # make feature nesting map
        nest = self.nest(twigs)
        nest = {'Data': nest}
        self._dump(nest, 'jsons/nest.json')

        # combine into datasets
        self._stamp('combining into datasets...')
        datasets = []
        for shape, members in panels.items():

            # combine into one
            dataset = xarray.concat(members, dim='parameter', coords='minimal', compat='override')

            # add units info
            units = [self._query(self.units, name.split(':')[0], '_') for name in dataset.coords['parameter'].data]
            dataset.attrs['units'] = ':'.join(units)

            # append
            datasets.append(dataset)

        # save to netcdf
        self._stamp('saving datasets...')
        [dataset.to_netcdf('netcdfs/dataset_{}.nc'.format(str(dataset.shape[-1])), engine='h5netcdf') for dataset in datasets]

        # print status
        self._stamp('datasets saved.')

        return None

    def harvest(self, matrix, targets, names, scores, destination='plots/harvest.png'):
        """Draw the results of a random forest.

        Arguments:
            matrix: numpy array
            targets: numpy array
            names: list of str
            scores: list of floats

        Returns:
            None
        """

        # set the colors
        black = [0.0, 0.0, 0.0]
        mauve = [0.4, 0.0, 0.4]
        maroon = [0.7, 0.0, 0.7]
        magenta = [1.0, 0.0, 1.0]
        colors = [mauve, maroon, magenta]

        # swap axes
        matrix = numpy.swapaxes(matrix, 0, 1)

        # get horizontal
        horizontals = numpy.array([index for index, _ in enumerate(matrix[0])])
        indices = [index for index, _ in enumerate(scores)]

        # get each lines attributes
        scratches = []
        for index, row, target, name, score in zip(indices, matrix, targets, names, scores):

            # get the color
            color = colors[index % len(colors)]

            # set opacity
            opacity = 1.0

            # get the label
            label = '{}'.format(name)
            #label = '{}'.format(index)

            # collect scratch
            scratch = (horizontals, row, color, opacity, label, score)
            scratches.append(scratch)

        # begin plot
        pyplot.clf()
        # pyplot.title(prism.scatter.title)
        # pyplot.ylabel(prism.units)
        # pyplot.xlabel(prism.signifiers[prism.horizontal])

        # begin figure and break into a 3x3 grid
        figure = pyplot.figure()
        grid = figure.add_gridspec(2, 1, width_ratios=[10], height_ratios=[2, 3])

        # create the axes by combining subplots
        axes = []
        axes.append(figure.add_subplot(grid[0, :]))
        axes.append(figure.add_subplot(grid[1, :]))
        # axes.append(figure.add_subplot(grid[2, 0]))
        # axes.append(figure.add_subplot(grid[2, 1]))
        # axes.append(figure.add_subplot(grid[2, 2]))

        # adjust margins
        figure.subplots_adjust(hspace=.1, wspace=0.3)

        # make plot
        scratches.sort(key=lambda scratch: scratch[-1])
        count = 0
        number = 10
        offset = number + 1.0
        for horizontals, line, color, opacity, label, score in scratches[-number:]:

            # decrease offset
            offset += -1.0

            # set label
            label = str(number - count) + ')' + label

            # set opacity
            opacity = exp(-0.07 * (number - count) ** 2)
            count += 1

            # plot line
            axes[0].plot(horizontals, numpy.add(line, offset), color=color, alpha=opacity)

        # plot target
        axes[0].plot(horizontals, targets, color=black, alpha=1.0)

        # repeat for legend
        count = 0
        number = 10
        offset = number + 1.0
        for horizontals, line, color, opacity, label, score in scratches[-number:]:

            # decrease offset
            offset += -1.0

            # set label
            label = str(number - count) + ')' + label

            # set opacity
            opacity = exp(-0.1 * (number - count) ** 2)
            count += 1

            # plot line
            axes[1].plot(horizontals, numpy.add(line, offset), color=color, alpha=opacity, label=label)

        # plot target
        axes[1].plot(horizontals, targets, color=black, alpha=1.0, label='target')

        # add legend
        axes[1].legend(loc='upper right')

        # save the figure
        pyplot.savefig(destination)

        return None

    def jibe(self, whole, *parts):
        """Test whether earth radiance modes jibe with standard mode observations.

        Arguments:
            whole: tuple of data, time routes
            parts: *unpacked tuple of data, time route tuples

        Returns:
            None
        """

        # concatenate times and measurements
        measurements, deltas = whole
        measurements = self._grab(measurements)
        deltas = self._grab(deltas)

        # assign deltas for time coordinates
        measurements = measurements.assign_coords({'dim_1': [time for time in deltas.squeeze().data]})
        measurements = measurements.sortby('dim_1')

        # make parts
        arrays = []
        for measurement, delta in parts:

            # concatenate times and measurements
            measurement = self._grab(measurement)
            delta = self._grab(delta)

            # assign deltas for time coordinates
            measurement = measurement.assign_coords({'dim_1': [time for time in delta.squeeze().data]})
            measurement = measurement.sortby('dim_1')

            # append to array
            arrays.append(measurement)

        # concatenate arrays
        array = xarray.concat(arrays, dim='dim_1')
        array = array.sortby('dim_1')

        # make comparison
        comparison = [measurements, array]

        return comparison

    def look(self, five, level=100):
        """Look at the contents of an h5 file, to a certain level.

        Arguments:
            five: the h5 file
            level=100: the max nesting level to see

        Returns:
            None
        """

        # unpack into a dict
        data = self._unpack(five, level=level)

        # pretty print it
        pprint(data)

        return None

    def monitor(self):
        """Generate OMPS style monitoring files.

        Arguments:
            None

        Returns:
            None
        """

        # begin status
        self._stamp('gathering features...')

        # define operations:
        operations = {}
        operations['OrbitMean'] = lambda tensor: tensor.mean(dim=tuple(tensor.dims))
        operations['OrbitMin'] = lambda tensor: tensor.min(dim=tuple(tensor.dims))
        operations['OrbitMax'] = lambda tensor: tensor.max(dim=tuple(tensor.dims))
        operations['OrbitStd'] = lambda tensor: tensor.std(dim=tuple(tensor.dims))
        operations['OrbitMedian'] = lambda tensor: xarray.DataArray(tensor).quantile(0.5, dim=tuple(tensor.dims))

        # get reservoir of all features for multiple orbits
        reservoir = self._gather(self.tree)

        # subset by relevant file
        products = ['L1-OML1BRVG', 'L1-OML1BRUG', 'L1-OML1BIRR', 'L1-OML1BCAL']
        relevants = products[0:2]
        self._stamp('paring down to files: {}'.format('_'.join(relevants)))
        subset = [feature for feature in reservoir if feature['route'][1] in relevants]

        # subset
        words = ['analysis', 'calibration', 'radiance', 'irradiance']
        self._stamp('paring down to main categories from {} features...'.format(len(reservoir)))
        subset = [feature for feature in subset if any([word in feature['route'][2].lower() for word in words])]

        # removing geodata
        self._stamp('removing geodata from {} features...'.format(len(subset)))
        subset = [feature for feature in subset if 'GEODATA' not in feature['route']]

        # remove features with zero change for all instances
        self._stamp('removing features with zero orbital change from {} features...'.format(len(subset)))
        zeros = self._load('jsons/zeros.json')
        zeros = [name for name, differences in zeros.items() if sum(differences) == 0 and 'time' not in name]
        subset = [feature for feature in subset if feature['name'] not in zeros]

        # collect all routes, orbits, and routes with orbit truncated
        self._stamp('organizing {} features by orbit...'.format(len(subset)))
        routes = [feature['route'] for feature in subset]
        orbits = oz._skim([feature['route'][0] for feature in subset])
        orbits.sort()
        numbers = {orbits[0]: 37292, orbits[1]: 80097}

        # keep only feature represented amongst all orbits
        subset = [feature for feature in subset if all([[orbit] + feature['route'][1:] in routes for orbit in orbits])]

        # go though each orbit
        for orbit in orbits:

            # get the subset
            self._stamp('reducing orbit {} features...'.format(orbit))
            features = [feature for feature in subset if feature['route'][0] == orbit]

            # find time info
            self._stamp('extracting time info...')
            times = [feature for feature in features if 'time' in feature['route'] and 'OBSERVATIONS' in feature['route']]
            times = [self._grab(feature['route']).data[0] for feature in times]
            seconds = int(times[0])

            # make datetime object
            epoch = datetime(2010, 1, 1, 0, 0, 0)
            start = epoch + timedelta(seconds=seconds)

            # make time features
            times = {'DOM': start.day, 'DOY': start.timetuple().tm_yday, 'Hr': start.hour, 'Min': start.minute}
            times.update({'Mon': start.month, 'Sec': start.second, 'Yr': start.year})

            # construct time from year
            beginning = datetime(start.year, 1, 1, 0, 0, 0).timestamp()
            ending = datetime(start.year + 1, 1, 1, 0, 0, 0).timestamp()
            current = start.timestamp()
            fraction = (current - beginning) / (ending - beginning)
            times.update({'TimeFrYr': fraction})

            # add prefix to names
            times = {'OrbitStart' + field: quantity for field, quantity in times.items()}

            # add orbit number
            times.update({'OrbitNumber': numbers[orbit]})

            # create xarrays
            times = {field: xarray.DataArray([quantity], name=field).astype('f8') for field, quantity in times.items()}

            # recast doy as strt
            times['OrbitStartDOY'] = times['OrbitStartDOY'].astype(int).astype('S3')

            # sort
            times = [array for array in times.values()]
            times.sort(key=lambda array: array.name)

            # create unique names
            self._stamp('making unique names...')
            features = self._distinguish(features, 2)

            # subset by file
            files = products[1:2]
            self._stamp('paring down to files: {}'.format(files))
            features = [feature for feature in features if feature['route'][1] in files]

            # convert to camel case
            for feature in features:

                # make camel case name
                feature['name'] = self._cameo(feature['name'])

            # remove time fields
            features = [feature for feature in features if 'time' not in feature['name'].lower()]

            # sort
            features.sort(key=lambda feature: feature['type'])
            features.sort(key=lambda feature: feature['name'])

            # reduce data
            reductions = []
            self._stamp('reducing...')
            for feature in features:

                # grab data
                tensor = self._grab(feature['route'])

                # ignore zeroes
                if tensor.sum() != 0:

                    # try to
                    try:

                        # make all reductions
                        for operation, function in operations.items():

                            # perform reduction
                            reduction = function(tensor)

                            # check for inf, na, nan
                            if xarray.ufuncs.isinf(reduction.data) or xarray.ufuncs.isnan(reduction.data):

                                # set data to 0
                                reduction.data = 0

                            # expand and recastt as float
                            reduction = reduction.expand_dims(dim='dim_0')
                            reduction = reduction.astype('f8')

                            # update name
                            name = operation + feature['name']
                            reduction.name = name

                            # append
                            reductions.append(reduction)

                    # otherwise
                    except Exception as error:

                        # print error
                        print(error)

            # make h5 file
            self._stamp('writing to file...')
            destination = 'bibliotek/omps/omi_{}_o0{}_{}.h5'.format(orbit, numbers[orbit], '_'.join(files))
            storage = h5py.File(destination, 'w')
            trending = storage.create_group('Trending')
            [trending.create_dataset(reduction.name, data=reduction) for reduction in times + reductions]
            storage.close()

            # generate schema
            self._stamp('making schema...')
            self.scheme(times + reductions, files)

        # print status
        self._stamp('files created.')

        return None

    def navigate(self, place=0):
        """Navigate through the reported datasets.

        Arguments:
            place=0: int, starting index of queue

        Returns:
            None
        """

        # begin navigation
        self.navigation = {}

        # continue looping through features
        while place > -1:

            # check for prism
            index = self.subset[place]['index']
            if index not in self.navigation.keys():

                # initialize prism
                self.navigation[index] = self.etch(self.subset[place])

            # get prism
            prism = self.navigation[index]
            print('feature {} of {}'.format(place, len(self.subset)))
            pprint(prism)

            # fill prism with default entry
            if len(prism) < 1:

                # add first entry as mean of all dimensions below top two
                layer = ['vertical', 'horizontal'] + ['mean'] * (len(prism.tensor.shape) - 2)
                prism.view(layer)

            # draw the most recent plate
            plate = prism[-1]
            pprint(plate)

            # sketch a 2-D graph
            plate.glance()

            # wait for next command
            place = self._offer(place, prism)

        return None

    def nest(self, features, index=1):
        """Create a nesting map of features.

        Arguments:
            features: list of dict

        Returns:
            None
        """

        # begin nest
        nest = {}

        # collect all routes that are long enough
        routes = [feature['route'] for feature in features if len(feature['route']) > index]

        # get route members members
        members = self._skim([route[index] for route in routes])

        # update nest
        for member in members:

            # subset the features
            subset = [feature for feature in features if feature['route'][index] == member]
            nest.update({member: self.nest(subset, index + 1)})

        return nest

    def orbit(self):
        """Plot the orbit from the data set.

        Arguments:
            field: the field name to plot

        Returns:
            None
        """

        # begin 3d plot with initial view angle
        pyplot.clf()
        figure = pyplot.figure()
        globe = figure.add_subplot(111, projection='3d')
        globe.view_init(elev=30, azim=260)

        # print status
        print('plotting globe...')

        # set globe color
        color = [0.3, 0.3, 0.6]

        # plot the legend
        modes = ['EARTH_RADIANCE_MODE_0001', 'BACKGROUND_RADIANCE_MODE_0006', 'DARK_MODE_0141', 'LED_MODE_0011', 'SOLAR_IRRADIANCE_MODE_0008']
        for mode in modes:

            # abbreviate for legend
            abbreviation = mode.split('_')[0]

            # plot faux point at center
            globe.plot(0, 0, 0, color=self.palette[mode], marker='o', label=abbreviation)

        # cover faux point with white
        globe.plot(0, 0, 0, color=[1, 1, 1], marker='o')

        # plot latitude lines for every 10 degrees
        for latitude in range(-90, 90, 10):

            # get plotting coordinates
            coordinates = [self._coordinate(latitude, longitude) for longitude in range(-180, 180)]
            xs, ys, zs = [numpy.array(trace) for trace in zip(*coordinates)]

            # plot circle
            globe.plot(xs, ys, zs, color=color, linestyle='solid', linewidth=1)

        # plot longitude lines for every 10 degrees
        for longitude in range(-180, 180, 10):

            # get plotting coordinates
            coordinates = [self._coordinate(latitude, longitude) for latitude in range(-90, 90)]
            xs, ys, zs = [numpy.array(trace) for trace in zip(*coordinates)]

            # plot circle
            globe.plot(xs, ys, zs, color=color, linestyle='solid', linewidth=1)

        # print status
        print('plotting swaths...')

        # gather up the swath coordinates for band3 (vis) only
        banding = lambda record: 'BAND3_RADIANCE' in record['route']
        disregarding = lambda record: 'satellite' not in record['name'] and 'bounds' not in record['name']
        latitudes = self._apply(lambda record: 'latitude' in record['name'] and banding(record) and disregarding(record))
        longitudes = self._apply(lambda record: 'longitude' in record['name'] and banding(record) and disregarding(record))

        # gather up coordinate data
        verticals = self._grab(latitudes[0])
        horizontals = self._grab(longitudes[0])

        # gather up land classification data
        lands = self._apply(lambda record: 'land_water_classification' in record['name'] and banding(record))[0]
        lands = self._grab(lands)

        # go through each swath and plot
        for vertical, horizontal, land in zip(verticals, horizontals, lands):

            # break into separate lines
            blocks = self._congeal(land)
            for start, finish, value in blocks:

                # get color
                ratio = float(value) / 7
                color = [0.0, (1 - ratio), ratio]

                # determine coordinates
                coordinates = [self._coordinate(latitude, longitude) for latitude, longitude in zip(vertical[start:finish], horizontal[start:finish])]
                xs, ys, zs = [numpy.array(trace) for trace in zip(*coordinates)]

                # plot block
                globe.plot(xs, ys, zs, color=color, marker=',', linestyle='', linewidth=0)

        # print status
        print('plotting trajectory...')

        # set satellite altitude at 705 km
        altitude = 705

        # gather up the relevant satellite data from satellite entries
        latitudes = self._apply(lambda record: 'satellite_latitude' in record['name'])
        longitudes = self._apply(lambda record: 'satellite_longitude' in record['name'])

        # go through each series and plot
        for series in zip(latitudes, longitudes):

            # get the values
            verticals = self._grab(series[0])
            horizontals = self._grab(series[1])

            # determine mode and color
            mode = [field for field in series[0]['route'] if 'MODE' in field][0]
            color = self.palette[mode]

            # determine coordinates
            coordinates = [self._coordinate(latitude, longitude, altitude) for latitude, longitude in zip(verticals, horizontals)]
            xs, ys, zs = [numpy.array(trace) for trace in zip(*coordinates)]

            # plot
            globe.plot(xs, ys, zs, color=color, marker='.', linestyle='', linewidth=0)

        # get times to add to title
        start, finish = self.synchronize()
        start = datetime.strftime(start, '%Y-%m-%d %H:%M')
        finish = datetime.strftime(finish, '%Y-%m-%d %H:%M')

        # add legend and show figure
        pyplot.legend(loc='lower left')
        pyplot.title('OMI trajectory,\n{} to {}'.format(start, finish))
        pyplot.show()

        # save figure
        figure.savefig('plots/globe.png')

        return None

    def parse(self, features):
        """Parse feature names into smallest set of words.

        Arguments:
            None

        Returns:
            None
        """

        # get all names
        names = self._skim([feature['name'].lower() for feature in features])

        # get all words
        words = self._skim([word for name in names for word in name.split('_')])

        # only retain three-letter words or longer
        words = [word for word in words if len(word) > 2]

        # remove particular cases
        herrings = ['per', 'avg', 'col']
        words = [word for word in words if word not in herrings]

        # peel off most important
        peels = []
        for _ in range(40):

            # count names
            counts = {word: len([name for name in names if word in name.lower()]) for word in words}
            counts = [item for item in counts.items()]
            counts.sort(key=lambda item: item[1], reverse=True)

            # add top to peels
            peels.append(counts[0])

            # print word
            print(counts[0][0])

            # remove all names
            names = [name for name in names if counts[0][0] not in name.lower()]

        return peels

    def plant(self, shape, target):
        """Plant a random forest based on all features in the subset matching the shape.

        Arguments:
            shape: tuple of ints

        Returns:
            None
        """

        # vectorize the feautures in the subset
        print('vectorizing features...')
        tensor, targets, names = self._vectorize(shape, target, 'mean')

        # load into random forest
        print('planting forest...')
        regressor = RandomForestRegressor(1000, max_depth=5)
        forest = regressor.fit(tensor, targets)

        # make predictions
        predictions = forest.predict(tensor)

        # determine the coefficient of determination
        determination = self._determine(targets, predictions)

        # print out feature importances
        scores = forest.feature_importances_
        importances = [(name, score) for name, score in zip(names, scores)]
        importances.sort(key=lambda pair: pair[1], reverse=True)

        # draw a picture
        self.harvest(tensor, targets, names, scores, destination='plots/forest.png')

        # create report
        report = ['Random forest predicting {} with all features shaped {}'.format(target, shape)]
        report.append(' ')
        report.append('Coefficient of determination: {}'.format(determination))
        report.append(' ')
        count = 0
        for name, score in importances:

            # print space
            if count % 5 == 0:

                # add space
                report.append('')

            # add to report
            report.append('{}) {}: {}'.format(count, round(score, 3), name))
            count += 1

        # make report
        self._jot(report, 'reports/forest.txt')

        return forest

    def probe(self, exact=None, contains=None):
        """Probe the data for redundancies.

        Arguments:
            exact=None: str
            contains=None: str

        Returns:
            None
        """

        # collect all singlet features
        singlets = []
        for feature in self:

            # splay route into multiple routes
            routes = self._splay(feature['route'])

            # for each route
            for route in routes:

                # add singlet
                singlet = {'route': route, 'name': feature['name'], 'shape': feature['shape']}
                singlets.append(singlet)

        # get subset
        if contains:

            # only keep those with the word in the name
            singlets = [singlet for singlet in singlets if contains in singlet['name']]

        # get subset
        if exact:

            # only keep those with the word in the name
            singlets = [singlet for singlet in singlets if singlet['name'] == exact]

        # group singlets by shapes
        shapes = self._group(singlets, lambda singlet: singlet['shape'])

        # go through each shape
        uniques = {}
        for shape, entries in shapes.items():

            # group members by names
            names = self._group(entries, lambda entry: entry['name'])
            for name, members in names.items():

                # print shape
                print('shape: {}, {}, {} members'.format(shape, name, len(members)))

                # grab data from all members
                [member.update({'data': self._grab(member['route']).data}) for member in members]

                # find groups
                groups = []

                # go through each member
                for member in members:

                    # go through each member again
                    group = []
                    for memberii in members:

                        # check for equality
                        if numpy.array_equal(member['data'], memberii['data']):

                            # add to group
                            group.append(memberii['route'])

                    # add to groups
                    groups.append(group)

                # sort all groups
                [group.sort(key=lambda member: str(member)) for group in groups]

                # convert to strings
                groups = [str(group) for group in groups]
                groups = self._skim(groups)
                groups = [literal_eval(group) for group in groups]

            # add to uniques
            field = '{}:{}'.format(name, shape)
            uniques[field] = groups

        # begin report
        report = ['groups of redundant datasets', '']

        # sort uniques
        uniques = [item for item in uniques.items()]
        uniques.sort(key=lambda item: numpy.prod(literal_eval(item[0].split(':')[1])), reverse=True)

        # add groups
        for shape, groups in uniques:

            # add shape
            report.append('shape: {}'.format(shape))
            for group in groups:

                # print each route
                [report.append(str(member)) for member in group]
                report.append(' ')

        # add to reports
        self._jot(report, 'reports/redundancies.txt')

        return None

    def publish(self, *paths):
        """Publish all paths with a date stamp.

        Arguments:
            *paths: unpacked list of str

        Returns:
            None
        """

        # get current time string
        now = datetime.now()
        stamp = now.strftime('%b%d_%H%M')

        # get current news members
        news = os.listdir('bibliotek/news')

        # go through each path
        for path in paths:

            # get stub and extension
            stub = path.split('.')[0].split('/')[-1]
            extension = path.split('.')[-1]

            # check against news
            for member in news:

                # if it matches
                if stub in member and extension in member:

                    # send to archive
                    os.rename('bibliotek/news/{}'.format(member), 'bibliotek/archives/{}'.format(member))

            # create new destination pathway
            destination = 'bibliotek/news/{}_{}.{}'.format(stub, stamp, extension)

            # copy to directory
            print('copying {} to {}...'.format(path, destination))
            command = 'cp {} {}'.format(path, destination)
            os.system(command)

        return None

    def radiate(self, swath, pixel):
        """Generate a profile by wavelength of a time point and a pixel.

        Arguments:
            swath: int, swath index
            pixel: int, pixel index

        Returns:
            None
        """

        # begin plot
        pyplot.clf()
        pyplot.title('radiance profile, swath {}, pixel {}'.format(swath, pixel))
        pyplot.xlabel('wavelength (nm)')
        pyplot.ylabel('radiance ({})'.format(self.units['radiance']))

        # set bands and colors
        bands = (1, 2, 3)
        colors = ['k-', 'm-', 'b-']

        # correct band 1 pixel indices to be half sized
        corrections = {1: lambda index: int(index / 2), 2: lambda index: index, 3: lambda index: index}

        # plot each band
        for band, color in zip(bands, colors):

            # status
            print('plotting band {}...'.format(band))

            # get records for band
            records = self._apply(lambda record: 'BAND{}_RADIANCE'.format(band) in record['route'])
            records = self._apply(lambda record: 'STANDARD_MODE' in record['route'], records)

            # get wavelength data
            feature = self._apply(lambda record: record['name'] == 'wavelength', records)[0]
            wavelengths = self._grab(feature, corrections[band](pixel))
            wavelengths = numpy.array(wavelengths)

            # get radiance data
            feature = self._apply(lambda record: record['name'] == 'radiance', records)[0]
            radiances = self._grab(feature, swath, corrections[band](pixel))
            radiances = numpy.array(radiances)

            # plot
            pyplot.plot(wavelengths, radiances, colors[band - 1], label='Band{}'.format(band))

        # save plot
        pyplot.legend()
        pyplot.savefig('plots/profile.png')

        return None

    def rake(self, swath):
        """Rake across a particular swath, generating a radiance profile.

        Arguments:
            swath: int, swath index

        Returns:
            None
        """

        # begin 3d plot with initial view angle
        pyplot.clf()
        figure = pyplot.figure()
        profile = figure.add_subplot(111, projection='3d')
        profile.view_init(elev=30, azim=260)

        # set bands and colors
        bands = (1, 2, 3)
        colors = ['k-', 'm-', 'b-']

        # correct band 1 pixel indices to be half sized
        corrections = {1: lambda index: index * 2, 2: lambda index: index, 3: lambda index: index}

        # plot each band
        for band, color in zip(bands, colors):

            # status
            print('plotting band {}...'.format(band))

            # get records for band
            records = self._apply(lambda record: 'BAND{}_RADIANCE'.format(band) in record['route'])
            records = self._apply(lambda record: 'STANDARD_MODE' in record['route'], records)

            # get wavelength data
            feature = self._apply(lambda record: record['name'] == 'wavelength', records)[0]
            wavelengths = self._grab(feature)

            # get radiance data
            feature = self._apply(lambda record: record['name'] == 'radiance', records)[0]
            radiances = self._grab(feature, swath)

            # go through each
            pixel = 1
            for wavelength, radiance in zip(wavelengths[1:-1], radiances[1:-1]):

                # set arrays
                pixels = numpy.array([corrections[band](pixel) for _ in radiance])
                wavelength = numpy.array(wavelength)
                radiance = numpy.array(radiance)

                # plot
                profile.plot(pixels, wavelength, radiance, colors[band - 1])

                # increment pixel
                pixel += 1

            # add faux point for legend
            profile.plot(0, 500, 0, colors[band - 1], label='Band{}'.format(band))

        # add axis labels
        profile.set_xlabel('pixel')
        profile.set_ylabel('wavelength (nm)')
        profile.set_zlabel('radiance\n ({})'.format(self.units['radiance']))

        # set legend and show
        pyplot.legend(loc='upper right')
        pyplot.title('radiance profile at swath {}'.format(swath))
        pyplot.show()

        # save figure
        figure.savefig('plots/rake.png')

        return None

    def rain(self, shape, target):
        """Rain down with a regression for each feature.

        Arguments:
            shape: tuple of ints

        Returns:
            None
        """

        # vectorize the feautures in the subset
        print('vectorizing features...')
        tensor, targets, names = self._vectorize(shape, target, 'mean')

        # reshape targets
        targets = targets.reshape(-1, 1)

        # perform linear regression for each feature
        print('raining regressions...')
        tensor = numpy.swapaxes(tensor, 0, 1)
        scores = []
        for vector, name in zip(tensor, names):

            # reshape
            vector = vector.reshape(-1, 1)

            # perform linear regression
            regressor = LinearRegression()
            bolt = regressor.fit(vector, targets)
            score = bolt.score(vector, targets)
            scores.append(score)

        # draw a picture
        tensor = numpy.swapaxes(tensor, 0, 1)
        self.harvest(tensor, targets, names, scores, destination='plots/rain.png')

        # sort scores
        determinations = [(name, score) for name, score in zip(names, scores)]
        determinations.sort(key=lambda pair: pair[1], reverse=True)

        # create report
        report = ['Top correlations for linear regression of {} agsinst all features shaped {}'.format(target, shape)]
        report.append(' ')
        count = 0
        for name, score in determinations:

            # print space
            if count % 5 == 0:

                # add space
                report.append('')

            # add to report
            report.append('{}) {}: {}'.format(count, round(score, 3), name))
            count += 1

        # make report
        self._jot(report, 'reports/rain.txt')

        return None

    def reshape(self):
        """Test out pyreshaper.

        Arguments:
            None

        Returns:
            None
        """

        # Create a Specifier object
        specifier = pyreshaper.specification.create_specifier()

        # get file list
        files = ['{}/{}'.format(self.directory, name) for name in os.listdir(self.directory) if 'BIRR' in name][:1]

        # Specify the input needed to perform the PyReshaper conversion
        specifier.input_file_list = ["/path/to/infile1.nc", "/path/to/infile2.nc", ...]
        specifier.input_file_list = files
        specifier.netcdf_format = "netcdf4"
        specifier.compression_level = 1
        specifier.output_file_prefix = '{}/{}'.format(self.directory, 'reshaper_test')
        specifier.output_file_suffix = ".000101-001012.nc"
        specifier.time_variant_metadata = ["time", "time_bounds"]
        specifier.exclude_list = ['HKSAT', 'ZLAKE']

        # Create the PyReshaper object
        rshpr = pyreshaper.reshaper.create_reshaper(specifier,
                                         serial=False,
                                         verbosity=1,
                                         wmode='s')

        # Run the conversion (slice-to-series) process
        rshpr.convert()

        # Print timing diagnostics
        rshpr.print_diagnostics()

        return None

    def scheme(self, data, files):
        """Create an OMPs schema based on reductions data.

        Arguments:
            data: list of tensors
            files: list of str

        Returns:
            None
        """

        # begin schema
        schema = ['netcdp OMI {} '.format('_'.join(files)) + ' Telemetry {']
        schema.append('')

        # set global attributes
        schema.append('// global attributes:')
        schema.append('		:DATA_QUALITY = 0LL ;')
        schema.append('')

        # declare variables
        schema.append('group: Trending {')
        schema.append('  dimensions:')
        schema.append('  	phony_dim_0 = 1 ;')
        schema.append('  variables:')

        # add all variables
        for index, datum in enumerate(data):

            # set type to double by default
            type = 'double'

            # but if the data type is for a string
            if str(datum.dtype) == '|S3':

                # convert to string
                type = 'string'

            # make entry
            schema.append('  	{} {}(phony_dim_0) ;'.format(type, datum.name))

            # add spacer every five lines after
            if index >= 8 and (index - 8) % 5 == 0:

                # add spacer
                schema.append('')

        # cap off file
        schema.append('  } // group Trending')
        schema.append('}')
        schema.append('')

        # jot to file
        self._jot(schema, 'bibliotek/omps/omi_{}_fields.txt'.format('_'.join(files)))

        return None

    def scour(self, features):
        """Scour the features for those with zero sums or std amongst all names.

        Arguments:
            None

        Returns:
            None
        """

        # print status
        self._stamp('scouring...')

        # grab all tensors
        self._stamp('grabbing tensors...')
        tensors = [oz._grab(feature['route']) for feature in features]

        # calculating sums
        self._stamp('calculating sums...')
        [feature.update({'sum': tensor.sum()}) for feature, tensor in zip(features, tensors)]

        # calculate deviations
        self._stamp('calculating deviations...')
        [feature.update({'deviation': tensor.std()}) for feature, tensor in zip(features, tensors)]

        # group features by routes
        self._stamp('grouping by route...')
        groups = self._group(features, lambda feature: tuple(feature['route'][1:]))

        # calculate the %rsd for each group
        names = {}
        for pair in groups.values():

            # calculate ratio of differences to sums
            difference = float(pair[1]['sum']) - float(pair[0]['sum'])
            #summation = pair[1]['sum'] + pair[0]['sum']
            ratio = difference

            # add to names
            names.setdefault(pair[0]['name'], []).append(float(ratio))

        # sort all entries
        [value.sort(key=lambda ratio: numpy.isinf(ratio), reverse=True) for value in names.values()]
        [value.sort(key=lambda ratio: numpy.isnan(ratio), reverse=True) for value in names.values()]
        [value.sort(reverse=True) for value in names.values()]

        # # sort by maximum rsd
        # names = [item for item in names.items()]
        # names.sort(key=lambda item: numpy.isinf(item[1][0]), reverse=True)
        # names.sort(key=lambda item: numpy.isnan(item[1][0]), reverse=True)
        # names.sort(key=lambda item: item[1][0], reverse=True)

        # jot down to file
        lines = [str(name) for name in names]
        self._dump(names, 'jsons/zeros.json')

        # print status
        self._stamp('scoured.')

        return None

    def scrutinize(self):
        """Scrutinize the irradiance values.

        Arguments:
            None

        Returns:
            None
        """

        # get irradiance features
        features = [feature for feature in self if feature['name'] == 'irradiance']

        # get all tensors
        tensors = [self._siphon(feature) for feature in features]

        # squeeze out matrices for first orbit
        matrices = [tensor.squeeze() for tensor in tensors]

        # try many margins
        for margin in range(21):

            # make pairs
            pairs = [(matrices[index], matrices[index + 1]) for index in range(0, len(matrices), 2)]

            # slice into second matrix
            pairs = [(pair[0], pair[1][:, margin:84 - margin, :, :]) for pair in pairs]

            # average second member of all pairs
            pairs = [(pair[0], pair[1].mean(dim='measurement')) for pair in pairs]

            # subtract one from the other
            triplets = [(pair[0], pair[1], pair[1] - pair[0]) for pair in pairs]

            # reduce all to means
            triplets = [(matrix.mean(dim=['spatial_pixel', 'spectral_pixel']) for matrix in triplet) for triplet in triplets]

            # print names
            print('margin: {}'.format(margin))
            for one, two, three in triplets:

                # print matrices
                #print([matrix.name for matrix in triplet])
                print(three.name, three.data)

        return None

    def sketch(self, prism):
        """Sketch a line plot of the data in the prism.

        Arguments:
            prism: prism instance

        Returns:
            None
        """

        # set the colors
        black = [0.0, 0.0, 0.0]
        mauve = [0.5, 0.0, 0.5]
        magenta = [1.0, 0.0, 1.0]
        colors = [magenta, mauve, black]

        # get indices
        indices = numpy.array([index for index in range(prism.data.shape[1])])

        # get each lines attributes
        scratches = []
        for index, line in enumerate(prism.data):

            # get the color
            color = colors[index % 3]

            # get the opacity
            opacity = exp(-0.1 * index ** 2)

            # get the label
            label = '{} {}'.format(prism.signifiers[prism.vertical], str(index))

            # collect scratch
            scratch = (indices, line, color, opacity, label)
            scratches.append(scratch)

        # begin plot
        pyplot.clf()
        pyplot.title(prism.scatter.title)
        pyplot.ylabel(prism.units)
        pyplot.xlabel(prism.signifiers[prism.horizontal])

        # make plot
        scratches.reverse()
        for indices, line, color, opacity, label in scratches:

            # plot line
            pyplot.plot(indices, line, color=color, alpha=opacity, label=label)

        # add legend
        pyplot.legend(loc='lower right')

        # save the figure
        destination = 'plots/sketch.png'
        pyplot.savefig(destination)

        return None

    def skim(self, pixel):
        """Skim across a particular pixel, generating a radiance profile along the flight path

        Arguments:
            pixel: int, pixel index

        Returns:
            None
        """

        # begin 3d plot with initial view angle
        pyplot.clf()
        figure = pyplot.figure()
        profile = figure.add_subplot(111, projection='3d')
        profile.view_init(elev=30, azim=260)

        # set bands and colors
        bands = (1, 2, 3)
        colors = ['k-', 'm-', 'b-']

        # correct band 1 pixel indices to be half sized
        corrections = {1: lambda index: int(index / 2), 2: lambda index: index, 3: lambda index: index}

        # plot each band
        for band, color in zip(bands, colors):

            # status
            print('plotting band {}...'.format(band))

            # get records for band
            records = self._apply(lambda record: 'BAND{}_RADIANCE'.format(band) in record['route'])
            records = self._apply(lambda record: 'STANDARD_MODE' in record['route'], records)

            # get wavelength data
            feature = self._apply(lambda record: record['name'] == 'wavelength', records)[0]
            wavelengths = self._grab(feature)

            # get radiance data
            feature = self._apply(lambda record: record['name'] == 'radiance', records)[0]
            radiances = self._grab(feature)

            # go through each swath
            swath = 0
            for series in radiances:

                # set arrays
                swaths = numpy.array([swath for _ in series[0]])
                wavelength = numpy.array(wavelengths[corrections[band](pixel)])
                radiance = numpy.array(series[corrections[band](pixel)])

                # plot
                profile.plot(swaths, wavelength, radiance, colors[band - 1])

                # increment swath
                swath += 1

            # add faux point for legend
            profile.plot(0, 500, 0, colors[band - 1], label='Band{}'.format(band))

        # add axis labels
        profile.set_xlabel('swath')
        profile.set_ylabel('wavelength (nm)')
        profile.set_zlabel('radiance\n ({})'.format(self.units['radiance']))

        # set legend and show
        pyplot.legend(loc='upper right')
        pyplot.title('radiance profile at pixel {}'.format(pixel))
        pyplot.show()

        # save figure
        figure.savefig('plots/skim.png')

    def spill(self, index):
        """Spill the contents of the feature into a numpy array.

        Arguments:
            index: int, feature index

        Returns:
            numpy array
        """

        # grab the contents
        contents = self._grab(self[index])

        return contents

    def squirrel(self, directory='squirrels'):
        """Make a csv files from h5 files to load into SQL.

        Arguments:
            None

        Returns:
            None
        """

        # set path
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory) if '.h5' in path]

        # open up h5 file wih xarray
        arrays = [oz._fetch(path) for path in paths]

        # get all keys
        fields = oz._skim([key for array in arrays for key in array['Trending'].keys()])

        # create csv rows
        headers = [fields]
        rows = []
        for array in arrays:

            # create rows
            row = []
            for field in fields:

                # get data, converting nans to 0
                datum = numpy.array(array['Trending'][field])
                datum = numpy.nan_to_num(datum, nan=0, neginf=0, posinf=0)
                row.append(float(datum))

            # append to rows
            rows.append(row)

        # dump into csv
        self._tabulate(headers + rows, 'squirrels/squirrel.csv')
        self._tabulate(rows, 'squirrels/acorns.csv')
        self._tabulate(headers, 'squirrels/header.csv')

        # create sql file
        sql = ['CREATE TABLE squirrel (']

        # go through each field
        for field in fields:

            # add line
            #sql += ['    {} DECIMAL(30, 20) NOT NULL,'.format(field)]
            sql += ['    {} VARCHAR(255) NOT NULL,'.format(field)]

        # add cap
        sql += [');']
        sql = ' '.join(sql)

        # write to sql
        self._jot([sql], 'squirrels/squirrel.txt')

        # create insert command
        insert = "insert into squirrel {} values ".format(str(tuple(fields)).replace("'", ""))
        insert += "(select * from openrowset(bulk '/omi_2011m0720_o037292_L1-OML1BRUG.h5', single_blob) as data)"
        self._jot([insert], 'squirrels/insert.txt')

        return None

    def stitch(self, source, name, disposal=True, order=None):
        """Stitch all pdfs together into a compositie.

        Arguments:
            source: str, source directory
            name: str, name for file
            disposal=True: remove intermediate files?
            order=None: list of str

        Returns:
            None
        """

        # status
        self._stamp('stitching...')

        # load up pdfs
        pages = os.listdir(source)
        pages = [page for page in pages if '.png' in page]
        paths = ['{}/{}'.format(source, page) for page in pages]

        # convert pngs to temporary pdf files
        ghosts = [path.replace('.png', '.pdf') for path in paths]

        # open all images and convert to arrays
        images = [Image.open(path) for path in paths]
        arrays = [numpy.array(image) for image in images]
        shapes = [array.shape for array in arrays]
        height = max([shape[0] for shape in shapes])
        width = max([shape[1] for shape in shapes])

        # pad each array
        pads = []
        for array in arrays:

            # get the array shape
            shape = array.shape

            # adjust height to maximum
            pad = numpy.ones((height, shape[1], shape[2])) * 255
            pad = numpy.concatenate([pad, array], axis=0)
            pad = pad[-height:]

            # adjust width to maximum
            half = int(width / 2)
            left = numpy.ones((height, half, 4)) * 255
            right = numpy.ones((height, half, 4)) * 255
            pad = numpy.concatenate([left, pad, right], axis=1)
            middle = int(pad.shape[1] / 2)
            pad = pad[:, middle - half: middle + width - half]

            # convert type
            pad = pad.astype('uint8')
            pads.append(pad)

        # save all pads to temporary pads if not already there
        [Image.fromarray(pad).convert('RGB').save(ghost) for pad, ghost in zip(pads, ghosts) if ghost.split('/')[-1] not in os.listdir(source)]

        # sort according to order
        ghosts.sort()
        if order:

            # go through each phrase backwards
            for index, phrase in enumerate(order):

                # sort each tail according to next phrase
                tail = ghosts[index:]
                tail.sort(key=lambda path: fuzz.ratio(path, phrase), reverse=True)
                ghosts = ghosts[:index] + tail

        # initialize writer
        writer = PdfFileWriter()

        # go through ghosts
        for ghost in ghosts:

            # print the ghost
            print(ghost)

            # read in ghost
            reader = PdfFileReader(ghost)
            for page in range(reader.getNumPages()):

                # add to writer
                writer.addPage(reader.getPage(page))

        # Write out the merged PDF
        destination = '{}/{}'.format(source, name)
        with open(destination, 'wb') as pointer:

            # write file
            writer.write(pointer)

        # if disposal is true
        if disposal:

            # remove ghost files
            for ghost in ghosts:

                # remove
                os.remove(ghost)

        return None

    def survey(self, *options):
        """Report on the shapes of the features in the dataset.

        Arguments:
            *options: unpacked tuple of function objects, str, or int

        Returns:
            None

        Populates:
            self.shapes
        """

        # get subset
        subset = self._apply(lambda record: ['PROCESSOR'] not in record['route'])
        for option in options:

            # check for string
            if option == str(option):

                # split by spaces in case of multiple entries
                conditions = []
                for word in option.split(','):

                    # check for exclamation point
                    negate = word.startswith('!')
                    word = word.replace('!', '')

                    # check for partial
                    partial = word.startswith(':')
                    word = word.replace(':', '')

                    # make into function
                    conditions.append(self._dig(word, negate=negate, partial=partial))

                # create filter for any condition to be true (or logic)
                filter = lambda record: any([condition(record) for condition in conditions])

            # otherwise check for int
            else:

                # test for int
                try:

                    # assume integer
                    size = int(option)
                    filter = self._sift(size)

                # otherwise
                except TypeError:

                    # assume already a function
                    filter = option

            # reduce subset
            subset = self._apply(filter, subset)

        # sort by feature name
        subset.sort(key=lambda feature: feature['name'])

        # set subset
        self.subset = subset

        # make report
        self._report()

        return None

    def synchronize(self):
        """Determine the time range in the records.

        Arguments:
            None

        Returns:
            tuple of datetime objects
        """

        # set up base time as 01-01-2010
        base = datetime.strptime('2010-01-01', '%Y-%m-%d')

        # get all times
        times = []
        features = self._apply(lambda record: record['name'] == 'time')
        for feature in features:

            # get data
            time = self._grab(feature)
            times.append(time[0])

        # find the max seconds and add to the base for the start of the day
        seconds = float(max(times))
        base += timedelta(seconds=seconds)

        # get all time deltas
        deltas = []
        features = self._apply(lambda record: record['name'] == 'delta_time')
        for feature in features:

            # get data
            data = self._grab(feature)
            data = numpy.array([data]).ravel().tolist()
            deltas += data

        # get start and finish
        start = base + timedelta(seconds=min(deltas) / 1000)
        finish = base + timedelta(seconds=max(deltas) / 1000)

        return start, finish


# reload full system
# import graphs; import plates; import prisms; import ozonators; ozo = ozonators.ozonator
# graphs.reload(graphs); plates.reload(plates); prisms.reload(prisms); ozonators.reload(ozonators); ozo = ozonators.ozonator

# initialize ozonator
ozonator = Ozonator('ozonator/superposition')
ozonator.survey()
